<?php

return [
	'home:header:baseline' => 'BRIAN IS IN THE KITCHEN',
	'index_all:tabs:online_sales' => 'Online sales',
	'index_all:tabs:every_park' => 'Every park',

	// brand
	'brand:bmw' => 'BMW',
	'brand:audi' => 'Audi',
	'brand:citroen' => 'Citro&euml;n',

	// park
	'park:beauvais' => 'Beauvais',
	'park:meaux' => 'Meaux',
	'park:tours' => 'Tours',
	'park:nancy' => 'Nancy',
	'park:troyes' => 'Troyes',

	// item_type
	'item_type:car' => 'car',
	'item_type:truck' => 'truck',
	'item_type:motorbike' => 'motorbike',

	// auth
	'auth:email' => 'Email',
	'auth:password' => 'Password',
	'auth:register' => 'Register',
	'auth:login' => 'Sign-in',
	'auth:login_with' => 'Sign-in with',
	'auth:first_name' => 'First name',
	'auth:last_name' => 'Last name',
];