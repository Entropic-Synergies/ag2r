<?php

trait TSocial
{
	public function format_social(array $row)
	{
		$me = \Cameleon\Services::get_me();
		$row['is_liked'] = $row['n_likes'] > 0 && $me && (new \Cameleon\Model\ObjectLike())->no_depth()->is_liked($row['key'], $me);

		return $row;
	}

	public function callback_comment($object_id, $comment)
	{
	}

	public function callback_delete_comment($object_id)
	{
	}

	public function callback_like($object_id, array $like)
	{
	}

	public function callback_unlike($object_id, array $like)
	{
	}
}