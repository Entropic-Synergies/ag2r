<?php

class JobPosition extends \Cameleon\Model\AModel
{
	static public function get_jobPosition_tree()
	{
		$jobs = (new JobPositionController())->get_jobs();
		$items = [];
		static::append_to_list($items, $jobs);

		return $items;
	}

	static private function append_to_list(&$items, $children, $level = 0)
	{
		$prefix = '';
		if ($level > 0) {
			$prefix = str_repeat('--', $level) . ' ';
		}
		foreach ($children as $job) {
			$items[$job['jobposition_id']] = $prefix . $job['name'];
			if (isset($job['children'])) {
				static::append_to_list($items, $job['children'], $level + 1);
			}
		}
	}

	public function seed()
	{
		$job_list = [
			'Centre de Gestion',
			'Comité d\'Entreprise',
			'Direction Régionale – MDE',
			'Direction Régionale – MDP',
			'Direction Régionale – MDPro',
			'Direction Régionale – Assistantes',
			'Dir. Audit',
			'Dir. des Assurances de Risques',
			'Dir. Dév Activités Mutualistes',
			'Dir. des Activités Sociales',
			'Dir. CICAS',
			'Dir. Commerciale',
			'Dir. Communication & Développement durable',
			'Dir. Contrôle Gesion',
			'Dir. Comptabilité',
			'Dir. Financière',
			'Dir. Gestion',
			'Dir. Systeme Information',
			'Dir. Immobilière',
			'Dir. Juridique et Fiscale',
			'Dir. Logistique et Achat',
			'Dir. Marketing / Innovation / Vente à distance',
			'Dir. Projets et Organistion',
			'Dir. Ressources Humaines',
			'Dir. des Risques',
			'Dir. Veille Informative et Documentaire',
			'Délégation Générale',
			'Autre',
		];
		$x = 1;
		foreach ($job_list as $job) {
			$this->create([
				'jobposition_id' => $x++,
				'name'           => $job,
			]);
		}
	}
}