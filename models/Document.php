<?php

class Document extends \Cameleon\Model\File
{
	public $fields = [
		'user_id'           => 'reference',
		'datatype'          => ['other', 'img', 'pdf'],
		'object_table'      => 'text', # ex. request
		'object_id'         => 'int', # ex. 3
		'filename'          => 'text',
		'original_filename' => 'text',
		'size'              => 'int',
		'cdn'               => 'boolean', # has been sent to AWS S3 ?
		'uploaded'          => 'boolean', # has been uploaded
	];

	public $indexes = [
		'object_table'    => ['object_table'],
		'object_id'       => ['object_id'],
		'object_table_id' => ['object_table', 'object_id'],
		'file_id'         => ['file_id'],
	];

	public function seed_from_model($key = null, $nb)
	{
		$data = [];
		foreach (range(1, $nb) as $i) {
			$this->seed_upload(__DIR__ . '/../resources/fixtures/This is a AG2R Document.pdf', 'application/pdf', $key, array_merge($data, [
					'name'    => 'Document #' . $i,
					'user_id' => rand(1, 4),
				]));
		}
	}

	public function callback_create(&$data = [])
	{
		\Cameleon\Services::get_acl()->set_ACL($data['user_id'], 'document/' . $data['document_id'], [\Cameleon\Security\MaskBuilder::MASK_OWNER]);

		(new Notification())->notify($data['user_id'], $data['object_table'] . '/' . $data['object_id'], 'create_document', $data['key']);
	}

	public function format(array $row)
	{
		$row = parent::format($row);
		$row['user'] = (new User())->no_depth()->get_current($row['user_id']);

		return $row;
	}

	public function upload(array $uploaded_file, $key, array $data = [], array $allowed_types = null)
	{
		if (!isset($data['name']) or !$data['name']) {
			$data['name'] = $uploaded_file['name'];
		}
		$data['original_filename'] = $uploaded_file['name'];

		return parent::upload($uploaded_file, $key, $data, $allowed_types);
	}

	public function callback_delete($where = [])
	{
		if (isset($where['document_id']) && $where['document_id']) {
			(new Notification())->unnotify('document/' . $where['document_id']);
		}
	}
}