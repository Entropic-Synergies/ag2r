<?php

class Perimeter extends \Cameleon\Model\AModel
{
	static public $perimeter_types = [
		'functional'     => 'Périmètre fonctionnel',
		'organizational' => 'Périmètre organisationnel',
		'technical'      => 'Périmètre applicatif & technique',
	];

	public $fields = [
		'type' => 'text',
	];

	public function format_perimeters(array &$row)
	{
		$allPerimeters = $this->format_array($row['perimeters']);
		if (!is_array($allPerimeters)) {
			$allPerimeters = [];
		}
		$row['magic_perimeters'] = $row['magic_perimeters_id'] = $row['perimeters'] = array_fill_keys(array_keys(Perimeter::$perimeter_types), []);
		foreach ($allPerimeters as $type => $perimeters) {
			if (!is_array($perimeters)) {
				$perimeters = [];
			}
			foreach ($perimeters as $key => $perimeter) {
				$p = (new Perimeter())->get_current($perimeter);
				if (!$p) {
					continue;
				}
				$row['perimeters'][$type][] = $p;
				$row['magic_perimeters'][$type][] = $this->format_perimeter($p);
				$row['magic_perimeters_id'][$type][] = $p['perimeter_id'];
			}
		}
	}

	protected function format_perimeter($perimeter)
	{
		return [
			'perimeter_id' => $perimeter['perimeter_id'],
			'name'         => $perimeter['name'],
		];
	}

	public function seed()
	{
		$this->create(['perimeter_id' => 1, 'type' => 'functional', 'name' => "Épargne"]);
		$this->create(['perimeter_id' => 2, 'type' => 'functional', 'name' => "Épargne salariale"]);
		$this->create(['perimeter_id' => 3, 'type' => 'functional', 'name' => "Gestion d'actifs"]);
		$this->create(['perimeter_id' => 4, 'type' => 'functional', 'name' => "Retraite complémentaire"]);
		$this->create(['perimeter_id' => 5, 'type' => 'functional', 'name' => "Retraite supplémentaire"]);
		$this->create(['perimeter_id' => 6, 'type' => 'functional', 'name' => "Santé / Prévoyance"]);

		$this->create(['perimeter_id' => 7, 'type' => 'organizational', 'name' => "DSI"]);
		$this->create(['perimeter_id' => 8, 'type' => 'organizational', 'name' => "Direction"]);
		$this->create(['perimeter_id' => 9, 'type' => 'organizational', 'name' => "Fonctions support"]);
		$this->create(['perimeter_id' => 10, 'type' => 'organizational', 'name' => "Informatique"]);
		$this->create(['perimeter_id' => 11, 'type' => 'organizational', 'name' => "Gestion"]);
		$this->create(['perimeter_id' => 12, 'type' => 'organizational', 'name' => "DRH"]);

		foreach (range(1, 15) as $i) {
			$this->create(['perimeter_id' => $i++, 'type' => 'technical',
						   'name'         => 'Périmètre applicatif & technique #' . $i]);
		}
	}

	public function get_perimeters($object = null)
	{
		$perimeters = [];
		foreach (static::$perimeter_types as $perimeter_type => $label) {
			$perimeters[$perimeter_type] = [
				'label'    => $label,
				'list'     => $this->get_perimeter_list($perimeter_type),
				'selected' => isset($object['perimeters'][$perimeter_type]) ? $object['perimeters'][$perimeter_type] : null
			];
		}

		return $perimeters;
	}

	public function get_perimeter_list($type)
	{
		return $this->get_array(100, ['name' => 'ASC'], ['type' => $type]);
	}

	public function handle_new_perimeters(array $data)
	{
		$perimeters_set = [];
		foreach ($data as $type => $values) {
			$perimeters = json_decode($values);
			$perimeters_set[$type] = [];

			foreach ($perimeters as $perimeter) {
				if (is_string($perimeter)) {
					$result = (new self)->get_array(1, null, [
						'name' => $perimeter,
					]);
					if (count($result) === 1) {
						$result = reset($result);
					} else {
						$result = (new self)->create([
							'name' => $perimeter,
							'type' => $type
						]);
					}
					$perimeters_set[$type][] = (int)$result['perimeter_id'];
				} else {
					$perimeters_set[$type][] = $perimeter;
				}
			}
		}

		return $perimeters_set;
	}
}
