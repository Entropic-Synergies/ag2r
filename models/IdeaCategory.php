<?php
use Cameleon\Model\AModel;

class IdeaCategory extends AModel
{
	public $table_name = 'idea_category';

	public $fields = [
		'parent_id' => 'reference',
		'allow' => 'boolean',
		'start_date' => 'date',
		'end_date' => 'date',
		'user_id'  => 'int',
		'description' => 'text',
	];

	public function format(array $row)
	{
		$row = $this->formatsuperficially($row);
		return $row;
	}

	public function formatsuperficially(array $row)
	{
		$row['allow'] = (Boolean)$row['allow'];
		if (isset($row['user_id']) and $row['user_id'] != 0)
			$row['user'] = (new User)->no_depth()->get_absolutely($row['user_id']);
		if (isset($row['parent_id']) and $row['parent_id'] != 0)
			$row['parent'] = (new IdeaCategory)->no_depth()->get_absolutely($row['parent_id']);
		return $row;
	}

	static public function get_categories_tree($filterIdea = false, $all = false)
	{
		if($filterIdea) {
			$categories = (new IdeaController())->get_categories($user = null, $filterIdea);
		}
		elseif($all) {
            $categories = (new IdeaController())->get_all_categories_active();
        }
        else {
			$categories = (new IdeaController())->get_categories();
		}
		$items = [];
		static::append_to_list($items, $categories);

		return $items;
	}

    public function get_all_categories()
    {
        $db = $this->get_db();
        $categories = $db->get_where('idea_categorys', [], [], null, ['idea_categorys.idea_category_id', 'idea_categorys.name'], []);
        $items = [];
        static::append_to_list($items, $categories);
        return $items;
    }

	static private function append_to_list(&$items, $children, $level = 0)
	{
		$prefix = '';
		if ($level > 0) {
			$prefix = str_repeat('--', $level) . ' ';
		}
		foreach ($children as $category) {
			$items[$category['idea_category_id']] = $prefix . $category['name'];
			if (isset($category['children'])) {
				static::append_to_list($items, $category['children'], $level + 1);
			}
		}
	}

	public function seed()
	{
		$this->create([
			'idea_category_id' => 1,
			'name'             => 'Theme #1',
		]);
		$this->create([
			'idea_category_id' => 2,
			'name'             => 'Theme #2',
		]);
		$this->create([
			'idea_category_id' => 3,
			'name'             => 'Theme #3',
		]);
		$this->create([
			'idea_category_id' => 4,
			'name'             => 'Theme #4',
		]);

		$this->create([
			'idea_category_id' => 5,
			'name'             => 'Theme #3-1',
			'parent_id'        => 3,
		]);
	}
}