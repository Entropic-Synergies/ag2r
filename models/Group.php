<?php

use Cameleon\Security\MaskBuilder;

class Group extends \Cameleon\Model\Group
{
	public $fields = [
		'user_id'            => 'reference', // OWNER
		'n_users'            => 'int',
		'description'        => 'text',
		'experimentation_id' => 'reference',
		'privacy'            => ['public', 'closed', 'secret']
	];

	public function format(array $row)
	{
		$row['user'] = (new User)->no_depth()->get_absolutely($row['user_id'], 'Integrity error: user#' . $row['user_id'] . ' not found');
		if ($row['experimentation_id']) {
			$row['experimentation'] = (new Experimentation())->get_absolutely($row['experimentation_id'], 'Integrity error: experimentation not found');
		}
		$row['users'] = (new User())->no_depth()->get_existing_objects('group', $row['group_id']);

		$me = \Cameleon\Services::get_me();
		$row['user_status'] = $this->get_user_status($me['user_id'], $row['group_id']);

		return $row;
	}

	protected function do_add_user($group_id, $user_id, $status, $invitor_id = null)
	{
		$result = parent::do_add_user($group_id, $user_id, $status);
		if ($status === 'invited') {
			if (null === $invitor_id) {
				throw new InvalidArgumentException('Missing invitor_id');
			}
			(new Notification())->notify($invitor_id, 'group/' . $group_id, 'invite', null, null, [['user_id' => $user_id]]);
		} elseif ($status === 'accepted') {
			(new Notification())->notify($user_id, 'group/' . $group_id, 'join');
			(new \NotificationSubscription())->follow($user_id, 'group/' . $group_id, 'all');
		} elseif ($status === 'pending') {
			$group = (new Group)->get_absolutely($group_id);
			(new Notification())->notify($user_id, $group['key'], 'request', null, null, [['user_id' => $group['user_id']]]);
		}

		return $result;
	}

	public function get_user_status($user_id, $group_id)
	{
		$db = $this->get_db();
		$res = $db->db_query($db->query_format('SELECT status FROM groups_users WHERE group_id = %s AND user_id = %s LIMIT 1', $group_id, $user_id));
		if ($res->num_rows === 1) {
			return $res->fetch_array(MYSQL_ASSOC)['status'];
		}
	}

	public function callback_create(&$data = [])
	{
		\Cameleon\Services::get_acl()->set_ACL($data['user_id'], $data['key'], [MaskBuilder::MASK_OWNER]);
		$this->updateACL($data);
	}

	public function callback_update($data = [], $where = [])
	{
		$this->updateACL($data);
	}

	private function updateACL(array $data)
	{
		if (isset($data['privacy'])) {
			if ($data['privacy'] !== 'secret') {
				\Cameleon\Services::get_acl()->set_ACL('USER', $data['key'], [MaskBuilder::MASK_VIEW]);
			} else {
				\Cameleon\Services::get_acl()->set_ACL('USER', $data['key'], []);
			}
		}
	}

	public function get_recommended($limit = 4, $offset = 0, &$nb_rows = null)
	{
		$db = $this->get_db();
		$me = \Cameleon\Services::get_me();

		return $this->no_depth()->readall($db->query_format('SELECT SQL_CALC_FOUND_ROWS g.*
		FROM groups g
		LEFT JOIN groups_users gu ON (gu.group_id = g.group_id AND gu.user_id = %s)
		WHERE gu.user_id IS NULL AND g.privacy = "public"
		GROUP BY g.group_id
		ORDER BY RAND()
		LIMIT ' . $offset . ',' . $limit,
				$me['user_id'])
			, $nb_rows);
	}

	public function get_users_by_status($group_id, $status)
	{
		$db = $this->get_db();
		$sql = $db->query_format('SELECT u.* FROM users u
		INNER JOIN groups_users gu ON gu.user_id = u.user_id
		WHERE gu.group_id = %s AND gu.`status` = %s ORDER BY gu.created_at DESC', $group_id, $status);

		return (new User)->no_depth()->readall($sql);
	}

	public function seed()
	{
		return;
		$groups = [
			'Les outils des commerciaux',
			'Tour du monde',
			'Le cahier virtuel',
			'Seminaire de juin 2013',
			'arrosoir',
			'assiette',
			'balle',
			'bateau',
			'boîte',
			'bouchon',
			'bouteille',
			'bulles',
			'canard'
		];
		$x = 1;
		foreach ($groups as $group) {
			$group = [
				'group_id'    => $x,
				'name'        => $group,
				'user_id'     => rand(1, 4),
				'description' => $this->get_lorem(),
				'privacy'     => $this->fields['privacy'][rand(0, 1)],
			];
			$this->create($group);
			$users = [];
			foreach (range(1, rand(0, 8)) as $u) {
				$tmp = rand(1, 8);
				if (!in_array($tmp, $users)) {
					$users[] = $tmp;
				}
			}
			foreach ($users as $user) {
				$this->add_user($group, $user);
			}
			$x++;
		}
	}
}