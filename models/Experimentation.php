<?php

use Cameleon\Exception\NotFoundHttpException;

class Experimentation extends \Cameleon\Model\AModel
{
	use TPicture, TSyncRelation, TSocial;

	public $likable = true;
	public $commentable = true;

	public $fields = [
		'user_id'         => 'int', // owner
		'description'     => 'text',
		'innovation_id'   => 'int',
		'budget'          => 'int',
		'perimeters'      => 'text', // array
		'support'         => 'text', // array
		'roi'             => 'text', // array
		'file_picture_id' => 'reference',
	];

	public $install_query = ['
		CREATE TABLE IF NOT EXISTS `experimentations_users` (
			`user_id` int(10) NOT NULL,
			`experimentation_id` int(10) NOT NULL,
			UNIQUE `experimentations_users` (`user_id`,`experimentation_id`),
			KEY `user_id` (`user_id`),
			KEY `experimentation_id` (`experimentation_id`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
	',
		'CREATE TABLE IF NOT EXISTS `experimentations_tags` (
		  `experimentation_id` int(5) NOT NULL,
		  `tag_id` int(5) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;'
	];

	public $drop_queries = [
		'TRUNCATE TABLE experimentations_users',
		'TRUNCATE TABLE experimentations_tags',
	];

	public $indexes = [
		'innovation_id' => ['innovation_id'],
	];

	public function format(array $row)
	{
		$row['users'] = (new User())->in_depth(false)->get_existing_objects('experimentation', $row['experimentation_id']);
		$row['magic_users'] = (new User)->get_magicsuggest_mapped_users($row['users']);

		(new Perimeter)->format_perimeters($row);

		$row['support'] = $this->format_array($row['support']);
		$row['innovation'] = (new Innovation())->in_depth(false)->get_current($row['innovation_id']);
		$row['roi'] = $this->format_array($row['roi']);

		$row['tags'] = (new Tag)->get_object_tags($row['key']);
		$row['magic_tags'] = '[' . implode(', ', $this->get_ids($row['tags'], 'tag_id')) . ']';
		$row['experimentation_tags'] = (new Tag)->get_object_tags($row['key']);

		$row = $this->formatsuperficially($row);

		return $row;
	}

	public function formatsuperficially(array $row)
	{
		$row = $this->format_picture($row, '/assets/img/placeholders/experimentation.jpg');
		$row = $this->format_progress($row);
		$row = $this->format_social($row);
		$row['user'] = (new User)->no_depth()->get_absolutely($row['user_id']);

		return $row;
	}

	public function format_progress(array $row)
	{
		$db = $this->get_db();
		$sql = $db->query_format('SELECT MAX(due_date) as res FROM milestones WHERE experimentation_id = %s', $row['experimentation_id']);

		$result = $db->db_query($sql)->fetch_array(MYSQLI_ASSOC);

		if (count($result) === 1 && null !== $result['res']) {
			$max_due_date = strtotime($result['res']);
			$from_date = $row['created_at']->getTimestamp();

			$total = $max_due_date - $from_date;
			$progress = time() - $from_date;

			$row['progress'] = round($progress / $total * 100);
			$row['progress'] = $row['progress'] > 100 ? 100 : $row['progress'];
		} else {
			$row['progress'] = null;
		}

		return $row;
	}

	public function duplicate($experimentation_id)
	{
		$experimentation_model = $this->get_current($experimentation_id);
		if (!$experimentation_model) {
			throw new NotFoundHttpException('Expérimentation introuvable');
		}

		$count = $this->count([
			'innovation_id' => $experimentation_model['innovation_id']
		]);

		$data = $experimentation_model;
		unset($data['experimentation_id']);
		unset($data['created_at']);
		unset($data['updated_at']);
		$data['name'] = 'Expérimentation n°' . ($count + 1);
		$data['n_likes'] = 0;
		$data['n_comments'] = 0;
		$experimentation = $this->create($data, false, true, true);

		$this->duplicate_relations($experimentation, $experimentation_model, 'tag');
		$this->duplicate_relations($experimentation, $experimentation_model, 'user');

		$milestones = (new Milestone())->get_array('all', [], ['experimentation_id' => $experimentation_model['experimentation_id']]);
		foreach ($milestones as $milestone) {
			unset($milestone['created_at']);
			unset($milestone['updated_at']);
			unset($milestone['milestone_id']);
			$milestone['experimentation_id'] = $experimentation['experimentation_id'];
			(new Milestone())->create($milestone, false, true, true);
		}

		(new Document())->duplicate($experimentation_model, 'experimentation', $experimentation['experimentation_id']);
		(new File())->duplicate($experimentation_model, 'experimentation', $experimentation['experimentation_id']);

		return $experimentation;
	}

	protected function duplicate_relations(array $experimentation, array $experimentation_model, $model)
	{
		$db = $this->get_db();
		$db->db_query($db->query_format('INSERT IGNORE INTO
		experimentations_' . $model . 's (experimentation_id, ' . $model . '_id)
		SELECT %s, ' . $model . '_id FROM experimentations_' . $model . 's WHERE experimentation_id = %s',
			$experimentation['experimentation_id'],
			$experimentation_model['experimentation_id']));
	}

	public function add_user(array $experimentation, array $user)
	{
		$db = $this->get_db();

		return $db->insert('experimentations_users', [
			'user_id'            => $user['user_id'],
			'experimentation_id' => $experimentation['experimentation_id']
		]);
	}

	public function seed_from_innovation(array $innovation, $nb = 1)
	{
		$innovation_id = $innovation['innovation_id'];
		$data = [
			'innovation_id' => $innovation_id,
			'user_id'       => $innovation['user_id'],
		];
		foreach (range(1, $nb) as $i) {
			$item = $this->create(array_merge($data, [
				'created_at'   => (new \DateTime())->sub(new \DateInterval('P' . rand(1, 20) . 'D')),
				'name'         => 'Innov#' . $innovation_id . ' XP#' . $i,
				'description'  => $this->get_lorem(),
				'objectives'   => $this->get_lorem(),
				'stakes'       => $this->get_lorem(),
				'expectations' => $this->get_lorem(),
				'budget'       => rand(1, 100000),
			]));
			$users = [];
			foreach (range(1, rand(0, 4)) as $u) {
				$users[] = rand(1, 4);
			}
			(new User())->sync('experimentation', 'user', $item['experimentation_id'], $users);
			(new Document())->seed_from_model($item['key'], rand(0, 3));
			(new Milestone())->seed_from_experimentation($item, rand(1, 5));
		}
	}

	public function callback_create(&$data = [])
	{
		(new Group)->create([
			'name'               => $data['name'],
			'user_id'            => $data['user_id'],
			'privacy'            => 'closed',
			'experimentation_id' => $data['experimentation_id'],
		]);
		(new Feed())->create([
			'user_id'       => $data['user_id'],
			'object_table'  => 'experimentation',
			'object_id'     => $data['experimentation_id'],
			'context_table' => 'innovation',
			'context_id'    => $data['innovation_id'],
			'action_type'   => 'create',
		]);
		(new Notification())->notify($data['user_id'], 'innovation/' . $data['innovation_id'], 'create_xp', $data['key']);
	}

	public function callback_delete($where = [])
	{
		$id_name = $this->table_name . '_id';
		if (isset($where[$id_name]) && $where[$id_name]) {
			(new Feed())->object_deleted($this->table_name, $where[$id_name]);
			(new NotificationSubscription())->context_deleted($this->table_name, $where[$id_name]);
			(new Notification())->context_deleted($this->table_name, $where[$id_name]);
		}
	}

	public function callback_pre_update(array &$data, array $where)
	{
		foreach ($data as &$value) {
			if (is_array($value)) {
				foreach ($value as $_key => $_value) {
					if ($_value === '') {
						unset($value[$_key]);
					}
				}
			}
		}
	}

	public function callback_update($data = [], $where = [])
	{
		if (isset($data['name']) && isset($where['experimentation_id'])) {
			(new Group)->update([
				'name' => $data['name'],
			], [
				'experimentation_id' => $where['experimentation_id']
			]);
		}
	}
}