<?php

Class ObjectShare extends \Cameleon\Model\AModel
{
	public $table_name = 'object_share';

	public $fields = [
		'from_id'      => 'reference',
		'to_id'        => 'reference',
		'object_table' => 'string',
		'object_id'    => 'reference',
		'comment'      => 'text',
	];

	public $indexes = [
		'object_table' => ['object_table'],
		'object_id'    => ['object_id'],
		'to_id'        => ['to_id'],
	];

	public function callback_create(&$data = [])
	{
		$context_key = $data['object_table'] . '/' . $data['object_id'];
		(new Notification())->notify($data['from_id'], $context_key, 'share', $data['key'], $data['comment'], [['user_id' => $data['to_id']]]);
	}

	public function formatsuperficially(array $row = [])
	{
		$row['from'] = (new User())->no_depth()->get_current($row['from_id']);

		return $row;
	}

	public function share($object_table, $object_id, $user_id, $comment = null)
	{
		$me = \Cameleon\Services::get_me();
		$share = $this->create([
			'object_table' => $object_table,
			'object_id'    => $object_id,
			'from_id'      => $me['user_id'],
			'to_id'        => $user_id,
			'comment'      => $comment,
		]);
		(new NotificationSubscription())->follow($share['to_id'], $share['key'], 'all');
		(new NotificationSubscription())->follow($share['from_id'], $share['key'], 'all');
	}
}
