<?php

use Cameleon\Services;

class Post extends \Cameleon\Model\AModel
{
	use TSocial;

	public $likable = true;
	public $commentable = true;

	public $fields = [
		'user_id'         => 'reference',
		'content'         => 'text',
		'embeded_preview' => 'text',
		'embeded_html'    => 'text',
		'picture_id'      => 'reference',
	];

	public function format(array $row)
	{
		if ($row['picture_id']) {
			$row['picture'] = (new File)->get_absolutely($row['picture_id']);
		}
		$row['user'] = (new User)->get_absolutely($row['user_id']);

		return $this->format_social($row);
	}

	public function create(array $data = [], $delayed = false, $ignore_duplicate = true, $bypass_validation = false)
	{
		$db = $this->get_db();
		$db->begin_transaction();
		try {
			$post = parent::create($data, $delayed, $ignore_duplicate, true);
			if (isset($_FILES['picture']) && $_FILES['picture']['error'] === 0) {
				$file = (new File())->upload($_FILES['picture'], $post['key'], [], [
					'image/jpeg',
					'image/gif',
					'image/png',
				]);
				$this->update([
					'picture_id' => $file['file_id'],
				], [
					'post_id' => $post['post_id'],
				]);
			}
			$db->commit();
		} catch (Exception $e) {
			$db->rollback();
			throw $e;
		}

		return $post;
	}

	public function callback_create(&$data = [])
	{
		$user = (new User())->get_signed_in();

		$feed_data = [
			'user_id'      => $user['user_id'],
			'object_table' => 'post',
			'object_id'    => $data['post_id'],
			'action_type'  => 'create',
		];
		if (isset($data['context'])) {
			list($context_table, $context_id) = explode('/', $data['context']);
			$feed_data['context_table'] = $context_table;
			$feed_data['context_id'] = $context_id;
		}
		(new Feed())->create($feed_data);
		if (isset($data['context'])) {
			(new Notification())->notify($data['user_id'], $data['context'], 'post', $data['key'], $data['key']);
		}
	}
}