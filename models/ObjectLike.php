<?php

use Cameleon\Model\ObjectLike as ObjectLikeBase;

Class ObjectLike extends ObjectLikeBase
{
	public function callback_create(&$data = [])
	{
		$context_key = $data['object_table'] . '/' . $data['object_id'];
		(new Notification())->notify($data['user_id'], $context_key, 'like', $data['key']);
	}

	public function callback_delete($where = [])
	{
		if (isset($where['object_like_id']) && $where['object_like_id']) {
			(new Notification())->unnotify('object_like/' . $where['object_like_id']);
		}
	}
}
