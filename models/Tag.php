<?php


class Tag extends \Cameleon\Model\AModel
{
	use TSyncRelation;

	public $fields = [

	];

	public function get_tags()
	{
		return $this->readall("SELECT `tag_id`, `name` FROM `tags`");
	}

	public function set_xp_tags(array $tags, $key)
	{
		list($object_table, $object_id) = explode('/', $key);

		foreach ($tags as &$tag) {
			if (strpos($tag, '"') === 0) {
				$tag = (new Tag())->create([
					'name' => mb_substr($tag, 1, -1)
				])['tag_id'];
			}
		}

		$this->sync($object_table, 'tag', $object_id, $tags);
	}

	public function get_object_tags($key)
	{
		list($object_table, $object_id) = explode('/', $key);

		return $this->get_existing_objects($object_table, $object_id);
	}

	public function seed()
	{
		return;
		$list = 'Musique
Théâtre
Cinéma
Pêche
Sport
Voyage';
		$words = explode("\n", $list);
		$i = 1;
		foreach ($words as $word) {
			$this->create([
				'tag_id' => $i++,
				'name'   => trim($word),
			]);
		}
	}
}