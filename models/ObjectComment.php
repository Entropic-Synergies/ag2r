<?php

use Cameleon\Model\ObjectComment as ObjectCommentBase;

Class ObjectComment extends ObjectCommentBase
{
	public $likable = true;

	public function callback_create(&$data = [])
	{
		\Cameleon\Services::get_acl()->set_ACL($data['user_id'], $data['key'], [\Cameleon\Security\MaskBuilder::MASK_OWNER]);

		$context_key = $data['object_table'] . '/' . $data['object_id'];
		(new Notification())->notify($data['user_id'], $context_key, 'comment', $data['key'], $data['content']);
	}

	public function callback_delete($where = [])
	{
		if (isset($where['object_comment_id']) && $where['object_comment_id']) {
			(new Notification())->delete([
				'object_table' => 'object_comment',
				'object_id'    => $where['object_comment_id'],
			]);
		}
	}

	public function formatsuperficially(array $row = [])
	{
		$row['user'] = (new User())->no_depth()->get_current($row['user_id']);
		$row = parent::formatsuperficially($row);

		return $row;
	}

	public function add_comment($object, $object_id, array $user, $comment)
	{
		$comment = parent::add_comment($object, $object_id, $user, $comment);
		(new NotificationSubscription())->follow($comment['user_id'], $comment['key'], 'all');
	}
}
