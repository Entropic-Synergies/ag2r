<?php

use Cameleon\Model\Message as BaseMessage;

class Message extends BaseMessage
{
	public function seed()
	{
		return;
		$data = [
			'to_user_id' => 1,
			'status'     => 'unread',
			'content'    => 'Mon message ...',
		];

		foreach (range(1, 15 * 2, 2) as $i) {
			$this->send_message(array_merge($data, [
				'message_id'   => $i,
				'content'      => self::get_lorem(),
				'from_user_id' => rand(2, 4)
			]));
		}
	}

	public function callback_pre_create(array &$data)
	{
		$data['thread_id'] =
			(int)$data['user_id'] === (int)$data['to_user_id'] ? // thread_id = interlocutor_id
				$data['from_user_id']
				: $data['to_user_id'];
	}

	public function send_message(array $data)
	{
		if ($data['to_user_id'] === $data['from_user_id']) {
			throw new \Exception('You can\'t talk to yourself');
		}
		$thread_user = $this->create(array_merge($data, ['user_id' => $data['from_user_id'], 'status' => 'read']));
		if (isset($data['message_id'])) {
			$data['message_id']++;
		}
		$this->create(array_merge($data, ['user_id' => $data['to_user_id']]));

		return $thread_user;
	}

	public function format(array $row)
	{
		$row['from_user'] = (new User($row['from_user_id']))->get_absolutely();
		$row['to_user'] = (new User($row['to_user_id']))->get_absolutely();

		$user_id = (new User)->get_signed_id();

		$row['interlocutor'] = $row['from_user_id'] == $user_id ? $row['to_user'] : $row['from_user'];

		$row['sent_date'] = $row['created_at']->fromNow('%day d %month H:i');

		return $row;
	}

	public function create(array $data = [], $delayed = false, $ignore_duplicate = true, $bypass_validation = false)
	{
		if (!isset($data['user_id'])) {
			throw new \RuntimeException('Use Message::send_message instead of Message::create method');
		}

		return parent::create($data, $delayed, $ignore_duplicate);
	}
}