<?php

use Cameleon\Exception\NotFoundHttpException;
use Cameleon\Model\TModel;
use Cameleon\Security\MaskBuilder;
use Cameleon\Exception\AccessDeniedException;

class Innovation extends \Cameleon\Model\AModel
{
	use TSocial, TPicture;

	public $commentable = true;
	public $likable = true;

	public $fields = [
		'idea_id'         => 'int', // origin
		'user_id'         => 'int', // owner
		'description'     => 'text',
		'group_id'        => 'int', // it's "team"
		'objectives'      => 'text',
		'stakes'          => 'text',
		'expectations'    => 'text',
		'perimeters'      => 'text', // array
		'support'         => 'text', // array
		'roi'             => 'text', // array
		'file_picture_id' => 'reference',
	];

	public $indexes = [
		'idea_id' => ['idea_id'],
	];

	public $install_query = ['CREATE TABLE IF NOT EXISTS `innovations_tags` (
  `innovation_id` int(5) NOT NULL,
  `tag_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;'];

	public function format(array $row)
	{
		$row = $this->formatsuperficially($row);
		$row = $this->format_progress($row);
		$row['experimentations'] = (new Experimentation())->in_depth(false)->get_array(10, ['created_at'         => 'DESC',
																							'experimentation_id' => 'DESC'], ['innovation_id' => $row['innovation_id']]);
		(new Perimeter)->format_perimeters($row);
		$row['support'] = $this->format_array($row['support']);
		$row['roi'] = $this->format_array($row['roi']);
		$row['magic_tags'] = '[' . implode(', ', $this->get_ids($row['tags'], 'tag_id')) . ']';
		$row['idea'] = (new Idea())->get_absolutely($row['idea_id']);

		return $row;
	}

	public function formatsuperficially(array $row)
	{
		$row = $this->format_progress($row);
		$row = $this->format_picture($row, '/assets/img/placeholders/innovation.jpg');
		$row = $this->format_social($row);
		$this->format_current_experimentation($row);
		$row['tags'] = $this->get_tags_and_inherited_tags($row['innovation_id']);
		$row['user'] = (new User)->no_depth()->get_absolutely($row['user_id']);

		return $row;
	}

	protected function format_current_experimentation(array &$row)
	{
		$current_experimentations = (new Experimentation())->in_depth(false)->readall($this->get_db()->query_format('SELECT e.*
FROM experimentations e
INNER JOIN milestones m ON m.experimentation_id = e.experimentation_id
WHERE e.innovation_id = %s AND m.due_date >= %s
ORDER BY m.due_date DESC, e.created_at DESC, e.experimentation_id DESC
LIMIT 1', $row['innovation_id'], new \DateTime('2000-01-01')));
		if (count($current_experimentations) === 1) {
			$row['current_experimentation'] = reset($current_experimentations);
		}
	}

	protected function get_tags_and_inherited_tags($innovation_id)
	{
		$tags_id = [];
		$sql = 'SELECT t.*
FROM tags t
INNER JOIN innovations_tags it ON it.tag_id = t.tag_id
WHERE innovation_id = %s

UNION

SELECT t.*
FROM tags t
INNER JOIN experimentations_tags et ON et.tag_id = t.tag_id
INNER JOIN experimentations e ON e.experimentation_id = et.experimentation_id
WHERE e.innovation_id = %s';
		$db = $this->get_db();

		return (new Tag)->readall($db->query_format($sql, $innovation_id, $innovation_id));
	}

	public function format_progress(array $row)
	{
		$db = $this->get_db();
		$sql = $db->query_format('SELECT MAX(m.due_date) as res
		FROM milestones m
		INNER JOIN experimentations xp ON xp.experimentation_id = m.experimentation_id
		WHERE xp.innovation_id = %s', $row['innovation_id']);

		$result = $db->db_query($sql)->fetch_array(MYSQLI_ASSOC);

		if (count($result) === 1 && null !== $result['res']) {
			$max_due_date = strtotime($result['res']);
			$from_date = $row['created_at']->getTimestamp();

			$total = $max_due_date - $from_date;
			$progress = time() - $from_date;

			if ($progress < 0) {
				$row['progress'] = 0;
			} else {
				$row['progress'] = round($progress / $total * 100);
				$row['progress'] = $row['progress'] > 100 ? 100 : $row['progress'];
			}
		} else {
			$row['progress'] = null;
		}

		return $row;
	}

	public function seed()
	{
		return;
		$data = [
		];
		foreach (range(1, 8) as $i) {
			$item = $this->create(array_merge($data, [
				'created_at'    => (new \DateTime())->sub(new \DateInterval('P' . rand(20, 30) . 'D')),
				'innovation_id' => $i,
				'name'          => 'Innovation ' . $i,
				'user_id'       => rand(1, 4),
				'description'   => $this->get_lorem(),
			]));
			$users = [];
			foreach (range(1, rand(0, 4)) as $u) {
				$users[] = rand(1, 4);
			}
			(new User())->sync('innovation', 'user', $item['innovation_id'], $users);

			(new Document())->seed_from_model($item['key'], rand(0, 15));
			(new Experimentation)->seed_from_innovation($item, rand(1, 15));
		}
	}

	public function import_idea($idea_id, array $user)
	{
		$idea = (new Idea)->get_absolutely($idea_id, 'Idée introuvable');
		if ($idea['status'] === 'experiment') {
			throw new InvalidArgumentException(sprintf('L\'idée "%s" a déjà été retenue.', $idea['name']));
		}

		(new Idea)->update([
			'status' => 'experiment'
		], [
			'idea_id' => $idea['idea_id']
		]);

		$innovation = $this->create([
			'idea_id'     => $idea['idea_id'],
			'name'        => $idea['name'],
			'description' => $idea['description'],
			'user_id'     => $user['user_id'],
		]);
		(new Notification())->notify($user['user_id'], $idea['key'], 'accepted', $innovation['key']);

		return $innovation;
	}

	public function callback_create(&$data = [])
	{
		\Cameleon\Services::get_acl()->set_ACL($data['user_id'], 'innovation/' . $data['innovation_id'], [MaskBuilder::MASK_OWNER]);
		$xp = (new Experimentation())->create([
			'user_id'       => $data['user_id'],
			'name'          => 'Expérimentation n°1',
			'innovation_id' => $data['innovation_id']
		]);
		(new Feed())->create([
			'user_id'      => $data['user_id'],
			'object_table' => 'innovation',
			'object_id'    => $data['innovation_id'],
			'action_type'  => 'create',
		]);
		(new NotificationSubscription())->follow($data['user_id'], $data['key'], 'all');
	}

	public function callback_delete($where = [])
	{
		$id_name = $this->table_name . '_id';
		if (isset($where[$id_name]) && $where[$id_name]) {
			(new Feed())->object_deleted($this->table_name, $where[$id_name]);
			(new NotificationSubscription())->context_deleted($this->table_name, $where[$id_name]);
			(new Notification())->context_deleted($this->table_name, $where[$id_name]);
		}
	}
}