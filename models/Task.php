<?php


class Task extends \Cameleon\Model\AModel
{

	public $fields = [
		'description'        => 'text',
		'experimentation_id' => 'int',
		'date_end'           => 'date',
	];

	public $indexes = [
		'experimentation_id' => ['experimentation_id'],
	];
}