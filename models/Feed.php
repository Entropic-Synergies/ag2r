<?php
class Feed extends \Cameleon\Model\Feed
{
	use TSocial;

	public $indexes = [
		'object_table'  => ['object_table'],
		'object_id'     => ['object_id'],
		'context_table' => ['context_table'],
		'context_id'    => ['context_id'],
		'created_at'    => ['created_at'],
		'score'         => ['n_likes', 'n_comments'],
	];

	public function formatsuperficially(array $row)
	{
		$row = $this->format_social($row);

		return parent::formatsuperficially($row);
	}

	public function callback_create(& $data = [])
	{
		(new NotificationSubscription())->follow($data['user_id'], $data['key'], 'all');
	}

	public function callback_delete($where = [])
	{
		$id_name = $this->table_name . '_id';
		if (isset($where[$id_name]) && $where[$id_name]) {
			(new NotificationSubscription())->context_deleted($this->table_name, $where[$id_name]);
			(new Notification())->context_deleted($this->table_name, $where[$id_name]);
		}
	}
}