<?php

trait TSyncRelation
{
	/**
	 * @param $left_table  string The owner side table
	 * @param $right_table string The reverse side table
	 * @param $id          integer The owner side ID
	 * @param array $items Posted items
	 */
	public function sync($left_table, $right_table, $id, array $items, $extra_data = [])
	{
		$is_experimentation = $left_table === 'experimentation' && $right_table === 'user';
		if ($is_experimentation) {
			$group = (new Group())->get_array(1, null, [
				'experimentation_id' => $id,
			]);
			$group_id = reset($group)['group_id'];
			if (count($group) === 1) {
				$this->sync('group', 'user', $group_id, $items);
				(new Group)->update_row_counter_field($group_id, 'n_users', 'groups_users', [
					'group_id' => $group_id
				]);
			}
		}
		$relation_table = $this->get_relation_table($left_table, $right_table);
		$existing_items_ids = $this->get_existing_objects_ids($left_table, $right_table, $id);

		// Remove empty values
		foreach ($items as $key => $value) {
			if (!$value) {
				unset($items[$key]);
			}
		}

		/* @var $db Cameleon\Db\Db */
		$db = $this->get_db();

		$insertions = [];
		foreach ($items as $item_id) {
			if (!in_array($item_id, $existing_items_ids) && !in_array($item_id, $insertions)) {
				$insertions[] = $item_id;
			}
		}

		foreach ($insertions as $item_id) {
			$db->insert($relation_table, array_merge($extra_data, [
				$left_table . '_id'  => $id,
				$right_table . '_id' => $item_id
			]));
		}

		$deletions = [];
		foreach ($existing_items_ids as $item_id) {
			if (!in_array($item_id, $items)) {
				$deletions[] = $item_id;
			}
		}

		$sql = $db->query_format('DELETE FROM `' . $relation_table . '` WHERE `' . $left_table . '_id` = %s AND `' . $right_table . '_id` IN (%s)', $id, $deletions);
		$db->db_query($sql);

		if ($is_experimentation) {
			$context_key = $left_table . '/' . $id;
			(new NotificationSubscription())->unfollow($deletions, $context_key, 'all');
			(new Notification())->notify($deletions, $context_key, 'quit');
			(new Notification())->notify($insertions, $context_key, 'join');

			$actor_id = \Cameleon\Services::get_me()['user_id'];
			foreach ($insertions as $user_id) {
				(new Notification())->notify($actor_id, $context_key, 'add', null, null, [['user_id' => $user_id]]);
			}
			foreach ($deletions as $user_id) {
				(new Notification())->notify($actor_id, $context_key, 'remove', null, null, [['user_id' => $user_id]]);
			}
			(new NotificationSubscription())->follow($insertions, $context_key, 'all');
		}
	}

	public function get_existing_objects_ids($left_table, $right_table, $id)
	{
		$relation_table = $this->get_relation_table($left_table, $right_table);
		$items_ids = [];
		/* @var $db Cameleon\Db\Db */
		$db = $this->get_db();
		$sql = $db->query_format('SELECT `' . $right_table . '_id` FROM `' . $relation_table . '` WHERE `' . $left_table . '_id` = %s', $id);
		$result = $db->db_query($sql);

		while ($row = $result->fetch_array(MYSQLI_NUM)) {
			$items_ids[] = $row[0];
		}

		return $items_ids;
	}

	public function get_existing_objects($left_table, $id, $reverse = false, $limit = null, &$nb_rows = null, $calc_found_rows = false)
	{
		$right_table = $this->table_name;
		$relation_table = $reverse ? $this->get_relation_table($right_table, $left_table) : $this->get_relation_table($left_table, $right_table);
		$items_ids = [];
		/* @var $db Cameleon\Db\Db */
		$db = $this->get_db();
		$sql = 'SELECT';
		if ($calc_found_rows) {
			$sql .= ' SQL_CALC_FOUND_ROWS';
		}
		$sql .= $db->query_format(' l.* FROM `' . $right_table . 's` l
		INNER JOIN `' . $relation_table . '` r ON r.`' . $right_table . '_id` = l.`' . $right_table . '_id`
		WHERE r.`' . $left_table . '_id` = %s', $id);
		if (null !== $limit) {
			$sql .= sprintf(' LIMIT %d', $limit);
		}

		return $this->readall($sql, $nb_rows);
	}

	protected function get_relation_table($left_table, $right_table)
	{
		return sprintf('%ss_%ss', $left_table, $right_table);
	}
}