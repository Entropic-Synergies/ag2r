<?php

use Cameleon\Model\ObjectRating as ObjectRatingBase;

Class ObjectRating extends ObjectRatingBase
{
	public function callback_create(&$data = [])
	{
		(new Notification())->notify($data['user_id'], $data['object_table'] . '/' . $data['object_id'], 'rate', $data['key']);
	}

	public function callback_delete($where = [])
	{
		if (isset($where['object_rating_id']) && $where['object_rating_id']) {
			(new Notification())->unnotify('object_rating/' . $where['object_rating_id']);
		}
	}
}
