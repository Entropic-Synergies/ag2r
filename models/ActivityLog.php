<?php

class ActivityLog extends \Cameleon\Model\AModel
{
	public $fields = [
		'user_id'          => 'reference',
		'activity'         => [
			'login',
			'logout'
		],
		'timestamp'        => 'date'
	];
	
	public function user_login($user_id) {
		$this->create([ 'user_id' => $user_id, 'activity' => 'login', 'timestamp' => new DateTime('NOW') ]);
	}
	
	public function user_logout($user_id) {
		$this->create([ 'user_id' => $user_id, 'activity' => 'logout', 'timestamp' => new DateTime('NOW') ]);		
	}
	
	public function get_monthly_active_users() {
		$start_day = new DateTime('NOW');
		$start_day->modify('first day of this month 00:00:00');
		
		$db = \Cameleon\Services::get_db();
		$sql = 'SELECT *
				FROM activitylogs al
				WHERE al.activity = \'login\' AND al.timestamp >= \'%start_date%\'
				GROUP BY al.user_id';

		$sql = str_replace(['%start_date%'], [date("Y-m-d H:i:s", $start_day->getTimestamp())], $sql);
				
		$monthly_active = $this->readall($sql);
		
		return $monthly_active;
	}
}
