<?php

class User extends \Cameleon\Model\User
{
	use TSyncRelation, TPicture;

	public $fields = [
		'first_name'             => 'string',
		'last_name'              => 'string',
		'description'            => 'text',
		'jobposition_id'         => 'reference',
		'location'               => 'text',
		'email'                  => 'string',
		'password'               => 'text',
		'phone'                  => 'string',
		'roles'                  => 'text', // Interne pour ACL
		'last_connection'        => 'date',
		'skills'                 => 'text',
		'file_picture_id'        => 'reference',
		'n_notifications_unread' => 'int',
		'cgu'                    => 'boolean',
		'login_name'             => 'string'
	];

	public $install_priority = 255;

	public $install_query = ["
		ALTER TABLE `users` CHANGE `email` `email` VARCHAR( 255 )
	", "
		ALTER TABLE `users` CHANGE `password` `password` VARCHAR( 255 )
	", "
		CREATE TABLE IF NOT EXISTS `users_users` (
  `user_id` int(11) NOT NULL,
  `friend_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('pending','refused','accepted') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pending',
  UNIQUE KEY `user_user` (`user_id`,`friend_id`),
  KEY `user_id` (`user_id`),
  KEY `friend_id` (`friend_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
	", "
		CREATE TABLE IF NOT EXISTS `users_skills` (
  `user_id` int(11) NOT NULL,
  `skill` varchar(255) NOT NULL,
  `value` smallint(3) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `skill` (`skill`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
	", "
		CREATE TABLE IF NOT EXISTS `innovations_users` (
	  `innovation_id` int(11) NOT NULL,
	  `user_id` int(11) NOT NULL,
	  UNIQUE KEY `unique` (`innovation_id`,`user_id`),
	  KEY `innovation_id` (`innovation_id`),
	  KEY `user_id` (`user_id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	", "
		CREATE TABLE IF NOT EXISTS `users_tags` (
		  `user_id` int(5) NOT NULL,
		  `tag_id` int(5) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
	"
	];

	public static function is_signed_in()
	{
		return isset($_SESSION['user_id']) && $_SESSION['user_id'];
	}

	public function get_signed_in()
	{
		if ($this->is_signed_in()) {
			return $this->no_depth()->get_current($this->get_signed_id());
		}

		return [];
	}

	public function get_signed_id()
	{
		if ($this->is_signed_in()) {
			return $_SESSION['user_id'];
		}

		return null;
	}

	public function format(array $row)
	{
		$row['skills']     = $this->format_array($row['skills']);
		$row['tags']       = (new Tag)->get_object_tags($row['key']);
		$row['magic_tags'] = '[' . implode(', ', $this->get_ids($row['tags'], 'tag_id')) . ']';

		return $this->formatsuperficially($row);
	}

	public function formatsuperficially(array $row)
	{
		$row['full_name'] = $row['name'];
		$row['roles']     = !$row['roles'] ? array() : unserialize($row['roles']);
		if (!in_array('USER', $row['roles'])) {
			$row['roles'][] = 'USER';
		}
		$row = $this->format_picture($row, '/assets/img/placeholders/user.jpg');
		if ($row['jobposition_id']) {
			$row['jobposition'] = (new JobPosition())->get_current($row['jobposition_id']);
		}

		if (!isset($row['is_friend'])) {
			$me               = \Cameleon\Services::get_me();
			$row['is_friend'] = $me && $this->get_db()->count('users_users', [
					'user_id'   => $me['user_id'],
					'friend_id' => $row['user_id'],
				]) === 1;
		}

		return $row;
	}

	public function get_magicsuggest_mapped_users(array $users)
	{
		$_users = [];
		foreach ($users as $user) {
			$data     = [
				'id'    => $user['user_id'],
				'name'  => $user['name'],
				'image' => $user['picture']['url'],
				'desc'  => isset($user['jobposition']) ? $user['jobposition']['name'] : '',
			];
			$_users[] = $data;
		}

		return $_users;
	}

	public function seed()
	{
		$i     = 1;
		$email = 'rcarneiro+X@entropic-synergies.com';

		$createUser = function ($data) use ($email, &$i) {
			$email = str_replace('X', $i, $email);
			$data  = array_merge([
				'user_id'  => $i,
				'password' => $this->hash_password('xxx'),
				'email'    => $email,
			], $data);
			$this->create($data);
			$i++;
		};

		$createUser(['first_name' => 'Julien', 'last_name' => 'Tavernier', 'roles' => [], 'login_name' => "JulienT"]);
		$createUser(['first_name' => 'Florian', 'last_name' => 'Leblanc', 'roles' => ['SUPERADMIN'],
					 'login_name' => "FlorianL"]);
		$createUser(['first_name' => 'Stéphane', 'last_name' => 'Hatté', 'roles' => [], 'login_name' => "StéphaneH"]);
		$createUser(['first_name' => 'Stéphane', 'last_name' => 'Saint-Alme', 'roles' => ['SUPERADMIN'],
					 'login_name' => "StéphaneS"]);
		$createUser(['first_name' => 'Patrick', 'last_name' => 'Mesroua', 'roles' => [], 'login_name' => "PatrickM"]);
		$createUser(['first_name' => 'Christian', 'last_name' => 'Chabanon', 'roles' => [],
					 'login_name' => "ChristianC"]);
		$createUser(['first_name' => 'Bertrand', 'last_name' => 'Sellos', 'roles' => ['SUPERADMIN'],
					 'login_name' => "BertrandS"]);
		$createUser(['first_name' => 'Consultants', 'last_name' => 'ARISMORE', 'roles' => [],
					 'login_name' => "Consultants"]);
		$createUser(['first_name' => 'Sophie', 'last_name' => 'Nicolas', 'roles' => [], 'login_name' => "SophieN"]);
		$createUser(['first_name' => 'Julien', 'last_name' => 'Dubois', 'roles' => [], 'login_name' => "JulienD"]);
		$createUser(['first_name' => 'Arthur', 'last_name' => 'de Moulins', 'roles' => [], 'login_name' => "ArthurD"]);
		$createUser(['first_name' => 'Damien', 'last_name' => 'de Bloteau', 'roles' => ['SUPERADMIN'],
					 'login_name' => "DamienD", 'email' => 'damien.debloteau@ag2rlamondiale.fr']);
		$createUser(['first_name' => 'Rodrigue', 'last_name' => 'Carneiro', 'roles' => ['SUPERADMIN'],
					 'login_name' => "RodrigueC", 'email' => 'rcarneiro+X@entropic-synergies.com']);
		$createUser(['first_name' => 'Admin', 'last_name' => 'Admin', 'roles' => ['SUPERADMIN'],
					 'login_name' => "admin", 'email' => 'ag2r@entropic-group.com']);
	}

	public function callback_pre_create(array &$data)
	{
		$this->fix_fullname($data);
	}

	public function callback_pre_update(array &$data, array $where)
	{
		$this->fix_fullname($data);
	}

	private function fix_fullname(array &$data)
	{
		if (!isset($data['name']) && isset($data['first_name'])) {
			$data['name'] = sprintf('%s %s', $data['first_name'], $data['last_name']);
		}
	}

	public function callback_create(& $datas = [])
	{
		$this->update_skills($datas['user_id'], $datas);
	}

	public function callback_update($datas = [], $where = [])
	{
		$this->update_skills($where['user_id'], $datas);
	}

	private function update_skills($user_id, array $data)
	{
		if (isset($data['skills'])) {
			if (!$user_id) {
				throw new Exception('Missing user_id');
			}
			$skills          = unserialize($data['skills']);
			$db              = $this->get_db();
			$existing_skills = $db->db_query($db->query_format('SELECT skill FROM users_skills WHERE user_id = %s', $user_id));
			while ($row = $existing_skills->fetch_array()) {
				if (!isset($skills[$row['skill']])) {
					$db->delete('users_skills', [
						'user_id' => $user_id,
						'skill'   => $row['skill'],
					]);
				} else {
					unset($skills[$row['skill']]);
				}
			}
			$existing_skills->free_result();

			foreach ($skills as $skill => $value) {
				$db->insert('users_skills', [
					'user_id' => $user_id,
					'skill'   => $skill,
					'value'   => $value,
				]);
			}
		}
	}

	public function get_recommended($limit = 4, $offset = 0, &$nb_rows = null)
	{
		$db = $this->get_db();
		$me = \Cameleon\Services::get_me();

		return $this->no_depth()->readall($db->query_format('SELECT SQL_CALC_FOUND_ROWS u.*, 0 AS is_friend FROM (
			(
				SELECT 3 AS position, u1.*
				FROM users u1
				INNER JOIN experimentations_users eu ON eu.user_id = u1.user_id
				INNER JOIN experimentations_users eu2 ON (eu2.experimentation_id = eu.experimentation_id AND eu2.user_id = %s)
			)
			UNION
			(
				SELECT 2 AS position, u2.* FROM users u2
				INNER JOIN users_users uu ON uu.friend_id = u2.user_id
				INNER JOIN users_users uu2 ON uu.user_id = uu2.friend_id AND uu2.user_id = %s
			)
			UNION
			(
				SELECT 1 AS position, u3.* FROM users u3
			)
		) u
		LEFT JOIN users_users uu ON (uu.friend_id = u.user_id AND uu.user_id = %s)
		WHERE uu.friend_id IS NULL
		AND u.user_id != %s
		GROUP BY u.user_id
		ORDER BY u.position DESC, RAND()
		LIMIT ' . $offset . ',' . $limit,
				$me['user_id'],
				$me['user_id'],
				$me['user_id'],
				$me['user_id'])
			, $nb_rows);
	}

	public function get_contacts($user_id, $limit = 'all', $offset = 0, &$nb_rows = null, $query = null)
	{
		$db = $this->get_db();

		$query = null !== $query ? str_replace(' ', '%%', $db->real_escape_string($query)) : null;

		$sql = $db->query_format('SELECT SQL_CALC_FOUND_ROWS users.*, 1 AS is_friend FROM `users`
INNER JOIN users_users uu ON users.user_id = uu.friend_id AND uu.status = "accepted"
WHERE `uu`.`user_id` = %s
 ' . (null !== $query ? ' AND users.name LIKE "%%' . $query . '%%"' : null)
			. ' GROUP BY `users`.`user_id` ORDER BY `last_name` ASC LIMIT ' . $offset . ', ' . $limit,
			$user_id
		);

		return $this->no_depth()->readall($sql, $nb_rows);
	}

	public function get_contacts_count($user_id)
	{
		$this->get_contacts($user_id, 9999999, 0, $nb_rows);

		return $nb_rows;
	}

	public function contact_request($contact_id)
	{
		$contact = (new self)->get_absolutely($contact_id);

		$db = $this->get_db();
		$db->insert('users_users', [
			'user_id'    => $this->user_id,
			'friend_id'  => $contact['user_id'],
			'created_at' => new \DateTime(),
		]);

		(new Notification())->notify($this->user_id, 'user/' . $this->user_id, 'contact_request', null, null, [$contact]);

		return $this;
	}

	public function accept_request($contact_id)
	{
		$contact = (new self)->get_absolutely($contact_id);
		$db      = $this->get_db();
		$db->db_query($db->query_format('
			UPDATE `users_users`
			SET `status` = "accepted"
			WHERE `user_id` = %s
			AND `friend_id` = %s', $contact['user_id'], $this->user_id));

		$filter = [
			'user_id'   => $this->user_id,
			'friend_id' => $contact['user_id'],
		];

		if ($db->count('users_users', $filter) === 1
		) {
			$db->update('users_users', ['status' => 'accepted'], $filter);
		} else {
			$db->insert('users_users', array_merge($filter, [
				'created_at' => new \DateTime(),
				'status'     => 'accepted',
			]));
		}
		(new Notification())->notify($this->user_id, 'user/' . $this->user_id, 'contact_accepted', null, null, [$contact]);

		return $this;
	}

	public function decline_request($contact_id)
	{
		$db = $this->get_db();
		$db->db_query($db->query_format('
			UPDATE `users_users`
			SET `status` = "refused"
			WHERE `user_id` = %s
			AND `friend_id` = %s',
			$contact_id,
			$this->user_id
		));

		return $this;
	}

	public function delete_relation($contact_id)
	{
		$db = $this->get_db();
		$db->db_query($db->query_format('
			DELETE FROM `users_users`
			WHERE (`user_id` = %s
			AND `friend_id` = %s)
			OR (`user_id` = %s
			AND `friend_id` = %s)',
				$this->user_id,
				$contact_id,
				$contact_id,
				$this->user_id
			)
		);

		return $this;
	}

	public function get_relation($contact_id)
	{
		$db     = $this->get_db();
		$sql    = $db->query_format('
			SELECT * 
			FROM `users_users` 
			WHERE (`user_id` = %s
			AND `friend_id` = %s)
			OR (`user_id` = %s
			AND `friend_id` = %s)',
			$this->user_id,
			$contact_id,
			$contact_id,
			$this->user_id
		);
		$result = $db->db_query($sql);

		while ($row = $result->fetch_assoc()) {
			$relation[] = $row;
		}

		return !empty($relation) ? current($relation) : [];
	}

	public function get_pending_contacts_details($user_id)
	{
		$db  = $this->get_db();
		$sql = $db->query_format('
			SELECT `users`.*, 0 AS is_friend
			FROM `users` 
			INNER JOIN `users_users` ON `users_users`.`user_id` = `users`.`user_id`
			WHERE `users_users`.`friend_id` = %s AND `status` = "pending"', $user_id);

		return $this->no_depth()->readall($sql);
	}

	public function get_pending_ideas_count($user_id)
	{
		$db  = $this->get_db();
		$sql = $db->query_format('
			SELECT COUNT(DISTINCT i.idea_id)
			FROM `ideas` i
			INNER JOIN `ideas_operators` io ON io.`idea_id` = i.`idea_id`
			WHERE `io`.`user_id` = %s AND i.`status` IN ("waiting", "study", "estimate")', $user_id);

		$result = $db->db_query($sql);

		return $result->fetch_array(MYSQLI_NUM)[0];
	}

	public function get_pending_requests_count($user_id)
	{
		$db  = $this->get_db();
		$sql = $db->query_format('
			SELECT COUNT(DISTINCT users.user_id)
			FROM `users`
			INNER JOIN `users_users` ON `users_users`.`user_id` = `users`.`user_id`
			WHERE `users_users`.`friend_id` = %s AND `status` = "pending"', $user_id);

		$result = $db->db_query($sql);

		return $result->fetch_array(MYSQLI_NUM)[0];
	}

	public function get_pending_drafts_count($user_id)
	{
		$db  = $this->get_db();
		$sql = $db->query_format('
			SELECT COUNT(DISTINCT i.idea_id)
			FROM `ideas` i
			WHERE `i`.`user_id` = %s AND i.`status` = "draft"', $user_id);

		$result = $db->db_query($sql);

		return $result->fetch_array(MYSQLI_NUM)[0];
	}

	public function get_pending_validations_count($user_id)
	{
		$db  = $this->get_db();
		$sql = $db->query_format('
			SELECT COUNT(DISTINCT i.idea_id)
			FROM `ideas` i
			WHERE `i`.`user_id` = %s AND i.`status` = "validation"', $user_id);

		$result = $db->db_query($sql);

		return $result->fetch_array(MYSQLI_NUM)[0];
	}

	public function get_export()
	{
		$db = $this->get_db();

		$sql = $db->query_format('SELECT last_name, first_name, email, jobpositions.name, location
                           FROM users
                           LEFT OUTER JOIN jobpositions ON users.jobposition_id = jobpositions.jobposition_id
                           ORDER BY last_name ASC');

		$result = $db->db_query($sql);

		$users = [];
		while ($user = $result->fetch_array()) {
			$users[] = $user;
		}

		return $users;
	}

	static public function get_sites_list()
	{
		return ['AGDE', 'AGEN', 'AIX EN PROVENCE', 'AJACCIO', 'ALBI', 'AMIENS', 'ANGERS', 'ANNECY', 'AURILLAC',
			'AVIGNON', 'BALMA', 'BASTIA', 'BAYONNE', 'BERGERAC', 'BESANCON', 'BEZIERS', 'BLOIS', 'BORDEAUX',
			'BOURG EN BRESSE', 'BOURGES', 'BOVES', 'BREST', 'BRIVE LA GAILLARDE', 'CAEN', 'CAHORS', 'CALAIS', 'CASTRES',
			'CAYENNE', 'CHALON SUR SAONE', 'CHAMBERY', 'CHARTRES', 'CHATILLON', 'CLERMONT FERRAND', 'COLMAR',
			'COMPIEGNE', 'CRETEIL', 'DAX', 'DIGNE', 'DIJON', 'EPINAL', 'FLERS EN ESCREBIEUX', 'FOIX', 'FORT DE FRANCE',
			'GAP', 'GRENOBLE', 'GUERET', 'HEROUVILLE SAINT CLAIR', 'ISLE', 'LA MOTTE SERVOLEX', 'LA ROCHE SUR YON',
			'LA ROCHELLE', 'LA VALETTE DU VAR', 'LANGUEUX', 'LE HAVRE', 'LILLE CEDEX', 'LILLE', 'LIMOGES', 'LOGNES',
			'LORIENT', 'LYON', 'MALAKOFF', 'MARSEILLE', 'MAXEVILLE', 'MAZAMET', 'MELUN', 'METZ', 'MONACO',
			'MONS-EN-BAROEUL', 'MONT DE MARSAN', 'MONTAUBAN', 'MONTELIMAR', 'MONTPELLIER', 'MOULINS', 'MULHOUSE',
			'NANCY', 'NANTES', 'NARBONNE', 'NEVERS', 'NICE', 'NIMES', 'NIORT', 'NOUMEA', 'ORLEANS', 'PAPEETE', 'PARIS',
			'PAU', 'PERIGUEUX', 'PERPIGNAN', 'PETIT QUEVILLY', 'POINTE A PITRE', 'POITIERS', 'QUIMPER', 'REIMS',
			'RENNES', 'ROUEN', 'SAINT BENOIT', 'SAINT BRIEUC', 'SAINT ETIENNE', 'SAINT MARIE LA REUNION',
			'SAINT NAZAIRE', 'SAINT PIERRE', 'SAINT QUENTIN', 'SAINT SATURNIN', 'SAINT VIT', 'SETE', 'SEYNOD',
			'SAINT HERBLAIN', 'STRASBOURG', 'TAISSY', 'TARBES', 'TOULON', 'TOULOUSE', 'TOURS', 'TROYES', 'VANNES',
			'VOISINS LE BRETONNEUX'];
	}
}
