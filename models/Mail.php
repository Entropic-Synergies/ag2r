<?php

class Mail extends \Cameleon\Model\Mail
{
	public function render_email($template, array $data = array())
	{
		$renderer = new \Cameleon\Renderer($template, null, null, [], $data, CAMELEON_DIR_APP . 'mails/');

		return $renderer->render();
	}

	public function send($to = [], $template = '', $variables = [], $from = [], $categories = [])
	{
		$this->to = $to;
		if (!isset($this->to['email']) or empty($this->to['email'])) {
			throw new \Exception('No email defined');
		}
		if (!isset($this->to['name'])) {
			$this->to['name'] = $this->to['email'];
		}
		if (!isset($this->to['user_id']) or empty($this->to['user_id'])) {
			$this->to['user_id'] = 0;
		}

		$this->categories = $categories;
		if (!in_array(CAMELEON_PROJECTNAME, $this->categories)) {
			$this->categories[] = CAMELEON_PROJECTNAME;
		}

		$this->template = $template;
		if (empty($this->template)) {
			$this->template = 'welcome';
		}

		$this->from = $from;
		if (!isset($this->from['email']) or empty($this->from['email'])) {
			$this->from['email'] = CAMELEON_MAIL_FROM_EMAIL;
		}
		if (!isset($this->from['name']) or empty($this->from['name'])) {
			$this->from['name'] = CAMELEON_MAIL_FROM_NAME;
		}
		if (!isset($this->from['user_id']) or empty($this->from['user_id'])) {
			$this->from['user_id'] = 0;
		}

		$this->html = $this->render_email($template, $variables);
		$this->subject = $this->render_email(str_replace('.html.twig', '.subject.twig', $template), $variables);

		$curl_response = $this->sendgrid_curl();

		$this->create([
			'user_id'       => $this->to['user_id'],
			'to_email'      => $this->to['email'],
			'to_name'       => $this->to['name'],
			'from_user_id'  => $this->from['user_id'],
			'template'      => $this->template,
			'curl_response' => $curl_response,
			'categories'    => json_encode($this->categories),
		], true);
	}
}