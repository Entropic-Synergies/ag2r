<?php

use Cameleon\Security\MaskBuilder;

class Idea extends \Cameleon\Model\AModel
{
	use TSocial, TPicture;

	public $commentable = true;
	public $likable = true;

	static public $statuses = [
		'study'      => 'Idée à étudier',
		'waiting'    => 'A l\'étude',
		'estimate'   => 'En cours d\'estimation',
		'confirmed'  => 'Mise en oeuvre',
		'experiment' => 'Retenue',
		'duplicate'  => 'Déjà existant',
		'refused'    => 'Refusée'
	];

	public $fields = [
		'description'      => 'text',
		'user_id'          => 'int',
		'status'           => [
			'waiting',
			'confirmed',
			'refused',
			'draft',
			'duplicate',
			'study',
			'validation',
			'estimate',
			'experiment',
			'deleted'
		],
		'closed'           => 'boolean',
		'file_picture_id'  => 'reference',
		'idea_category_id' => 'reference',
	];

	public $indexes = [
		'status' => ['status']
	];

	public $install_query = ["
		CREATE TABLE IF NOT EXISTS `ideas_operators` (
		  `idea_id` int(11) NOT NULL,
		  `user_id` int(11) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
	"];

	public function format(array $row)
	{
		$row = $this->formatsuperficially($row);

		$row['closed']     = (Boolean)$row['closed'];
		$row['_operators'] = $this->get_operators($row['idea_id']);
		$row['operators']  = (new User)->get_magicsuggest_mapped_users($row['_operators']);

		return $row;
	}

	public function formatsuperficially(array $row)
	{
		if ($row['idea_category_id']) {
			$row['idea_category'] = (new IdeaCategory())->no_depth()->get_absolutely($row['idea_category_id']);
		}
		$me            = \Cameleon\Services::get_me();
		$row['rating'] = $this->get_rating_avg($row['idea_id'], $row['ratings_count']);
		if (null !== $row['rating']) {
			$row['my_rate']   = $this->get_user_rate($row['idea_id'], $me['user_id']);
			$row['has_rated'] = null !== $row['my_rate'];
		} else {
			$row['has_rated'] = false;
		}
		$row['rating_rounded'] = round($row['rating']);
		$row['rating']         = round($row['rating'], 2);
		$row['user']           = (new User)->no_depth()->get_absolutely($row['user_id']);
		$row                   = $this->format_social($row);
		$row['picture']        = [
			'url' => '/assets/img/placeholders/innovation.jpg',
		];

		return $row;
	}

	public function deleteidea($idea_id, array $user, $message = null)
	{
		$idea = $this->get_absolutely($idea_id);
		if ($idea['status'] === 'draft' ||
			$idea['status'] === 'duplicate' ||
			$idea['status'] === 'refused'
		) {
			return;
		}
		if ($idea['status'] === 'experiment') {
			throw new InvalidArgumentException(sprintf('L\'idée "%s" a été retenue, vous ne pouvez plus la refuser.', $idea['name']));
		}

		if ($message and $user['user_id'] != $idea['user_id']) {
			(new Message())->send_message([
				'from_user_id' => $user['user_id'],
				'to_user_id'   => $idea['user_id'],
				'content'      => sprintf('Votre idée "%s" a été refusée pour la/les raisons suivantes :' . "\n" . '%s', $idea['name'], $message)
			]);
		}

		$this->update([
			'status' => 'deleted'
		], [
			'idea_id' => $idea['idea_id']
		]);
	}

	public function deny($idea_id, array $user, $message = null)
	{
		$idea = $this->get_absolutely($idea_id);
		if ($idea['status'] === 'draft' ||
			$idea['status'] === 'duplicate' ||
			$idea['status'] === 'refused'
		) {
			return;
		}
		if ($idea['status'] === 'experiment') {
			throw new InvalidArgumentException(sprintf('L\'idée "%s" a été retenue, vous ne pouvez plus la refuser.', $idea['name']));
		}

		if ($message and $user['user_id'] != $idea['user_id']) {
			(new Message())->send_message([
				'from_user_id' => $user['user_id'],
				'to_user_id'   => $idea['user_id'],
				'content'      => sprintf('Votre idée "%s" a été refusée pour la/les raisons suivantes :' . "\n" . '%s', $idea['name'], $message)
			]);
		}

		$this->update([
			'status' => 'refused'
		], [
			'idea_id' => $idea['idea_id']
		]);
	}

	public function validate($idea_id, array $user)
	{
		$idea = $this->get_absolutely($idea_id);
		if ($idea['status'] === 'refused') {
			return;
		}
		if ($idea['status'] !== 'validation') {
			throw new InvalidArgumentException(sprintf('L\'idée "%s" n\'est pas au bon statut, vous ne pouvez pas la valider.', $idea['name']));
		}

		$operators = $this->get_operators($idea_id);
		$users_id  = [];
		foreach ($operators as $operator) {
			if ((int)$operator['user_id'] !== (int)$user['user_id']) {
				$users_id[] = $operator['user_id'];
			}
		}
		if ($idea["idea_category_id"]) {
			$cat = (new IdeaCategory)->get_one('idea_category_id', 'idea_category_id = ' . $idea["idea_category_id"]);
		}
		if (isset($cat) and !empty($cat) and $cat["user_id"]) {
			$users_id[] = $cat['user_id'];
		}
		$this->update_operators($idea_id, $users_id, $idea['user_id']);

		if (NEED_VALIDATION) {
			if ($user['user_id'] != $idea['user_id']) {
				(new Message())->send_message([
					'from_user_id' => $user['user_id'],
					'to_user_id'   => $idea['user_id'],
					'content'      => sprintf('Votre idée "%s" a été validée', $idea['name'])
				]);
			}
		}

		$this->update([
			'status' => 'study'
		], [
			'idea_id' => $idea['idea_id']
		]);
	}

	public function waiting($idea_id, array $user, $message = null)
	{
		$idea = $this->get_absolutely($idea_id);
		if ($idea['status'] === 'study') {

			$this->update([
				'status' => 'waiting'
			], [
				'idea_id' => $idea['idea_id']
			]);
		} else {
			if ($idea['status'] === 'validation' ||
				$idea['status'] === 'draft' ||
				$idea['status'] === 'waiting' ||
				$idea['status'] === 'estimate'
			) {
				return;
			} else {
				throw new InvalidArgumentException(sprintf('L\'idée "%s" a été retenue, vous ne pouvez plus la passer en à l\'étude.', $idea['name']));
			}
		}
	}

	public function duplicate($idea_id, array $user, $message = null)
	{
		$idea = $this->get_absolutely($idea_id);
		if ($idea['status'] === 'study' ||
			$idea['status'] === 'waiting' ||
			$idea['status'] === 'estimate'
		) {

			$this->update([
				'status' => 'duplicate'
			], [
				'idea_id' => $idea['idea_id']
			]);
		} else {
			if ($idea['status'] === 'validation' ||
				$idea['status'] === 'draft'
			) {
				return;
			} else {
				throw new InvalidArgumentException(sprintf('L\'idée "%s" a été retenue, vous ne pouvez plus la passer en doublon.', $idea['name']));
			}
		}
	}

	public function estimate($idea_id, array $user, $message = null)
	{
		$idea = $this->get_absolutely($idea_id);
		if ($idea['status'] === 'waiting') {

			$this->update([
				'status' => 'estimate'
			], [
				'idea_id' => $idea['idea_id']
			]);
		} else {
			if ($idea['status'] === 'validation' ||
				$idea['status'] === 'draft' ||
				$idea['status'] === 'study' ||
				$idea['status'] === 'estimate'
			) {
				return;
			} else {
				throw new InvalidArgumentException(sprintf('L\'idée "%s" a été retenue, vous ne pouvez plus la passer en en cours d\'estimation.', $idea['name']));
			}
		}
	}

	public function confirm($idea_id, array $user, $message = null)
	{
		$idea = $this->get_absolutely($idea_id);
		if ($idea['status'] === 'study' ||
			$idea['status'] === 'waiting' ||
			$idea['status'] === 'estimate'
		) {

			$this->update([
				'status' => 'confirmed'
			], [
				'idea_id' => $idea['idea_id']
			]);
		} else {
			if ($idea['status'] === 'validation' ||
				$idea['status'] === 'draft' ||
				$idea['status'] === 'confirmed' ||
				$idea['status'] === 'duplicate' ||
				$idea['status'] === 'refused'
			) {
				return;
			} else {
				throw new InvalidArgumentException(sprintf('L\'idée "%s" a été retenue, vous ne pouvez plus la passer en en cours d\'estimation.', $idea['name']));
			}
		}
	}

	public function seed()
	{
		$data = [
			'idea_category_id' => 3
		];
		foreach (range(1, 15) as $i) {
			$this->create(array_merge($data, [
				'idea_id'     => $i,
				'name'        => 'Idée ' . $i,
				'user_id'     => rand(1, 4),
				'description' => $this->get_lorem(),
			]));
		}
	}

	public function rate($idea_id, $user_id, $rating)
	{
		return (new ObjectRating())->add_rating($this, $idea_id, $user_id, $rating);
	}

	public function get_user_rate($idea_id, $user_id)
	{
		return (new ObjectRating())->get_user_rate($this, $idea_id, $user_id);
	}

	public function has_rated($idea_id, $user_id)
	{
		throw new Exception('Deprecated');

		return (new ObjectRating())->has_rated($this, $idea_id, $user_id);
	}

	public function get_rating_avg($idea_id, &$count = null)
	{
		return (new ObjectRating())->get_avg($this->table_name, $idea_id, $count);
	}

	public function callback_create(&$data = [])
	{
		\Cameleon\Services::get_acl()->set_ACL($data['user_id'], $data['key'], [
			MaskBuilder:: MASK_EDIT,
			MaskBuilder::MASK_DELETE,
		]);
		$user = (new User)->no_depth()->get_absolutely($data['user_id']);

		if (NEED_VALIDATION and isset($data['status']) and $data['status'] == 'validation') {
			(new Mail)->send(['email' => WEBMASTER_EMAIL], 'newidea.html.twig', [
				'name'        => $data['name'],
				'user'        => $user['name'],
				'description' => $data['description']]);
		}
	}

	public function callback_update($datas = [], $where = [])
	{
		$old_idea = $this->get_absolutely($where['idea_id']);
		if (isset($old_idea['status']) and $old_idea['status'] != 'draft' and $old_idea['status'] != 'validation' and isset($datas['idea_id']) && isset($datas['operators'])) {
			$this->update_operators($where['idea_id'], $datas['operators']);
		}
		if (isset($datas['status']) and $datas['status'] == 'study') {
			(new Feed())->create([
				'user_id'      => $old_idea['user_id'],
				'object_table' => 'idea',
				'object_id'    => $datas['idea_id'],
				'action_type'  => 'create',
			]);
			(new NotificationSubscription())->follow($old_idea['user_id'], $datas['key'], 'all');
			if (NEED_VALIDATION) {
				$user = (new User)->no_depth()->get_absolutely($old_idea['user_id']);
				(new Mail)->send(['email' => $user['email']], 'validated.html.twig', ['name'    => $user['first_name'],
																					  'title'   => $old_idea['name'],
																					  'idea_id' => $old_idea['idea_id']]);
			}
		}
		if (isset($datas['status']) and $datas['status'] == 'validation') {
			$user = (new User)->no_depth()->get_absolutely($old_idea['user_id']);
			(new Mail)->send(['email' => WEBMASTER_EMAIL], 'newidea.html.twig', ['name'        => $old_idea['name'],
																				 'user'        => $user['name'],
																				 'description' => $old_idea['description']]);
		}
	}

	public function callback_pre_update(array &$data, array $where)
	{
		$old_idea = $this->get_absolutely($where['idea_id']);
		if (isset($data['user_id'])) {
			if ((int)$old_idea['user_id'] !== (int)$data['user_id']) {
				$acl = \Cameleon\Services::get_acl();
				$acl->set_ACL($old_idea['user_id'], 'idea/' . $where['idea_id'], []);
				$acl->set_ACL($data['user_id'], 'idea/' . $where['idea_id'], [
						MaskBuilder::MASK_EDIT,
						MaskBuilder::MASK_DELETE,
					]
				);
			}
		}
		if (isset($old_idea['status']) and isset($data['status']) && $old_idea['status'] != $data['status'] and $data['status'] != 'validation' and $data['status'] != 'study' and $data['status'] != 'draft') {
			$user = (new User)->no_depth()->get_absolutely($old_idea['user_id']);
			if ($data['status'] != 'refused' && $data['status'] != 'deleted') {
				(new Mail)->send(['email' => $user['email']], 'status.html.twig', ['firstname' => $user['first_name'],
																				   'title'     => $old_idea['name'],
																				   'idea_id'   => $old_idea['idea_id']]);
			}
		}
		if (isset($data['idea_category_id']) and empty($data['idea_category_id'])) {
			unset($data['idea_category_id']);
		}
	}

	public function get_operators($idea_id)
	{
		return (new User)->readall('SELECT u.* FROM users u INNER JOIN ideas_operators io ON io.user_id = u.user_id WHERE io.idea_id = ' . intval($idea_id));
	}

	public function update_operators($idea_id, array $users_id, $sender_id = null)
	{
		foreach ($users_id as $key => $user_id) {
			if (!$user_id) {
				unset($users_id[$key]);
			}
		}
		$current_operators    = $this->get_operators($idea_id);
		$acl                  = \Cameleon\Services::get_acl();
		$current_operators_id = [];
		$db                   = $this->get_db();
		foreach ($current_operators as $current_operator) {
			$current_operators_id[] = $current_operator['user_id'];
			if (!in_array($current_operator['user_id'], $users_id)) {
				$acl->set_ACL($current_operator['user_id'], 'idea/' . $idea_id, []);
				$db->delete('ideas_operators', [
					'user_id' => $current_operator['user_id'],
					'idea_id' => $idea_id
				]);
			}
		}
		$users_to_notify = [];
		foreach ($users_id as $user_id) {
			if (!in_array($user_id, $current_operators_id)) {
				$acl->set_ACL((int)$user_id, 'idea/' . $idea_id, [MaskBuilder::MASK_OPERATOR]);
				$db->insert('ideas_operators', [
					'user_id' => $user_id,
					'idea_id' => $idea_id
				]);
				$users_to_notify[] = ['user_id' => $user_id];
			}
		}
		if (count($users_to_notify) > 0) {
			$me = \Cameleon\Services::get_me();
			if (!$sender_id) {
				$sender_id = $me['user_id'];
			}
			(new Notification())->notify($sender_id, 'idea/' . $idea_id, 'operator', null, null, $users_to_notify);
			foreach ($users_to_notify as $user_to_notify) {
				$user   = (new User)->no_depth()->get_absolutely($user_to_notify['user_id']);
				$idea   = (new Idea)->no_depth()->get_absolutely($idea_id);
				$author = (new User)->no_depth()->get_absolutely($idea['user_id']);
				(new Mail)->send(['email' => $user['email']], 'notifypilot.html.twig', [
					'firstname'   => $user['first_name'],
					'title'       => $idea['name'],
					'author'      => $author['name'],
					'description' => $idea['description'],
					'user_id'     => $author['user_id'],
					'idea_id'     => $idea['idea_id']
				]);
			}
		}
	}

	public function add_operators($idea_id, array $users_id)
	{
		foreach ($users_id as $key => $user_id) {
			if (!$user_id) {
				unset($users_id[$key]);
			}
		}
		$current_operators    = $this->get_operators($idea_id);
		$acl                  = \Cameleon\Services::get_acl();
		$current_operators_id = [];
		$db                   = $this->get_db();
		foreach ($current_operators as $current_operator) {
			$current_operators_id[] = $current_operator['user_id'];
		}
		$users_to_notify = [];
		foreach ($users_id as $user_id) {
			if (!in_array($user_id, $current_operators_id)) {
				$acl->set_ACL((int)$user_id, 'idea/' . $idea_id, [MaskBuilder::MASK_OPERATOR]);
				$db->insert('ideas_operators', [
					'user_id' => $user_id,
					'idea_id' => $idea_id
				]);
				$users_to_notify[] = ['user_id' => $user_id];
			}
		}
		if (count($users_to_notify) > 0) {
			$me        = \Cameleon\Services::get_me();
			$sender_id = $me['user_id'];
			(new Notification())->notify($sender_id, 'idea/' . $idea_id, 'operator', null, null, $users_to_notify);
			foreach ($users_to_notify as $user_to_notify) {
				$user   = (new User)->no_depth()->get_absolutely($user_to_notify['user_id']);
				$idea   = (new Idea)->no_depth()->get_absolutely($idea_id);
				$author = (new User)->no_depth()->get_absolutely($idea['user_id']);
				(new Mail)->send(['email' => $user['email']], 'notifypilot.html.twig', [
					'firstname'   => $user['first_name'],
					'title'       => $idea['name'],
					'author'      => $author['name'],
					'description' => $idea['description'],
					'user_id'     => $author['user_id'],
					'idea_id'     => $idea['idea_id']
				]);
			}
		}
	}

	public function callback_delete($where = [])
	{
		$id_name = $this->table_name . '_id';
		if (isset($where[$id_name]) && $where[$id_name]) {
			(new Feed())->object_deleted($this->table_name, $where[$id_name]);
			(new NotificationSubscription())->context_deleted($this->table_name, $where[$id_name]);
			(new Notification())->context_deleted($this->table_name, $where[$id_name]);
		}
	}

	public function get_export($filters = [])
	{
		$db = $this->get_db();
		if (!empty($filters)) {
			if ((count($filters) == 1 && isset($filters['ideas.idea_id'])) || (count($filters) == 1 && isset($filters['operator']))) {
				$select = 'SELECT ideas.idea_id ';
			} else {
				$select = 'SELECT ideas.idea_id, ';
			}
			$joins = '';
			$i     = 1;
			foreach ($filters as $filter => $value) {
				if ($filter == 'ideas.idea_id') {
					$i++;
					continue;
				}
				if ($i == count($filters) && $filter != 'operator') {
					$select .= $filter;
				} elseif ($filter != 'operator') {
					$select .= $filter . ', ';
				}
				$i++;
				if ($filter == 'idea_categorys.name') {
					$joins .= 'INNER JOIN idea_categorys ON ideas.idea_category_id = idea_categorys.idea_category_id';
				} elseif ($filter == 'users.first_name' || ($filter == 'users.last_name' && !isset($filters['users.first_name'])) || ($filter == 'users.email' && !isset($filters['users.first_name']) && !isset($filters['users.last_name']))) {
					$joins .= ' INNER JOIN users ON ideas.user_id = users.user_id';
				}
			}
			$sql = $db->query_format($select . ' FROM ideas ' . $joins . ' ORDER BY ideas.idea_id DESC');
		} else {
			$sql = $db->query_format('SELECT ideas.idea_id , idea_categorys.name , ideas.name , ideas.description , users.first_name , users.last_name , users.email , ideas.created_at , ideas.status
                               FROM ideas
                               INNER JOIN idea_categorys ON ideas.idea_category_id = idea_categorys.idea_category_id
                               INNER JOIN users ON ideas.user_id = users.user_id
                               ORDER BY ideas.idea_id DESC');
		}

		$result = $db->db_query($sql);
		$items = [];
		while ($item = $result->fetch_array(MYSQL_NUM))  {
			$items[] = $item;
		}

		return $items;
	}

	public function get_operator()
	{
		$db        = $this->get_db();
		$sql       = $db->query_format('SELECT idea_id, users.first_name, users.last_name, users.email
                                  FROM ideas_operators
                                  INNER JOIN users ON users.user_id = ideas_operators.user_id');
		$result = $db->db_query($sql);
		$operators = [];
		while ($operator = $result->fetch_array()) {
			$operators[$operator[0]][] = array('Prénom' => $operator[1], 'Nom' => $operator[2], 'Email' => $operator[3]);
		}

		return $operators;
	}

	public function get_max_count_operator()
	{
		$db              = $this->get_db();
		$sql             = $db->query_format('SELECT idea_id, count(*)
                                  FROM ideas_operators
                                  GROUP BY idea_id
                                  HAVING count(*) >= ALL (SELECT count(*)
                                                          FROM ideas_operators
                                                          GROUP BY idea_id)');
		$result = $db->db_query($sql);

		return $result->fetch_array()[1];
	}

	/**
	 * Retourne la liste des catégories actives dans les ideas
	 *
	 * @return array
	 */
	public function get_distinct_categories_idea()
	{
		$db     = $this->get_db();
		$sql    = $db->query_format('SELECT DISTINCT idea_categorys.idea_category_id, idea_categorys.name
                                  FROM ideas
                                  INNER JOIN idea_categorys ON idea_categorys.idea_category_id = ideas.idea_category_id');
		$items  = [];
		$result = $db->db_query($sql);
		while ($categorie = $result->fetch_array()) {
			$items[$categorie[0]] = $categorie[1];
		}

		return $items;
	}
}