<?php

trait TPicture
{
	public function format_picture(array $row, $placeholder = null)
	{
		if (isset($row['file_picture_id']) && $row['file_picture_id'] && ($picture = (new File())->get_current($row['file_picture_id']))) {

			$row['picture'] = $picture;
		} else {
			if (null === $placeholder) {
				throw new InvalidArgumentException('Missing placeholder');
			}
			$row['picture'] = [
				'url' => $placeholder,
			];
		}

		return $row;
	}

	public function format_picture_old(array $row, $placeholder = null)
	{
		list($object_table, $object_id) = explode('/', $row['key']);
		$pictures = (new File())->get_array(1, ['created_at' => 'DESC', 'file_id' => 'DESC'], [
			'object_table' => $object_table,
			'object_id'    => $object_id
		]);

		if (count($pictures) > 0) {
			$row['picture'] = reset($pictures);
		} else {
			$row['picture'] = [
				'url' => $placeholder,
			];
		}

		return $row;
	}
}