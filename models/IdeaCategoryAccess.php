<?php

class IdeaCategoryAccess extends \Cameleon\Model\AModel
{
	public $fields = [
		'idea_category_id' => 'reference',
		'user_id'          => 'reference',
		'jobposition_id'   => 'reference',
        'location'         => 'reference',
	];

	public $indexes = [
		'idea_category_id' => ['idea_category_id'],
		'user_id'          => ['user_id'],
		'jobposition_id'   => ['jobposition_id'],
	];

	public function format(array $row)
	{
		$row['idea_category'] = (new IdeaCategory())->get_absolutely($row['idea_category_id']);

		if ($row['user_id']) {
			$row['user'] = (new User)->get_absolutely($row['user_id']);
		}
		if ($row['jobposition_id']) {
			$row['jobposition'] = (new JobPosition())->get_absolutely($row['jobposition_id']);
		}

		return parent::formatsuperficially($row);
	}

	public function seed()
	{
		$users = (new User)->get_array('all');
		$categories = (new IdeaCategory())->get_array('all');
		$db = $this->get_db();
		$x = 1;
		foreach ($users as $user) {
			foreach ($categories as $category) {
				$this->create([
					'ideacategoryaccess_id' => $x++,
					'user_id'          => $user['user_id'],
					'idea_category_id' => $category['idea_category_id']
				]);
			}
		}
	}

    public function get_categories_auhenticate()
    {
        $db = $this->get_db();
        $sql = $db->query_format('SELECT DISTINCT idea_categorys.idea_category_id, idea_categorys.name
                                  FROM ideacategoryaccesss
                                  INNER JOIN idea_categorys ON idea_categorys.idea_category_id = ideacategoryaccesss.idea_category_id');
		$result = $db->db_query($sql);

        $items = [];
		while ($row = $result->fetch_row()) {
			$items[$row[0]] = $row[1];
		}

        return $items;
    }
}