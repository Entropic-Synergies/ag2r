<?php


class Milestone extends \Cameleon\Model\AModel
{
	use TSocial;

	public $commentable = true;
	public $likable = true;

	public $fields = [
		'user_id'            => 'int', // owner
		'experimentation_id' => 'int',
		'description'        => 'text',
		'due_date'           => 'date',
		'status'             => ['open', 'closed'],
		'position'           => 'int', // ordered by due_date
	];

	public $indexes = [
		'experimentation_id' => ['experimentation_id'],
	];

	public function seed_from_experimentation($experimentation, $nb = 1)
	{
		$innovation_id = $experimentation['innovation_id'];
		$experimentation_id = $experimentation['experimentation_id'];
		$data = [
			'experimentation_id' => $experimentation_id,
		];
		foreach (range(1, $nb) as $i) {
			$this->create(array_merge($data, [
				'name'        => 'Innov#' . $innovation_id . ' XP#' . $experimentation_id . ' Jalon#' . $i,
				'user_id'     => rand(1, 4),
				'description' => $this->get_lorem(),
				'status'      => 'open',
				'due_date'    => (new \DateTime())->add(new \DateInterval('P' . rand(1, 20) . 'D')),
			]));
		}
	}

	public function format(array $row)
	{
		//$row = $this->format_social($row);
		return $row;
	}

	public function callback_create(&$data = [])
	{
		(new Feed())->create([
			'user_id'       => $data['user_id'],
			'object_table'  => 'milestone',
			'object_id'     => $data['milestone_id'],
			'context_table' => 'experimentation',
			'context_id'    => $data['experimentation_id'],
			'action_type'   => 'create',
		]);

		$this->reorder_position($data['experimentation_id']);

		(new Notification())->notify($data['user_id'], 'experimentation/' . $data['experimentation_id'], 'create_milestone', $data['key']);
	}

	public function callback_update($datas = [], $where = [])
	{
		if (isset($datas['due_date']) and isset($where['milestone_id'])) {
			$milestone = $this->get_current($where['milestone_id']);
			$this->reorder_position($milestone['experimentation_id']);
		}
	}

	public function reorder_position($experimentation_id)
	{
		$milestones = $this->get_array(null, ['due_date' => 'ASC'], ['experimentation_id' => $experimentation_id]);
		$x = 1;
		foreach ($milestones as $milestone) {
			$this->update([
				'position' => $x++
			], [
				'milestone_id' => $milestone['milestone_id']
			]);
		}
	}
}