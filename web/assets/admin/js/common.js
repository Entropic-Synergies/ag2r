
$('table th input:checkbox').on('click', function () {
	var that = this;
	$(this).closest('table').find('tr > td:first-child input:checkbox')
		.each(function () {
			this.checked = that.checked;
			$(this).closest('tr').toggleClass('selected');
		});

});

function gotoUrl(path, params, method) {
    method = method || "post";
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    if (typeof params === 'string') {
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", 'data');
        hiddenField.setAttribute("value", params);
        form.appendChild(hiddenField);
    }
    else {
        for (var key in params) {
            if (params.hasOwnProperty(key)) {
                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", key);
                if(typeof params[key] === 'object')
                    hiddenField.setAttribute("value", JSON.stringify(params[key]));
                else
                    hiddenField.setAttribute("value", params[key]);
                form.appendChild(hiddenField);
            }
        }
    }
    document.body.appendChild(form);
    form.submit();
}

$('.workflow').change(function () {
	var message = '';
	if ($(this).val() == 'refused') {
		var idea_id = $(this).data("id");
		$('#refuseModalBtn').click(function () {
			message = $('#refuseModalMsg').val();
			gotoUrl('/admin/idea/apply_action/', {'action':'refused', 'selection[]':idea_id, 'message':message});
		});
		$('#refuseModal').modal({
			backdrop: true,
			keyboard: true
		});
		$('#refuseModal').on('hidden.bs.modal', function (e) {
			location.reload();
		})
	}
	else if ($(this).data("object") == 'idea') {
		var idea_id = $(this).data("id");
		$('#statusUpdateCommentModalAction').val($(this).val());
		$('#statusUpdateCommentModalId').val($(this).data("id"));
		$('#statusUpdateCommentModalBtn').click(function () {
			message = $('#statusUpdateCommentModalMsg').val();
			action = $('#statusUpdateCommentModalAction').val();
			id = $('#statusUpdateCommentModalId').val();
			gotoUrl('/admin/idea/apply_action/', {'action':action, 'selection[]':id, 'message':message});
		});
		$('#statusUpdateCommentModal').modal({
			backdrop: true,
			keyboard: true
		});
		$('#statusUpdateCommentModal').on('hidden.bs.modal', function (e) {
			location.reload();
		})
	} else
		gotoUrl('/admin/' + $(this).data("object") + '/apply_action/', {'action':$(this).val(), 'selection[]':$(this).data("id"), 'message':message});
});

$('.ideavalidate').click(function (e) {
	e.preventDefault();
	var message = '';
	gotoUrl('/admin/validation/apply_action/', {'action':'accept', 'selection[]':$(this).data("id"), 'message':message});
});

$('.idearefuse').click(function (e) {
	e.preventDefault();
	var idea_id = $(this).data("id");
	$('#refuseModalBtn').click(function () {
		message = $('#refuseModalMsg').val();
		gotoUrl('/admin/validation/apply_action/', {'action':'refused', 'selection[]':idea_id, 'message':message});
	});
	$('#refuseModal').modal({
		backdrop: true,
		keyboard: true
	});
});