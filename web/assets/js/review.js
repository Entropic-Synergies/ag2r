var Review = function ($context) {
	this.$context = $context;
	this.init();
};

Review.prototype = {
	init: function () {
		this.$context.find(".star-rating")
			.each(function () {
				var $rating = $(this),
					rating = Math.round(parseFloat($rating.attr('rating')));

				function displayCurrentRating() {
					var i = 0;
					$rating.find("li").each(function () {
						if (i < rating) {
							$(this).addClass('active');
						} else {
							$(this).removeClass('active');
						}
						i++;
					});
				}

				displayCurrentRating();

				$rating.hover(function () {
				}, displayCurrentRating);

				if ($rating.hasClass("ratable")) {
					$rating.find("li").hover(function () {
						$(this).addClass("active").prevAll().addClass("active");
						$(this).addClass("active").nextAll().removeClass("active");
					}).click(
						function (e) {
							e.preventDefault();
							var new_rating = $rating.find("li").index($(this)) + 1;
							$.ajax({
								url: "/idea/rate",
								type: "post",
								data: {
									idea_id: $rating.closest(".one-idea").attr("ref"),
									rating: new_rating
								},
								success: function (resp) {
									console.log(resp);
									$rating.closest('.action-rating').replaceWith(resp);
								}
							})
						}
					);
				}
			});
	}
};