(function ($) {
	$.fn.AG2RLike = function (opts) {
		var options = $.extend({
			like_label: 'J\'aime',
			unlike_label: 'Je n\'aime plus',
			object_attr: 'oid',
			sub_target: 'a',
			like_url: '/like/like',
			unlike_url: '/like/unlike',
			nb_likes_target: '.nb-likes'
		}, opts);

		this.each(function () {
			var $like = $(this),
				key = $like.attr(options.object_attr);
			if (!key) {
				throw new Error('Missing object ID');
			}
			$like.on('click', options.sub_target, function (e) {
				e.preventDefault();
				var $a = $(this);
				var url = $a.find('.lbl').text() == options.like_label ? options.like_url : options.unlike_url;
				$.ajax({
					url: url,
					type: 'post',
					data: {
						'object': key
					},
					success: function (resp) {
						var $count = $like.find(options.nb_likes_target);
						$count.text(parseInt($count.text()) + (url === options.like_url ? 1 : -1));
						$a.find('.lbl').text(url === options.like_url ? options.unlike_label : options.like_label);
					}
				});
			});
		});

		return this;
	};
}(jQuery));