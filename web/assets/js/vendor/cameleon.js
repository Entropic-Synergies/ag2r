/* ----------
	  FEED
   ---------- */

function cameleon_feed_display (jquery_target)
{
	$.ajax({
		url: '/feed/all?hide_layout=1&channel=' + $(jquery_target).attr('oid'),
		beforeSend: function(xhr) {	$(jquery_target).html('<center><img src="/assets/img/cameleon/feed_loader.gif"></center>'); },
		success: function(html) {
			$(jquery_target).html(html);
			cameleon_like_display();
			cameleon_rating_display();
		},
	});
}

function cameleon_feed_add (callback_success)
{
	$.ajax({
		url: '/feed/new',
		type: 'POST',
		data: {
			channel: $('#feed_form_channel').val(),
			content: $('#feed_form_content').val(),
		},
		success: function() { callback_success(); },
	});
}

/* ------------
	  RATING
   ------------ */

function cameleon_rating_display ()
{
	// first loop, just quickly display empty stuff
	$('.action_rating').html('<ul class="star-rating"><li><a>1</a></li><li><a>2</a></li><li><a>3</a></li><li><a>4</a></li><li><a>5</a></li></ul>');
	// second loop refresh our rating with real values ...
	$('.action_rating').each(function(index, value){
		cameleon_rating_refresh($(this).attr('id'));
	});
}

function cameleon_rating_refresh (dom_id)
{
	$.ajax({
		url: "/rating/get?object_id=" + $('#' + dom_id).attr('oid'),
		dataType: 'json',
		success: function(json){
			n_stars = Math.round(json.rate / 20);
			html = '<ul class="star-rating">';
			for (var i = 1; i <= 5; i++) {
				class_active = '';
				val = i * 20;
				if (i<=n_stars) {class_active = ' class="active"';};
				html += '<li' + class_active + '><a onclick="cameleon_rating_add(\'' + dom_id + '\', ' + val + ');">' + i + '</a></li>';
			};
			html += '</ul>';
			$('#' + dom_id).html(html);
		}
	});
}

function cameleon_rating_add (dom_id, val)
{
	$.ajax({
		url: "/rating/post",
		data: {
			object_id: $('#' + dom_id).attr('oid'),
			val: val,
		},
		type: "POST",
		beforeSend: function(){
			$('#' + dom_id).html('<img class="loader" src="/assets/img/cameleon/rating_loader.gif">');
		},
		success: function(){
			cameleon_rating_refresh (dom_id);
		}
	});
}

$(document).ready(function(){ cameleon_rating_display(); });




/* ------------
	  LIKE
   ------------ */

function cameleon_like_display ()
{
	// first loop, just quickly display empty stuff
	$('.action_like').html('<a><i class="icon-heart"></i> J\'aime</a>');
	// second loop refresh our rating with real values ...
	$('.action_like').each(function(index, value){
		cameleon_like_refresh($(this).attr('id'));
	});
}

function cameleon_like_refresh (dom_id)
{
	console.log('refresh:' + dom_id);
	$.ajax({
		url: "/like/get?object_id=" + $('#' + dom_id).attr('oid'),
		dataType: 'json',
		success: function(json){
			if(json.n_likes == 0){
				html = '<a onclick="cameleon_like_add(\'' + dom_id + '\');"><i class="icon-heart"></i> J\'aime</a>';
			}else{
				html = '<a onclick="cameleon_like_add(\'' + dom_id + '\');"><i class="icon-heart"></i> J\'aime (' + json.n_likes + ')</a>';
			}
			$('#' + dom_id).html(html);
		}
	});
}

function cameleon_like_add (dom_id)
{
	console.log('add:' + dom_id);
	$.ajax({
		url: "/like/post",
		data: {
			object_id: $('#' + dom_id).attr('oid'),
		},
		type: "POST",
		beforeSend: function(){
			$('#' + dom_id).html('<img class="loader" src="/assets/img/cameleon/rating_loader.gif">');
		},
		success: function(){
			cameleon_like_refresh (dom_id);
		}
	});
}

$(document).ready(function(){ cameleon_like_display(); });



