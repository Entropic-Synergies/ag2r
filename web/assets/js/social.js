var AG2RSocial = function ($target) {
	this.$target = $target;
	this.key = $target.attr("oid");
};

AG2RSocial.prototype = {
	bind: function () {
		var self = this;
		this.$target.find("form").submit(function (e) {
			e.preventDefault();
			return self.onFormSubmit($(this), e);
		});

		// see comment => show message
		this.$target.find('.comment a').bind("click", function (e) {
			e.preventDefault();
			var $a = $(this);
			self.loadComments(function () {
				$a.parents(".one-social").eq(0).find(".comment-form textarea").focus();
			}, true);
		});

		this.$target.find('.like').AG2RLike();

		this.$target.find('.list-comments a').bind("click", function (e) {
			e.preventDefault();
			self.loadComments();
		});

		// delete => remove message
		this.$target.on("click", ".close", function (e) {
			e.preventDefault();
			self.deleteComment($(this), e);
		});

		this.$target.on("click", ".edit", function (e) {
			e.preventDefault();
			self.editComment($(this), e);
		});

		this.$target.parent().eq(0).on("click", ".cameleon_feed_edit", function (e) {
			e.preventDefault();
			self.editFeed($(this), e);
		});

		this.$target.parent().eq(0).on("click", ".cameleon_feed_close", function (e) {
			e.preventDefault();
			self.deleteFeed($(this), e);
		});

		this.$target.find('.report-abuse').click(function (e) {
			e.preventDefault();
			var $modal = $("#report-modal");
			if (!$modal.length) {
				alert("Missing report modal");
			}
			$modal.modal("show");
			$modal.find(":input[name=object]").val(self.key);
			$modal.find(":input[name=message]").val("");
		});
		this.$target.find('.share').click(function (e) {
			e.preventDefault();
			var $modal = $("#share-modal");
			if (!$modal.length) {
				alert("Missing share modal");
			}
			$modal.modal("show");
			$modal.find(":input[name=oid]").val(self.key);
			$modal.find(":input[name=comment]").val("");
		});
	},

	editFeed: function ($a) {
		var id = $a.attr('href').substring(1);
		$.ajax({
			url: '/feed/' + id + '?edit',
			success: function (resp) {
				var $content = $a.closest('.cameleon_feed').find('.cameleon_feed_text');
				var $textarea = $('<textarea rows="1" class="pull-left" style="width: 483px;padding: 8px 6px;">' + resp + '</textarea><input type="submit" class="btn pull-left push-l" value="Modifier">', {
					value: resp
				});
				
				var $form = $('<form class="clearfix"/>');
				$form.submit(function (e) {
					e.preventDefault();
					$.ajax({
						url: '/feed/' + id + '?edit',
						type: 'POST',
						data: {
							comment: $textarea.val()
						},
						success: function (resp) {
							$content.html(resp);
						}
					});
				});
				$textarea.appendTo($form);				
				$content.html('').append($form);
				$textarea.focus();
			}
		});
	},

	deleteFeed: function ($a) {
		var self = this;
		$.ajax({
			url: $a.attr("href"),
			success: function (resp) {
				console.log('delete ok');
				//self.refreshFeeds(resp);
			}
		});

		$a.closest('.cameleon_feed').fadeOut('slow', function () {
			// if siblings
			if ($(this).siblings().size() > 0) {
				$(this).remove();
			}
			// else remove container + inform user
			else {
				//$(this).parent().siblings('.one-social').find('.list-comments a').text('Aucun feed');
				//$(this).parent().remove();
			}
		});
	},

	deleteComment: function ($a) {
		var self = this;
		$.ajax({
			url: $a.attr("href"),
			success: function (resp) {
				self.refreshComments(resp);
				self.updateCounter(self.$target.find(".nb-comments"), -1);
			}
		});


		$(this).closest('li').fadeOut('slow', function () {
			// if siblings
			if ($(this).siblings().size() > 0) {
				$(this).remove();
			}
			// else remove container + inform user
			else {
				$(this).parent().siblings('.one-social').find('.list-comments a').text('Aucun commentaire');
				$(this).parent().remove();
			}
		});
	},

	editComment: function ($a) {
		var id = $a.attr('href').substring(1);
		$.ajax({
			url: '/comment/' + id + '?edit',
			success: function (resp) {
				var $content = $a.closest('li').find('p.comment-text');
				var $textarea = $('<textarea rows="1" class="pull-left" style="width: 483px;padding: 8px 6px;">' + resp + '</textarea><input type="submit" class="btn pull-left push-l" value="Modifier">');
				
				var $form = $('<form class="clearfix"/>');
				$form.submit(function (e) {
					e.preventDefault();
					$.ajax({
						url: '/comment/' + id + '?edit',
						type: 'POST',
						data: {
							comment: $textarea.val()
						},
						success: function (resp) {
							$content.html(resp);
						}
					});
				});
				$textarea.appendTo($form);				
				$content.html('').append($form);
				$textarea.focus();
			}
		});


	},

	loadComments: function (callback, force_display) {
		var self = this;
		var $dest = this.$target.find(".comments-container");
		if (force_display && $dest.is(":visible")) {
			callback.apply(self);
			return;
		}
		if ($dest.is(":visible")) {
			$dest.slideUp();
		} else {
			$.ajax({
				url: "/comment/all",
				data: {
					object: this.key
				}, success: function (resp) {
					$dest.slideDown();
					self.refreshComments(resp);
					if (callback) {
						callback.apply(self);
					}
				}
			});
		}
	},

	onFormSubmit: function ($form) {
		var self = this,
			$input = $form.find(":input[name=comment]");
		$input.attr("disabled", true);
		$.ajax({
			url: $form.attr('action'),
			type: $form.attr('method'),
			data: {
				"comment": $input.val()
			},
			success: function (resp) {
				if (self.$target.find(".nbcomments").length === 0) {
					self.$target.find('.list-comments a').html('<i class="custom-brown_44"></i>Voir les commentaires (<span class="nbcomments">0</span>)');
				}
				self.updateCounter(self.$target.find(".nbcomments"), 1);
				self.refreshComments(resp);
				$input.val("");
			},
			complete: function () {
				$input.removeAttr("disabled");
			}
		});
	},

	refreshComments: function (resp) {
		var $dest = this.$target.find(".comments-container .comments");
		$dest.html(resp);
		$dest.find(".like").AG2RLike();
	},

	updateCounter: function ($target, way) {
		$target.text(parseInt($target.text()) + way);
	}
};

$(document).ready(function () {
	init_social($("body"));
	$('.like').click(function() {		
		$(this).toggleClass('unlike');
	});
});

function init_social($origin) {
	$origin.find(".social-actions").not('.binded').each(function () {
		$(this).addClass('binded');
		var social = new AG2RSocial($(this));
		social.bind();
	});
}