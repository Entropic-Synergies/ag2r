$(document).ready(function () {

	function bootstrap_tab_bookmark(selector) {
		url = document.location.href.split('#');
		if (url[1] != undefined) {
			$(selector + '[href=#' + url[1] + ']').tab('show');
		}

		var update_location = function (event) {
			document.location.hash = this.getAttribute("href");
		}

		$(selector + "[data-toggle=pill]").click(update_location);
		$(selector + "[data-toggle=tab]").click(update_location);
	}

	bootstrap_tab_bookmark(".tabs ");


	$(".read-more-wr").each(function (){
		$(this).readmore({
			moreLink: '<a href="#">Lire la suite</a>',
			lessLink: '<a href="#">Fermer</a>',
			maxHeight: parseInt($(this).attr('maxheight'))
		});
	});

	$("#live-flashes-container").click(function (e) {
		$("#live-flashes-container").modal("hide");
	}).on('hidden', function () {
			$(this).find("ul").remove();
		})

});

function add_flash_message(type, message) {
	var $container = $("#live-flashes-container");
	var $ul = $container.find(".alert-" + type);
	if ($ul.length === 0) {
		$ul = $("<ul/>", {
			"class": "alert alert-" + type,
			html: "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>"
		}).appendTo($container);
	}

	$("<li/>", {
		"class": type,
		html: message
	}).appendTo($ul);

	$container.modal("show");
}