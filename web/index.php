<?php

if (isset($_SERVER['HTTP_LB_SSL']) && $_SERVER['HTTP_LB_SSL'] === 'on') {
	$_SERVER['HTTPS'] = 'on';
	$_SERVER['SERVER_PORT'] = '443';
}

require __DIR__ . '/../vendor/autoload.php';

$routes = require __DIR__ . '/../config/routes.php';

if (strpos($_SERVER['REQUEST_URI'], '/admin') === 0) {
	$app = new \Cameleon\Admin\AppAdmin();
} else {
	$app = new \Cameleon\App();
}
$app->staging_auth();
$app->routes($routes);
$app->run()->dump();
