<?php
require __DIR__ . '/../vendor/autoload.php';

$app = new \Cameleon\App();

shell_exec('git pull origin master');
shell_exec('php composer.phar install');
shell_exec('./console install');

mail(
	CAMELEON_LEAD_DEV,
	'[' . CAMELEON_PROJECTNAME . '] ' . CAMELEON_ENVIRONMENT . ' > git pull',
	'git pull origin master'
);