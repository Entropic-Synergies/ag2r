insert into `ideas_operators` (`idea_id`, `user_id`) values ("2116", "875");
insert into `ideas_operators` (`idea_id`, `user_id`) values ("1978", "875");
insert into `ideas_operators` (`idea_id`, `user_id`) values ("2407", "875");
insert into `ideas_operators` (`idea_id`, `user_id`) values ("3023", "875");
insert into `ideas_operators` (`idea_id`, `user_id`) values ("3210", "875");
insert into `ideas_operators` (`idea_id`, `user_id`) values ("2595", "875");
insert into `ideas_operators` (`idea_id`, `user_id`) values ("132", "875");
insert into `ideas_operators` (`idea_id`, `user_id`) values ("3852", "883");
-- Adding missing email philippe.quique@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Philippe", "Quique", "philippe.quique@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 8, 0);
-- Adding missing email brigitte.villette@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Brigitte", "Villette", "brigitte.villette@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 17, 0);
-- Adding missing email bernard.lavabre@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Bernard", "Lavabre", "bernard.lavabre@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 26, 0);
-- Adding missing email emilie.thomas@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Emilie", "Thomas", "emilie.thomas@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 17, 0);
-- Adding missing email amelie.potel@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Amélie", "Potel", "amelie.potel@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 12, 0);
-- Adding missing email emmanuel.chazalet@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Emmanuel", "Chazalet", "emmanuel.chazalet@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 13, 0);
-- Adding missing email olivier.theetten@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Olivier", "Theetten", "olivier.theetten@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 29, 0);
-- Adding missing email aurore.petit@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Aurore", "PETIT", "aurore.petit@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 22, 0);
-- Adding missing email jeanfrancois.ropelewski@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Jean-François", "Ropelewski", "jeanfrancois.ropelewski@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", NULL, 0);
-- Adding missing email antonio.deoliveira@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Antonio", "de Oliveira", "antonio.deoliveira@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 24, 0);
-- Adding missing email frederic.nouvian@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "FREDERIC", "NOUVIAN", "frederic.nouvian@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 17, 0);
-- Adding missing email idees@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "idees", "idees", "idees@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 22, 0);
-- Adding missing email michel.bazet@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Michel", "Bazet", "michel.bazet@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 26, 0);
-- Adding missing email philippe.paingris@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Philippe", "Paingris", "philippe.paingris@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 12, 0);
-- Adding missing email corinne.visse@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "corinne", "visse", "corinne.visse@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 22, 0);
-- Adding missing email emilie.paumier@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "emilie", "paumier", "emilie.paumier@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 22, 0);
-- Adding missing email jean-francois.ropelewski@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "jean francois", "ROPELEWSKI", "jean-francois.ropelewski@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 22, 0);
-- Adding missing email lauriane.filliard-saadoune@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Lauriane", "Filliard", "lauriane.filliard-saadoune@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 17, 0);
-- Adding missing email mariefrance.binard@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "marie france", "binard", "mariefrance.binard@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 17, 0);
-- Adding missing email yves-marie.lhenaff@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Yves-Marie", "L'Hénaff", "yves-marie.lhenaff@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 22, 0);
-- Adding missing email guillaume.boisard@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Guillaume", "Boisard", "guillaume.boisard@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 21, 0);
-- Adding missing email hubert.concile@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Hubert", "Concile", "hubert.concile@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 17, 0);
-- Adding missing email yves.bensimon@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Yves", "Bensimon", "yves.bensimon@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 25, 0);
-- Adding missing email marie-claude.fichelle@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "marie-claude", "fichelle", "marie-claude.fichelle@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 12, 0);
-- Adding missing email marielaure.metayer@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Marie-Laure", "Metayer", "marielaure.metayer@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 23, 0);
-- Adding missing email anne-celine.agrapart@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Anne-Céline", "Agrapart", "anne-celine.agrapart@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 12, 0);
-- Adding missing email nathalie.racine@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Nathalie", "Racine", "nathalie.racine@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 8, 0);
-- Adding missing email guy.legoff@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Guy", "Legoff", "guy.legoff@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 29, 0);
-- Adding missing email bertrand.camilli@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Bertrand", "Camilli", "bertrand.camilli@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 19, 0);
-- Adding missing email stephane.lhez@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Stéphane", "Lhez", "stephane.lhez@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 29, 0);
-- Adding missing email sophie.delhorbe@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Sophie", "Delhorbe", "sophie.delhorbe@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 22, 0);
-- Adding missing email gilles.cousin@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "gilles", "cousin", "gilles.cousin@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 12, 0);
-- Adding missing email alexandra.mailliard@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Alexandra", "Mailliard", "alexandra.mailliard@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 13, 0);
-- Adding missing email romuald.arnaud@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "romuald", "arnaud", "romuald.arnaud@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 22, 0);
-- Adding missing email igor.brenner@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Igor", "Brenner", "igor.brenner@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 5, 0);
-- Adding missing email christel.bombardier@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "christel", "bombardier", "christel.bombardier@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 22, 0);
-- Adding missing email gerard.escande@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "ESCANDE", "Gérard", "gerard.escande@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 29, 0);
-- Adding missing email colin.verhille@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Colin", "Verhille", "colin.verhille@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 29, 0);
-- Adding missing email florine.piechocki@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Florine", "Piechocki", "florine.piechocki@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 13, 0);
-- Adding missing email nicole.granghon@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Nicole", "Granghon", "nicole.granghon@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 17, 0);
-- Adding missing email olivier.ayral@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Olivier", "Ayral", "olivier.ayral@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 22, 0);
-- Adding missing email marie.singer@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "marie", "singer", "marie.singer@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 12, 0);
-- Adding missing email olivier.romestan@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "OLIVIER", "ROMESTAN", "olivier.romestan@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 29, 0);
-- Adding missing email christelle.pinto-lance@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Christelle", "Pinto-Lance", "christelle.pinto-lance@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 24, 0);
-- Adding missing email delphine.lalu@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Delphine", "Lalu", "delphine.lalu@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 13, 0);
-- Adding missing email lionel.petit@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Lionel", "Petit", "lionel.petit@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 29, 0);
-- Adding missing email anne.breniaux@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "anne", "breniaux", "anne.breniaux@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 13, 0);
-- Adding missing email monique.roudil@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Monique", "Roudil", "monique.roudil@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 29, 0);
-- Adding missing email benoit.raviart@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Benoît", "Raviard", "benoit.raviart@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 10, 0);
-- Adding missing email audrey.bernard@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Audrey", "Bernard", "audrey.bernard@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 12, 0);
-- Adding missing email thierry.patte@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Thierry", "Patte", "thierry.patte@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 14, 0);
-- Adding missing email monique.mackiw@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "monique", "mackiw", "monique.mackiw@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 25, 0);
-- Adding missing email arnauld.mistralbernard@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Arnauld", "Mistral-Bernard", "arnauld.mistralbernard@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 17, 0);
-- Adding missing email mariepierre.zanetti@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Marie-Pierre", "ZANETTI", "mariepierre.zanetti@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 22, 0);
-- Adding missing email barbara.dufresne@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "barbara", "dufresne", "barbara.dufresne@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 23, 0);
-- Adding missing email teddy.pade@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Teddy", "Padé", "teddy.pade@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 17, 0);
-- Adding missing email patricia.davideau@ag2rlamondiale.fr
insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "Patricia", "Davideau", "patricia.davideau@ag2rlamondiale.fr", "xxx", "a:0:{}", "a:0:{}", 25, 0);
