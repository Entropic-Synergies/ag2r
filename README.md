## AG2R

### Roles

- Chef de projet: rfulchiron@entropic-synergies.com
- Lead dev: mcarmona@entropic-synergies.com


### Web

#### DSI
- **Prod**:
    - http://ag2rlm-dsi.entropic-group.com/

#### Idées
- **Staging**:
    - http://ag2r-idees.st.entropic-group.com/ + `xdf4dcd`
- **Prod**:    
    - http://idees.ag2rlamondiale.fr/

### Services

- Sendgrid
- Dropbox
- Embed.ly

### Demo credentials
- ag2r@entropic-group.com + `xxx`

### Installation serveur SAML de dev
Créer un vhost `www.saml.ag2r.local` qui pointe vers `PROJECT_PATH/saml-server/www`
