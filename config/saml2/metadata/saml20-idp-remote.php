<?php
/**
 * SAML 2.0 remote IdP metadata for simpleSAMLphp.
 *
 * Remember to remove the IdPs you don't use from this file.
 *
 * See: https://rnd.feide.no/content/idp-remote-metadata-reference
 */

/*
 * Guest IdP. allows users to sign up and register. Great for testing!
 */
$metadata['dev:innovpart:idp:ag2rlm:saml2'] = array(
	'name'                => array(
		'en' => 'AG2RLM',
	),
	'description'         => 'AG2R Identity Provider.',

	'SingleSignOnService' => 'http://www.saml.ag2r.local/saml2/idp/SSOService.php',
	'SingleLogoutService' => 'http://www.saml.ag2r.local/saml2/idp/SingleLogoutService.php',
	'privatekey'          => 'server.pem',
	'certificate'         => 'server.crt',

	'NameIDFormat'        => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
);

$metadata['innovpart:idp:ag2rlm:saml2'] = array(
	'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
	'entityid'                  => 'innovpart:idp:ag2rlm:saml2',
	'contacts'                  =>
		array(
			0 =>
				array(
					'contactType'  => 'administrative',
					'company'      => 'AG2RLM',
					'givenName'    => 'PORNIN',
					'emailAddress' =>
						array(
							0 => 'guillaume.pornin@ag2rlamondiale.fr',
						),
				),
		),
	'metadata-set'              => 'saml20-idp-remote',
	'SingleSignOnService'       =>
		array(
			0 =>
				array(
					'Binding'  => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
					'Location' => 'https://ldcci061.unix.lan:9031/idp/SSO.saml2',
				),
			1 =>
				array(
					'Binding'  => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
					'Location' => 'https://ldcci061.unix.lan:9031/idp/SSO.saml2',
				),
			2 =>
				array(
					'Binding'  => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Artifact',
					'Location' => 'https://ldcci061.unix.lan:9031/idp/SSO.saml2',
				),
			3 =>
				array(
					'Binding'  => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
					'Location' => 'https://ldcci061.unix.lan:9031/idp/SSO.saml2',
				),
		),
	'SingleLogoutService'       =>
		array(),
	'ArtifactResolutionService' =>
		array(),
	'keys'                      =>
		array(
			0 =>
				array(
					'encryption'      => false,
					'signing'         => true,
					'type'            => 'X509Certificate',
					'X509Certificate' => '
						MIIB1zCCAUCgAwIBAgIGAT9b1PlyMA0GCSqGSIb3DQEBBQUAMC8xCzAJBgNVBAYTAkZSMQ8wDQYDVQQKEwZBRzJSTE0xDzANBgNVBAMTBkFHMlJMTTAeFw0xMzA2MTkwOTQ3MzFaFw0xNDA2MTkwOTQ3MzFaMC8xCzAJBgNVBAYTAkZSMQ8wDQYDVQQKEwZBRzJSTE0xDzANBgNVBAMTBkFHMlJMTTCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEAtbgkqISyN1UKJCjk3SPHgUWXSx+z8UeQP52JuTg8JmaE8F8TGpq+Ww30Bvz8JYYttyHSkJWeip84bOYQ1V+EuL/PZROAQiipCuFt21JY1eUc9pgMOzlReN0Vs2jUVaZuEbKJRbTeIaaBLvLaJLC/FjjvEdjRvYCMgFD/2Qy7YU0CAwEAATANBgkqhkiG9w0BAQUFAAOBgQCG8E32gpLe5mhOwTn9C3UgjCrVMv1WnSgK53DAQraN3XRq4LPMUmZrfKmSxiSxkjFY3Ai3DoItzhmgzzFT25O815/rbINjJB+ZEQrgr8lsfP7dDEeqbDjbNm9rKefXFwsPo7VugmIpgeIHoF6mgjGZLhR03RYPM++jyetB/kzFOA==
					',
				),
		),
);

$metadata['idp:ag2rlm:saml2'] = array(
	'entityid'                  => 'idp:ag2rlm:saml2',
	'contacts'                  =>
		array(
			0 =>
				array(
					'contactType' => 'administrative',
					'company'     => 'AG2RLM',
				),
		),
	'metadata-set'              => 'saml20-idp-remote',
	'SingleSignOnService'       =>
		array(
			0 =>
				array(
					'Binding'  => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
					'Location' => 'https://loginfdi/idp/SSO.saml2',
				),
			1 =>
				array(
					'Binding'  => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
					'Location' => 'https://loginfdi/idp/SSO.saml2',
				),
			2 =>
				array(
					'Binding'  => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Artifact',
					'Location' => 'https://loginfdi/idp/SSO.saml2',
				),
			3 =>
				array(
					'Binding'  => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
					'Location' => 'https://loginfdi/idp/SSO.saml2',
				),
		),
	'SingleLogoutService'       =>
		array(),
	'ArtifactResolutionService' =>
		array(),
	'keys'                      =>
		array(
			0 =>
				array(
					'encryption'      => false,
					'signing'         => true,
					'type'            => 'X509Certificate',
					'X509Certificate' => '
						MIIC3DCCAcSgAwIBAgIGAULh8TQbMA0GCSqGSIb3DQEBBQUAMC8xCzAJBgNVBAYTAkZSMQ8wDQYDVQQKEwZBRzJSTE0xDzANBgNVBAMTBkFHMlJMTTAeFw0xMzEyMTExMzU1NTBaFw0xNTEyMTExMzU1NTBaMC8xCzAJBgNVBAYTAkZSMQ8wDQYDVQQKEwZBRzJSTE0xDzANBgNVBAMTBkFHMlJMTTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAIaOCQsF8mTxaRSvAl7ULc67LL4IFv2qz1Ses0Q5zWgesdww3EbCr1b52lCcD3hZUgFjYp9grHTa/PHshrS3sjqYMqdV4+f2CNshwQ4Zzb+YsRrmisLNFUnEzB2xBT+7RaVFWHJpCA2kSnXBRBUlXhYrlKScIuNugFek5x2ImAGMna2SIKUWfzokFt1l9yvGsuoggEVjioVUawz/w7puanRRdfZQQsVOENaNZ2el1CG1EJ4bJvoh/sHJN3Up1wQou/o70CgXfxes4oashCswo1hfnH2VJAWlSkxDUEyy/43SssIjVpyk/UYZHjheNMRWZAUJL1gnj2hwnvUFtFB0aCkCAwEAATANBgkqhkiG9w0BAQUFAAOCAQEAdOqcCiLZB+OplTi/nY6RU4JJMzuZwSU1OcBTzYrIH0QnHk04SxbFheWCcOHAhOK0E6SKgM8+jS9jDqmVMEpk0WZBEOJWokDxndUQb48nfFD9A0E65mZOsz9glQoxO0hug/2HwEiUoQGdrLDaJ/gd5u1ks8QWJFr3n41lNRlcg11A5sBGzw0exYIJK7cnF9mdgKxtZGHzHy5dFxCtm45XMF1RxyNCdow40fowVQMmx0QOY5j8tk+CpjZrfSFzHah2Pw3/e04Oh7Ki+ODdjUdGRtCmACcrSaEUAscUr2trrrUHa+fAxUe+2RTdiPhlfs19Y0hel0Q4ELTlAItdaLiYLg==
					',
				),
		),
);