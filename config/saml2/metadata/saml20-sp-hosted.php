<?php

$spName = (CAMELEON_ENVIRONMENT === 'prod' ? '' : 'dev:') . 'innovpart:sp:ag2rlm:saml2';

$metadata[$spName] = array(
	//'AG2R-SP',
	/*
	 * The hostname of the server (VHOST) that will use this SAML entity.
	 *
	 * Can be '__DEFAULT__', to use this entry by default.
	 */
	'host'                     => '__DEFAULT__',
	'privatekey'               => 'server.pem',
	'certificate'              => 'server.crt',
	'SingleLogoutService'      => CAMELEON_BASEURL . '/saml/logout',
	'AssertionConsumerService' => CAMELEON_BASEURL . '/saml/acs',
	'NameIDFormat'             => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
	'RelayState'               => CAMELEON_BASEURL . '/auth/saml_endpoint',
);