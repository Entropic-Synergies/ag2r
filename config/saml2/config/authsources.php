<?php
$config = array(
	'AG2R-SP' => array(
		'saml:SP',
		'entityID'          => (CAMELEON_ENVIRONMENT === 'prod' ? '' : 'dev:') . 'innovpart:sp:ag2rlm:saml2',
		'idp'               => 'innovpart:idp:ag2rlm:saml2',

		'privatekey'        => 'server.pem',
		'certificate'       => 'server.crt',
		'redirect.sign'     => true,
		'redirect.validate' => true,
	),
	'AG2R-SP-dev' => array(
		'saml:SP',
		'entityID'          => 'dev:innovpart:sp:ag2rlm:saml2',
		'idp'               => 'dev:innovpart:idp:ag2rlm:saml2',

		'privatekey'        => 'server.pem',
		'certificate'       => 'server.crt',
		'redirect.sign'     => true,
		'redirect.validate' => true,
	),
);
