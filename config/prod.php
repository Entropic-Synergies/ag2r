<?php

$config['CAMELEON_BASEURL'] = 'https://idees.ag2rlamondiale.fr';


// db
$config['CAMELEON_DB_HOST'] = '78.41.233.16';
$config['CAMELEON_DB_NAME'] = $config['CAMELEON_PROJECTNAME'] . '_prod';
$config['CAMELEON_DB_USER'] = $config['CAMELEON_PROJECTNAME'] . '_mysql';

$config['CAMELEON_STAGINGAUTH'] = 'xdf4dcd';

$config['CAMELEON_MAIL_FROM_EMAIL'] = 'idees@ag2rlamondiale.fr';
$config['CAMELEON_MAIL_FROM_NAME'] = 'Idées AG2R';

require __DIR__ . '/../../config.php';
