<?php
ini_set('date.timezone', 'Europe/Paris');

$dirs = explode(DIRECTORY_SEPARATOR, __DIR__);
$config['CAMELEON_PROJECTNAME'] = $dirs[count($dirs) - 3];

$config['CAMELEON_LEAD_DEV'] = 'ademoulins@entropic-synergies.com';

$config['CAMELEON_SALT'] = 'QS_%6';
$config['CAMELEON_LANG'] = 'fr';
$config['CAMELEON_GITHOOKAUTH'] = 'xdf4dcd';

$config['SOCIAL_ACTIVE'] = false;
$config['NEED_VALIDATION'] = true;

$config['WEBMASTER_EMAIL'] = 'idees@ag2rlamondiale.fr';

$config['CAMELEON_PROJECTNAME'] = 'ag2r-idees';

if (strstr($config['CAMELEON_DIR_APP'], '/prod/')) {
	$config['CAMELEON_ENVIRONMENT'] = 'prod';
}

if ($config['CAMELEON_DIR_APP'] == '/var/www/st/') {
	$config['CAMELEON_ENVIRONMENT'] = 'staging';
}

if ($config['CAMELEON_DIR_APP'] == '/var/www/pr/') {
	$config['CAMELEON_ENVIRONMENT'] = 'preprod';
}