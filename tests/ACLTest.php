<?php

use Cameleon\Security\MaskBuilder;
use Cameleon\Security\Permissions;
use Cameleon\Services;
use Cameleon\Tests\WebTest;

class ACLTest extends WebTest
{
	public function test_acl()
	{
		$db = Services::get_db();
		$idea = (new Idea())->create([
			'description' => 'Idea 1 desc...',
			'name' => 'Idea 1 name...',
			'user_id' => 1,
		]);

		$acl = Services::get_acl();

		$user = ['user_id' => 1,
			'roles' => []
		];

		$this->assertEquals(true, $acl->is_granted($user, 'EDIT', 'idea/' . $idea['idea_id']), 'Bad ACL');

		$idea = (new Idea())->create([
			'description' => 'Idea 2 desc...',
			'name'        => 'Idea 2 name...',
			'user_id'     => 2,
		]);

		$this->assertEquals(false, $acl->is_granted($user, 'EDIT', 'idea/' . $idea['idea_id']), 'Bad ACL');

		// Add user SUPERADMIN role
		$user['roles'][] = 'SUPERADMIN';
		$this->assertEquals(true, $acl->is_granted($user, 'EDIT', 'idea/' . $idea['idea_id']), 'Bad ACL');

		// Remove user SUPERADMIN role
		array_pop($user['roles']);
		$this->assertEquals(false, $acl->is_granted($user, 'EDIT', 'idea/' . $idea['idea_id']), 'Bad ACL');

		// Add user ADMIN role
		$user['roles'][] = 'ADMIN';
		$this->assertEquals(false, $acl->is_granted($user, 'EDIT', 'idea/' . $idea['idea_id']), 'Bad ACL');

		// Add ACL for Admin
		$result = $acl->set_ACL('ADMIN', 'idea/' . $idea['idea_id'], [MaskBuilder::MASK_EDIT]);
		$this->assertEquals(true, $acl->is_granted($user, 'EDIT', 'idea/' . $idea['idea_id']), 'Bad ACL');
		$this->assertEquals(false, $acl->is_granted($user, 'OWNER', 'idea/' . $idea['idea_id']), 'Bad ACL');

		// Remove user ADMIN role
		array_pop($user['roles']);
		$this->assertEquals(false, $acl->is_granted($user, 'EDIT', 'idea/' . $idea['idea_id']), 'Bad ACL');
		$this->assertEquals(false, $acl->is_granted($user, 'OWNER', 'idea/' . $idea['idea_id']), 'Bad ACL');

		$result = $acl->set_ACL(1, 'idea/' . $idea['idea_id'], [MaskBuilder::MASK_OWNER]);

		$this->assertEquals(true, $acl->is_granted($user, 'EDIT', 'idea/' . $idea['idea_id']), 'Bad ACL');
		$this->assertEquals(true, $acl->is_granted($user, 'OWNER', 'idea/' . $idea['idea_id']), 'Bad ACL');

		$this->assertEquals(false, $acl->is_granted($user, 'CREATE', 'innovation'), 'Bad ACL');
	}
}