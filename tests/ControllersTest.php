<?php

use Cameleon\Tests\WebTest;

class ControllersTest extends WebTest
{
	public function test_all_routes()
	{
		$_SESSION['user_id'] = 1;

		$routes = [
			'',
			'group/all',
			'group/1',
			'message/all',
			'innovation/all',
			'innovation/1',
			'experimentation/all',
			'experimentation/1',
			'idea/all',
			'user/1',
			'feed/all',
			'document/all?innovation_id=1',
			'milestone/all?innovation_id=1',
			'milestone/all?experimentation_id=1',
			'user/invitations',
		];

		foreach ($routes as $route) {
			$this->testRoute('/' . $route);
		}
	}

	private function testRoute($url)
	{
		$response = $this->request('GET', $url);
		$this->assertEquals(200, $response->getStatusCode());
	}
}