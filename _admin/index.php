<?php

require __DIR__ . '/../vendor/autoload.php';
$app = new \Cameleon\App();

define('CAMELEON_ADMINAUTH', 'rP5qM13s');

if (isset($_GET['admin_auth']) and $_GET['admin_auth'] == CAMELEON_ADMINAUTH) {
	setcookie('admin_auth', CAMELEON_ADMINAUTH, time() + 864000); // 10 days
	setcookie('staging_auth', CAMELEON_STAGINGAUTH, time() + 864000); // 10 days
	header('Location: /', true);
	exit;
}
if (!isset($_COOKIE['admin_auth']) or $_COOKIE['admin_auth'] != CAMELEON_ADMINAUTH) {
	header('Location: /admin-auth.php', true);
	exit;
}

$model_manifest = (new \Cameleon\App())->get_models();

$db_link = mysqli_connect(CAMELEON_DB_HOST, CAMELEON_DB_USER, CAMELEON_DB_PASS);
mysqli_select_db($db_link, CAMELEON_DB_NAME);


if (
	isset($_GET['controller']) and    !empty($_GET['controller']) and    is_file('controllers/' . $_GET['controller'] . '.php') and
	isset($_GET['view']) and !empty($_GET['view']) and is_file('views/' . $_GET['controller'] . '/' . $_GET['view'] . '.php')
) {

	include_once __DIR__ . '/controllers/' . $_GET['controller'] . '.php';
	include_once __DIR__ . '/views/' . $_GET['controller'] . '/' . $_GET['view'] . '.php';

} else {

	include_once __DIR__ . '/controllers/default.php';
	include_once __DIR__ . '/views/default.php';

}
