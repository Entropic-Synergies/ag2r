<?php include("views/_head.php"); ?>
<?php include("views/_topnav.php"); ?>
<div class="main-container container-fluid">
	<a class="menu-toggler" id="menu-toggler" href="#">
		<span class="menu-text"></span>
	</a>
	<?php include("views/_sidebar.php"); ?>
	<div class="main-content">
		<div class="breadcrumbs breadcrumbs-fixed" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="icon-home home-icon"></i>
					<a href="index.php">Accueil</a>
							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
				</li>
				<li class="active">Idées</li>
			</ul>
		</div>
		<div class="page-content">
			<div class="page-header position-relative">
				<h1>Liste des idées</h1>
			</div>
			<div class="row-fluid">
				<div class="span12 text-center">
					<table id="sample-table-2" class="table table-striped table-bordered table-hover">
						<thead>
						<tr>
							<th>Idée ID</th>
							<th>Titre</th>
							<th>Description</th>
							<th>Accepter</th>
							<th>Refuser</th>
							<th>Editer</th>
<!--							<th>Supprimer</th>-->
						</tr>
						</thead>
						<tbody>
						<?php foreach ($users as $idea): ?>
							<tr>
								<td><?= $idea['idea_id'] ?></td>
								<td><?= $idea['name'] ?></td>
								<td><?= $idea['description'] ?></td>
								<td width="100"><a href="?controller=idea&view=edit&action=accept&idea_id=<?= $idea['idea_id'] ?>" class="btn btn-success">Accepter</a></td>
								<td width="100"><a href="?controller=idea&view=edit&action=deny&idea_id=<?= $idea['idea_id'] ?>" class="btn btn-danger">Refuser</a></td>
								<td>
									<a href="index.php?controller=idea&view=edit&idea_id=<?= $idea['idea_id'] ?>"><i
											class="icon-edit-sign blue"></i> Editer</a>
								</td>

<!--								<td>-->
<!--									<a href="index.php?controller=idea&view=list&action=delete&idea_id=--><?//= $idea['idea_id'] ?><!--"><i-->
<!--											class="icon-remove-sign blue"></i> Supprimer</a></td>-->
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<?php include("views/_ace-settings.php"); ?>
	</div>
</div>
<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse">
	<i class="icon-double-angle-up icon-only bigger-110"></i>
</a>
<?php include("views/_footer.php"); ?>
</body>
</html>