<?php include("views/_head.php"); ?>
<?php include("views/_topnav.php"); ?>
<div class="main-container container-fluid">
	<a class="menu-toggler" id="menu-toggler" href="#">
		<span class="menu-text"></span>
	</a>
	<?php include("views/_sidebar.php"); ?>
	<div class="main-content">
		<div class="breadcrumbs breadcrumbs-fixed" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="icon-home home-icon"></i>
					<a href="index.php">Accueil</a>
							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
				</li>
				<li>
					<a href="index.php?controller=idea&view=list">Idées</a>
							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
				</li>
				<li class="active"><?= $idea['name'] ?></li>
			</ul>
		</div>
		<div class="page-content">
			<div class="page-header position-relative">
				<h1>Edition de l'idée : <?= $idea['name'] ?></h1>
			</div>
			<div class="row-fluid">
				<div class="span12 text-center">
					<form method="post"
						  action="index.php?controller=idea&view=edit&action=edit&idea_id=<?= $idea['idea_id'] ?>"
						  autocomplete="off">
						<fieldset>
							<table class="table table-striped table-bordered table-hover">
								<tr>
									<td class="text-left">Titre de l'idée</td>
									<td><input type="text" name="name" value="<?= $idea['name'] ?>"></td>
								</tr>
								<tr>
									<td class="text-left">Description</td>
									<td>
										<textarea type="text" name="description"><?= $idea['description'] ?></textarea>
									</td>
								</tr>

							</table>
						</fieldset>
						<div class="form-actions center">
							<button class="btn btn-small btn-success" type="submit">
								Sauvegarder
								<i class="icon-arrow-right icon-on-right bigger-110"></i>
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<?php include("views/_ace-settings.php"); ?>
	</div>
</div>
<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse">
	<i class="icon-double-angle-up icon-only bigger-110"></i>
</a>
<?php include("views/_footer.php"); ?>
</body>
</html>