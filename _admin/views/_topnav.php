		<div class="navbar navbar-fixed-top" id="navbar">
			<script type="text/javascript">
				try{ace.settings.check('navbar' , 'fixed')}catch(e){}
			</script>

			<div class="navbar-inner">
				<div class="container-fluid">
					<a href="#" class="brand">
						<small>
							<img src="/assets/img/logo.png">
							AG2R Admin
						</small>
					</a><!--/.brand-->

					<ul class="nav ace-nav pull-right">
						<li class="grey">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="icon-envelope icon-animated-vertical"></i>
								<span class="badge badge-success">5</span>
							</a>

							<ul class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-closer">
								<li class="nav-header">
									<i class="icon-envelope-alt"></i>
									5 messages
								</li>

								<li>
									<a href="#">
										<img src="assets/avatars/avatar.png" class="msg-photo" alt="Alex Avatar" />
										<span class="msg-body">
											<span class="msg-title">
												<span class="blue">Alex:</span>
												Bonjour j'aurais voulu avoir ...
											</span>

											<span class="msg-time">
												<i class="icon-time"></i>
												<span>il y a 20 minutes</span>
											</span>
										</span>
									</a>
								</li>

								<li>
									<a href="#">
										<img src="assets/avatars/avatar3.png" class="msg-photo" alt="Sandy Avatar" />
										<span class="msg-body">
											<span class="msg-title">
												<span class="blue">Sandy:</span>
												Serais-ce possible d'avoir des Miles ...
											</span>

											<span class="msg-time">
												<i class="icon-time"></i>
												<span>il y a 2 semaines</span>
											</span>
										</span>
									</a>
								</li>

								<li>
									<a href="#">
										<img src="assets/avatars/avatar4.png" class="msg-photo" alt="Bob's Avatar" />
										<span class="msg-body">
											<span class="msg-title">
												<span class="blue">Florian:</span>
												Ce site est génial mais qui ...
											</span>

											<span class="msg-time">
												<i class="icon-time"></i>
												<span>il y a 1 mois</span>
											</span>
										</span>
									</a>
								</li>

								<li>
									<a href="#">
										Voir tous les messages
										<i class="icon-arrow-right"></i>
									</a>
								</li>
							</ul>
						</li>

						<li class="light-blue">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="assets/avatars/admin.png" alt="Administrateur" />
								<span class="user-info">
									<small>Bienvenue,</small>
									Administrateur
								</span>
							</a>
						</li>
					</ul><!--/.ace-nav-->
				</div><!--/.container-fluid-->
			</div><!--/.navbar-inner-->
		</div>