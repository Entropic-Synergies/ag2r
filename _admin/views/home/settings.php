<div class="span6">
	<div class="widget-box">
		<div class="widget-header">
			<h4 class="lighter smaller">
				<i class="icon-cogs blue"></i>
				Paramètres
			</h4>
			<span><a href="index.php?controller=setting&amp;view=edit"><i class="icon-edit-sign"></i> Editer</a></span>
		</div>

		<div class="widget-body">
			<div class="widget-main no-padding">
				<table id="sample-table-1" class="table table-striped table-bordered table-hover">
					<tbody>

					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>