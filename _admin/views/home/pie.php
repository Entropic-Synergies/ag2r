<div class="span6">
	<div class="widget-box">
		<div class="widget-header widget-header-flat widget-header-small">
			<h5>
				<i class="icon-signal"></i>
				Traffic Sources
			</h5>

			<div class="widget-toolbar no-border">
				<button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
					This Week
					<i class="icon-angle-down icon-on-right"></i>
				</button>

				<ul class="dropdown-menu dropdown-info pull-right dropdown-caret">
					<li class="active">
						<a href="#">This Week</a>
					</li>

					<li>
						<a href="#">Last Week</a>
					</li>

					<li>
						<a href="#">This Month</a>
					</li>

					<li>
						<a href="#">Last Month</a>
					</li>
				</ul>
			</div>
		</div>

		<div class="widget-body">
			<div class="widget-main">
				<div id="piechart-placeholder"></div>
			</div>
		</div>
	</div>
</div>