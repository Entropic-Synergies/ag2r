<div class="span6">
	<div class="widget-box transparent" id="recent-box">
		<div class="widget-header">
			<h4 class="lighter smaller">
				<i class="icon-rss orange"></i>
				Activité récente
			</h4>

			<div class="widget-toolbar no-border">
				<ul class="nav nav-tabs" id="recent-tab">
					<li class="active">
						<a data-toggle="tab" href="#bets-tab">Enchères</a>
					</li>
					<li>
						<a data-toggle="tab" href="#members-tab">Membres</a>
					</li>
				</ul>
			</div>
		</div>

		<div class="widget-body">
			<div class="widget-main padding-4">
				<div class="tab-content padding-8 overflow-visible">
					<div id="bets-tab" class="tab-pane active">
						<ul id="tasks" class="item-list">
							<li class="item-orange clearfix">
								<label class="inline">
									<span class="lbl">Noël 2013 Séjour pour 2 personnes à l’Île de France</span>
								</label>
							</li>
							<li class="item-red clearfix">
								<label class="inline">
									<span class="lbl">Séjour d'une semaine aux Maldives</span>
								</label>
							</li>
							<li class="item-default clearfix">
								<label class="inline">
									<span class="lbl">Voyage de 4 jours aux Etats-Unis</span>
								</label>
							</li>
							<li class="item-blue clearfix">
								<label class="inline">
									<span class="lbl">Ascension du Kilimandjaro en Juillet pour 2 personnes</span>
								</label>
							</li>
							<li class="item-grey clearfix">
								<label class="inline">
									<span class="lbl">Visite des chutes du Niagara</span>
								</label>
							</li>
							<li class="item-green clearfix">
								<label class="inline">
									<span class="lbl">Séjour de 2 jours pour le carnaval de Copacabana</span>
								</label>
							</li>
						</ul>
					</div>

					<div id="members-tab" class="tab-pane">
						<div class="clearfix">
							<div class="itemdiv memberdiv">
								<div class="user">
									<img alt="Bob Doe's avatar" src="assets/avatars/user.jpg" />
								</div>

								<div class="body">
									<div class="name">
										<a href="#">Bob Doe</a>
									</div>
									<div class="time">
										<i class="icon-time"></i>
										<span class="green">20 min</span>
									</div>
								</div>
							</div>

							<div class="itemdiv memberdiv">
								<div class="user">
									<img alt="Joe Doe's avatar" src="assets/avatars/avatar2.png" />
								</div>

								<div class="body">
									<div class="name">
										<a href="#">Joe Doe</a>
									</div>

									<div class="time">
										<i class="icon-time"></i>
										<span class="green">1 heure</span>
									</div>
								</div>
							</div>

							<div class="itemdiv memberdiv">
								<div class="user">
									<img alt="Jim Doe's avatar" src="assets/avatars/avatar.png" />
								</div>

								<div class="body">
									<div class="name">
										<a href="#">Jim Doe</a>
									</div>

									<div class="time">
										<i class="icon-time"></i>
										<span class="green">2 heures</span>
									</div>
								</div>
							</div>

							<div class="itemdiv memberdiv">
								<div class="user">
									<img alt="Alex Doe's avatar" src="assets/avatars/avatar5.png" />
								</div>

								<div class="body">
									<div class="name">
										<a href="#">Alex Doe</a>
									</div>

									<div class="time">
										<i class="icon-time"></i>
										<span class="green">3 heures</span>
									</div>
								</div>
							</div>

							<div class="itemdiv memberdiv">
								<div class="user">
									<img alt="Bob Doe's avatar" src="assets/avatars/avatar2.png" />
								</div>

								<div class="body">
									<div class="name">
										<a href="#">Bob Doe</a>
									</div>

									<div class="time">
										<i class="icon-time"></i>
										<span class="green">6 heures</span>
									</div>
								</div>
							</div>

							<div class="itemdiv memberdiv">
								<div class="user">
									<img alt="Susan's avatar" src="assets/avatars/avatar3.png" />
								</div>

								<div class="body">
									<div class="name">
										<a href="#">Susan</a>
									</div>

									<div class="time">
										<i class="icon-time"></i>
										<span class="green">Hier</span>
									</div>
								</div>
							</div>

							<div class="itemdiv memberdiv">
								<div class="user">
									<img alt="Phil Doe's avatar" src="assets/avatars/avatar4.png" />
								</div>

								<div class="body">
									<div class="name">
										<a href="#">Phil Doe</a>
									</div>

									<div class="time">
										<i class="icon-time"></i>
										<span class="green">il y a 2 jours</span>
									</div>
								</div>
							</div>

							<div class="itemdiv memberdiv">
								<div class="user">
									<img alt="Alexa Doe's avatar" src="assets/avatars/avatar1.png" />
								</div>

								<div class="body">
									<div class="name">
										<a href="#">Alexa Doe</a>
									</div>

									<div class="time">
										<i class="icon-time"></i>
										<span class="green">il y a 3 jours</span>
									</div>
								</div>
							</div>
						</div>

						<div class="center">
							<i class="icon-group icon-2x blue"></i>

							&nbsp;
							<a href="#">
								Liste des membres &nbsp;
								<i class="icon-arrow-right"></i>
							</a>
						</div>

						<div class="hr hr-double hr8"></div>
					</div><!--member-tab-->
				</div>
			</div><!--/widget-main-->
		</div><!--/widget-body-->
	</div><!--/widget-box-->
</div><!--/span-->