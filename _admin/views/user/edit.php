<?php include("views/_head.php"); ?>
	<?php include("views/_topnav.php"); ?>
		<div class="main-container container-fluid">
			<a class="menu-toggler" id="menu-toggler" href="#">
				<span class="menu-text"></span>
			</a>
			<?php include("views/_sidebar.php"); ?>
			<div class="main-content">
				<div class="breadcrumbs breadcrumbs-fixed" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-home home-icon"></i>
							<a href="index.php">Accueil</a>
							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
						</li>
						<li>
							<a href="index.php?controller=user&view=list">Utilisateurs</a>
							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
						</li>
						<li class="active"><?=$idea['name']?></li>
					</ul>
				</div>
				<div class="page-content">
					<div class="page-header position-relative">
						<h1>Edition de l'utilisateur : <?=$idea['name']?></h1>
					</div>
					<div class="row-fluid">
						<div class="span12 text-center">
							<form method="post" action="index.php?controller=user&amp;view=edit&amp;action=edit&amp;user_id=<?=$idea['user_id']?>" autocomplete="off">
								<fieldset>
									<table class="table table-striped table-bordered table-hover">
										<tr>
											<td class="text-left">Prénom</td>
											<td><input type="text" name="first_name" value="<?=$idea['first_name']?>"></td>
										</tr>
										<tr>
											<td class="text-left">Nom</td>
											<td><input type="text" name="last_name" value="<?=$idea['last_name']?>"></td>
										</tr>
										<tr>
											<td class="text-left">Email</td>
											<td><input type="text" name="email" value="<?=$idea['email']?>"></td>
										</tr>

									</table>
								</fieldset>
								<div class="form-actions center">
									<button class="btn btn-small btn-success" type="submit">
										Sauvegarder
										<i class="icon-arrow-right icon-on-right bigger-110"></i>
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<?php include("views/_ace-settings.php"); ?>
			</div>
		</div>
		<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse">
			<i class="icon-double-angle-up icon-only bigger-110"></i>
		</a>
<?php include("views/_footer.php"); ?>
	</body>
</html>