<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>AG2R Admin</title>

        <meta name="description" content="overview &amp; stats" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <!--basic styles-->

        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/css/bootstrap-responsive.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/css/font-awesome.min.css" />

        <!--[if IE 7]>
          <link rel="stylesheet" href="assets/css/font-awesome-ie7.min.css" />
        <![endif]-->

        <!--page specific plugin styles-->

        <link rel="stylesheet" href="assets/css/jquery-ui-1.10.3.custom.min.css" />
        <link rel="stylesheet" href="assets/css/chosen.css" />
        <link rel="stylesheet" href="assets/css/datepicker.css" />
        <link rel="stylesheet" href="assets/css/bootstrap-timepicker.css" />
        <link rel="stylesheet" href="assets/css/daterangepicker.css" />
        <link rel="stylesheet" href="assets/css/colorpicker.css" />
        <link rel="stylesheet" href="assets/css/magicsuggest-1.3.0.css" />


        <!--fonts-->

        <link rel="stylesheet" href="assets/css/ace-fonts.css" />

        <!--ace styles-->

        <link rel="stylesheet" href="assets/css/ace.min.css" />
        <link rel="stylesheet" href="assets/css/ace-responsive.min.css" />
        <link rel="stylesheet" href="assets/css/ace-skins.min.css" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <link rel="shortcut icon" href="assets/img/favicon.png">

        <!--[if lte IE 8]>
          <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
        <![endif]-->

        <!--inline styles related to this page-->

        <!--ace settings handler-->
        <script src="assets/js/ace-extra.min.js"></script>

        <script type="text/javascript" src="assets/js/tiny_mce/tinymce.min.js"></script>

        <script type="text/javascript">
        tinymce.init({
            selector: "textarea.tiny",
            entity_encoding : 'raw',
            theme: "modern",
            language : 'fr_FR',
            relative_urls: false,
            content_css: "assets/css/bootstrap.min.css, assets/css/preview.css, http://fonts.googleapis.com/css?family=Lobster",
            body_class: "content",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "template paste textcolor jbimages icons"
            ],
			style_formats: [
				{title: 'Image à gauche', selector: 'img',
					styles: {
						'float': 'left',
						'margin': '0 10px 0 10px'
					}
				},
				{title: 'Image à droite', selector: 'img',
					styles: {
						'float': 'right',
						'margin': '0 0 10px 10px'
					}
				}
			],
            toolbar1: "insertfile undo redo | styleselect formatselect fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
            toolbar2: "print preview fullscreen media | forecolor backcolor icons | link image jbimages",
            image_advtab: true,
        });
        </script>
    </head>
    <body class="breadcrumbs-fixed">