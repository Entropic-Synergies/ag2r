<?php
if (!isset($_GET['controller'])) { $_GET['controller'] = null; }
if (!isset($_GET['view'])) { $_GET['view'] = null; }
?>
<div class="sidebar sidebar-fixed" id="sidebar">
	<ul class="nav nav-list">
		<li <?php if (!isset($_GET['controller'])): ?>class="active"<?php endif; ?>>
			<a href="index.php">
				<i class="icon-dashboard"></i>
				<span class="menu-text"> Accueil </span>
			</a>
		</li>
		<li <?php if (@$_GET['controller'] == 'user'): ?>class="active"<?php endif; ?>>
			<a href="index.php?controller=user&amp;view=list">
				<i class="icon-group"></i>
				<span class="menu-text"> Utilisateurs </span>
			</a>
		</li>
		<li <?php if (@$_GET['controller'] == 'idea'): ?>class="active"<?php endif; ?>>
			<a href="index.php?controller=idea&amp;view=list">
				<i class="icon-lightbulb"></i>
				<span class="menu-text"> Idées </span>
			</a>
		</li>
	</ul>
	<div class="sidebar-collapse" id="sidebar-collapse">
		<i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
	</div>
</div>