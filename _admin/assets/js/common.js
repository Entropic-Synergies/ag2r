function addslashes(ch) {
	ch = ch.replace(/\\/g,"\\\\")
	ch = ch.replace(/\'/g,"\\'")
	ch = ch.replace(/\"/g,"\\\"")
	return ch;
}

function user_autocomplete(field, list, user_id_field)
{
	$(field).keyup(function(){
		if($(field).val() == '') {
			$(list).fadeOut();
		} else {
			$.ajax({
				url: 'ajax.php?model=user&action=search',
				type: "POST",
				dataType: 'json',
				data: {
					username : $(field).val()
				},
				success: function(json){
					if(json.length < 1) {
						$(list).fadeOut();
					} else {
						var html ='';
						$.each(json, function(i, item) {
							html = html + '<div class="one-user-on-autocomplete" onclick="complete_field(\'' + field + '\', \'' + addslashes(item.first_name) + ' ' + addslashes(item.last_name) + '\'); complete_field(\'' + user_id_field + '\', \'' + item.user_id + '\'); hide_list(\'' + list + '\');">' + item.first_name + ' ' + item.last_name + '</div>';
						});
						$(list).html(html);
						$(list).fadeIn();
					}
				}
			});
		}
	});
}

function complete_field(field, value)
{
	$(field).val(value);
}

function hide_list(list)
{
	$(list).fadeOut();
}

function display_tags_modal(oid,eid){
	$('#edit_tags').modal('show');
	$('#obj_id').val(oid + '/' + eid);
    interests.setDataUrlParams({object_id: $('#obj_id').val()});
	display_tags();
}

$('#add-tags').click(function() {
	$.each(interests.getValue(), function(index, tag_id) {
		$.ajax({
			url: 'ajax.php?model=tag&action=link_tag',
			type: 'post',
			data: {
				object_id: $('#obj_id').val(),
				tag_id: tag_id,
			},
			success:function(json){
			}
		});
	});
	interests.clear();
	display_tags();
});

function tag_unlink(tid){
	$.ajax({
		url: 'ajax.php?model=tag&action=unlink_tag',
		type: "POST",
		dataType: 'json',
		data: {
			tag_id: tid,
			object_id: $('#obj_id').val()
		},
		success: function(json){
			display_tags();
		},
	});
}

function display_tags(){
	$.ajax({
		url: 'ajax.php?model=tag&action=get_tags_of',
		type: "POST",
		dataType: 'json',
		data: {
			object_id: $('#obj_id').val()
		},
		success: function(json){
			if(json === null){
				$('#tag_list').fadeOut();
			}else{
				$('#tag_list').fadeIn();
				var html ='';
				$.each(json, function(i, item) {
						html = html + '<div class="input-append"><input class="span2" id="appendedInputButton" type="text" value="' + item.name + '"><button class="btn btn-danger del_tag" type="button" onclick="tag_unlink(\'' + item.tag_id + '\');"><i class="icon-remove"></i></button></div>';
				});
				$('#tag_list').html(html);
			}
		},
	});
}


$('.btn-back').click(function() {
	window.history.go(-1);
});