<?php

if(isset($_GET['view']) and $_GET['view'] == 'faq') {
	$faq = (new Page)->get_current(1);

	if(isset($_GET['action']) and $_GET['action'] == 'edit') {
		if(!empty($_POST)) {
			(new Page)->update(['content' => $_POST['content']], ['page_id' => 1]);
			header('Location: /index.php?controller=page&view=faq'); exit;
		}
	}
} elseif(isset($_GET['view']) and $_GET['view'] == 'about') {
	$about = (new Page)->get_current(2);

	if(isset($_GET['action']) and $_GET['action'] == 'edit') {
		if(!empty($_POST)) {
			(new Page)->update(['content' => $_POST['content']], ['page_id' => 2]);
			header('Location: /index.php?controller=page&view=about'); exit;
		}
	}
} elseif(isset($_GET['view']) and $_GET['view'] == 'cgu') {
	$cgu = (new Page)->get_current(3);

	if(isset($_GET['action']) and $_GET['action'] == 'edit') {
		if(!empty($_POST)) {
			(new Page)->update(['content' => $_POST['content']], ['page_id' => 3]);
			header('Location: /index.php?controller=page&view=cgu'); exit;
		}
	}
} elseif(isset($_GET['view']) and $_GET['view'] == 'privacy') {
	$privacy = (new Page)->get_current(4);

	if(isset($_GET['action']) and $_GET['action'] == 'edit') {
		if(!empty($_POST)) {
			(new Page)->update(['content' => $_POST['content']], ['page_id' => 4]);
			header('Location: /index.php?controller=page&view=privacy'); exit;
		}
	}
} elseif(isset($_GET['view']) and $_GET['view'] == 'legal') {
	$legal = (new Page)->get_current(5);

	if(isset($_GET['action']) and $_GET['action'] == 'edit') {
		if(!empty($_POST)) {
			(new Page)->update(['content' => $_POST['content']], ['page_id' => 5]);
			header('Location: /index.php?controller=page&view=legal'); exit;
		}
	}
} elseif(isset($_GET['view']) and $_GET['view'] == 'cnil') {
	$cnil = (new Page)->get_current(6);

	if(isset($_GET['action']) and $_GET['action'] == 'edit') {
		if(!empty($_POST)) {
			(new Page)->update(['content' => $_POST['content']], ['page_id' => 6]);
			header('Location: /index.php?controller=page&view=cnil'); exit;
		}
	}
} elseif (isset($_GET['view']) and $_GET['view'] == 'partners') {
	$partners = (new Page)->get_current(7);

	if (isset($_GET['action']) and $_GET['action'] == 'edit') {
		if (!empty($_POST)) {
			(new Page)->update(['content' => $_POST['content']], ['page_id' => 7]);
			header('Location: /index.php?controller=page&view=partners');
			exit;
		}
	}
} elseif (isset($_GET['view']) and $_GET['view'] == 'ambassador') {
	$ambassador = (new Page)->get_current(8);

	if (isset($_GET['action']) and $_GET['action'] == 'edit') {
		if (!empty($_POST)) {
			(new Page)->update(['content' => $_POST['content']], ['page_id' => 8]);
			header('Location: /index.php?controller=page&view=ambassador');
			exit;
		}
	}
}
