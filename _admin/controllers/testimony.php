<?php

if(isset($_GET['view']) and $_GET['view'] == 'list') {
	$testimonies = (new Testimony)->get_array('all');

	if(isset($_GET['action']) and $_GET['action'] == 'delete' and !empty($_GET['testimony_id'])) {
		$testimony_id = intval($_GET['testimony_id']);
		(new Testimony($testimony_id))->delete(['testimony_id' => $testimony_id]);
		header('Location: /index.php?controller=testimony&view=list'); exit;
	}

} elseif(isset($_GET['view']) and $_GET['view'] == 'edit' and !empty($_GET['testimony_id'])) {
	$testimony_id = intval($_GET['testimony_id']);
	$testimony = (new Testimony)->get_current($testimony_id);

	if(isset($_GET['delete_file']) and !empty($_GET['delete_file'])) {
		(new File)->delete(['file_id' => intval($_GET['delete_file'])]);
		header('Location: /index.php?controller=testimony&view=edit&testimony_id=' . $testimony_id);
		exit;
	}

	if(isset($_GET['action']) and $_GET['action'] == 'edit') {
		if(!empty($_POST)) {
	
			(new Testimony)->update($_POST, ['testimony_id' => $testimony_id]);

			if(!empty($_FILES['image']['name'])) {
				$f = (new File)->upload($_FILES['image'], 'testimony/' . $testimony_id);	
				(new Testimony)->update(['file_picture_id' => $f['file_id']], ['testimony_id' => $testimony_id]);
			}
			header('Location: /index.php?controller=testimony&view=list'); exit;
		}
	}
} elseif(isset($_GET['view']) and $_GET['view'] == 'new' and isset($_GET['action']) and $_GET['action'] == 'add') {
	if(!empty($_POST)) {

		$testimony = (new Testimony)->create($_POST);

		if(!empty($_FILES['image']['name'])) {
			$f = (new File)->upload($_FILES['image'], 'testimony/' . $testimony['testimony_id']);
			(new Testimony)->update(['file_picture_id' => $f['file_id']], ['testimony_id' => $testimony['testimony_id']]);
		}
		header('Location: /index.php?controller=testimony&view=list'); exit;
	}
}
