<?php

if(isset($_GET['view']) and $_GET['view'] == 'edit') {
	$setting = (new Setting)->get_current(1);

	if(isset($_GET['action']) and $_GET['action'] == 'edit') {
		if(!empty($_POST)) {
			$value = $_POST;
			if(!empty($value['promote_text'])) {
				$value['promote_text'] = serialize($value['promote_text']);
			}
			(new Setting)->update($value, ['setting_id' => 1]);
			header('Location: /index.php?controller=setting&view=edit'); exit;
		}
	}
}
