<?php

if(isset($_GET['view']) and $_GET['view'] == 'list') {
	$users = (new User)->get_array('all');

	if(isset($_GET['action']) and $_GET['action'] == 'delete' and !empty($_GET['user_id'])) {
		$user_id = intval($_GET['user_id']);
		(new User($user_id))->delete(['user_id' => $user_id]);
		header('Location: /index.php?controller=user&view=list'); exit;
	}

} elseif(isset($_GET['view']) and $_GET['view'] == 'edit' and !empty($_GET['user_id'])) {
	$user_id = intval($_GET['user_id']);
	$user = (new User)->get_current($user_id);

	if(isset($_GET['action']) and $_GET['action'] == 'edit') {
		if(!empty($_POST)) {
			(new User)->update($_POST, ['user_id' => $user_id]);
			header('Location: /index.php?controller=user&view=list'); exit;
		}
	}
}
