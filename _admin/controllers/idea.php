<?php

if (isset($_GET['view']) and $_GET['view'] == 'list') {
	$users = (new Idea)->get_array('all');

	if (isset($_GET['action']) and $_GET['action'] == 'delete' and !empty($_GET['idea_id'])) {
		$idea_id = intval($_GET['idea_id']);
		(new Idea($idea_id))->delete(['idea_id' => $idea_id]);
		header('Location: /index.php?controller=idea&view=list');
		exit;
	}
} elseif (isset($_GET['view']) and $_GET['view'] == 'edit' and !empty($_GET['idea_id'])) {
	$idea_id = intval($_GET['idea_id']);
	$idea = (new Idea)->get_current($idea_id);

	if (isset($_GET['action']) and $_GET['action'] == 'edit') {
		if (!empty($_POST)) {
			(new Idea)->update($_POST, ['idea_id' => $idea_id]);
			header('Location: /index.php?controller=idea&view=list');
			exit;
		}
	}
}
