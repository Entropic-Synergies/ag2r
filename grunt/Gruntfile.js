var path = require('path');

module.exports = function (grunt) {

	grunt.initConfig({

		pkg: grunt.file.readJSON('package.json'),


		less: {
			options: {
				paths: '../web/assets/styledocco/',
				clean: false,
				compress: false
			},
			layout: {
				options: {
					clean: true,
					compress: false
				},
				src: 'src/views/layout/layouta.less',
				dest: '../web/assets/css/layouta.css'
			},
			bootstrap: {
				options: {
					clean: true,
					compress: true
				},
				src: 'src/bootstrap/2-3-2/less/bootstrap.less',
				dest: '../web/assets/css/bootstrap/2-3-2/bootstrap.css'
			}
		},

		// https://github.com/gruntjs/grunt-contrib-compass
		compass: {

			dist: {
				options: {
					raw: 'preferred_syntax = :scss\n',
					noLineComments: true,
					relativeAssets: true,
					outputStyle: 'expanded',
					cssDir: '../web/assets/css/',
					sassDir: 'src/sass/',
					imagesDir: '../web/assets/img/sprites/'
					//config: 'config.rb'
				}
			}
		},

		// https://github.com/sindresorhus/grunt-shell
		shell: {
			styledocco: {
				command: [
					'cd ../web/assets/styledocco',
					'styledocco -n "AG2R_CSS" css'
				].join('&&')
			}
		},

		watch: {
			options: {
				spawn: false
			},
			gruntfile: {
				files: ['../web/assets/styledocco/css/README.md', 'Gruntfile.js'],
				tasks: ['less']
			},
			stylesheets: {
				files: [
					'src/**/**/**/*.less',					
				],
				tasks: ['less']
			},

			sprites: {
				files: [
					'src/sass/*.scss',
					'../web/assets/img/sprites/icons/*.png'
				],
				tasks: ['compass']
			}
		}
	});

	//grunt.loadNpmTasks('grunt-shell');
	//grunt.loadNpmTasks('grunt-todos');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-compass');

	grunt.registerTask('default', [
		'compass',
		'less',
	]);
	grunt.registerTask('dev', [
		'watch'
	]);

};