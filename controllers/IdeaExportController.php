<?php

use Cameleon\Controller\AController;

class IdeaExportController extends AController
{
	use TAg2rController;

    public function chapter () {}

	# [GET] /ideaexport/all
	public function action_index()
	{
        $this->app()->chapter();
		return $this->render();
	}
}