<?php
use Cameleon\Controller\AController;

class JobPositionController extends AController
{
	use TAG2RController;

	public function get_jobs()
	{
		$model = new JobPosition();
		$db = $this->get_db();
		$where = [];
		$user = $this->me;
		$jobs = $model->get_array('all', ['name' => 'DESC'], $where, 0, $nb_rows, false, ['jobposition_id']);

		return $jobs;
	}
}
