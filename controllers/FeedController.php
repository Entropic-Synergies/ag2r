<?php

class FeedController extends \Cameleon\Controller\FeedController
{
	use TAG2RController;

	protected function get_auth_user()
	{
		return $this->me;
	}

	# [GET] /feed/all
	public function action_index()
	{
		$limit = 10;
		$db = $this->get_db();

		$type = $this->get_get('type', 'all');
		$criterias = [];
		if ($key = $this->get_get('key')) {
			list($object_table, $object_id) = explode('/', $key);
			$criterias['object_table'] = $object_table;
			$criterias['object_id'] = $object_id;
		}

		if (($last_id = $this->get_get('last_id')) && ($last_date = $this->get_get('last_date'))) {
			$criterias[] = $db->query_format('created_at <= %s', date('Y-m-d H:i:s', $last_date));
			$criterias[] = $db->query_format('feed_id < %s', $last_id);
		}

		$order_by = [];

		switch ($type) {
			default:
			case 'all':
				break;
			case 'popular':
				$order_by[] = '(n_likes + n_comments) DESC';
				$criterias['object_table'] = 'idea';
				$criterias['action_type'] = 'create';
				break;
			case 'community':
				$criterias['context_table'] = 'group';
				$criterias[] = $db->query_format('context_id IN (SELECT group_id FROM groups_users WHERE user_id = %s)', $this->me['user_id']);
				break;
			case 'idea':
				$criterias['object_table'] = 'idea';
				$criterias[] = $db->query_format('action_type != %s', 'status_update');
				break;
		}

		$order_by['created_at'] = 'DESC';
		$order_by['feed_id'] = 'DESC';

		$feeds = (new Feed())->no_depth()->get_array($limit, $order_by, $criterias);

		return $this->render([
			'hide_layout' => true,
			'feeds' => $feeds,
			'limit' => $limit
		]);
	}

	public function action_show()
	{
		return $this->render([
			'feed'        => (new Feed)->get_absolutely($this->get_data('id')),
		]);
	}

	public function action_destroy()
	{
		$feed_id = $this->get_data('id');

		$feed = (new Feed())->get_current($feed_id);
		if (!$feed) {
			throw new NotFoundHttpException('Feed not found');
		}

		$acl = \Cameleon\Services::get_acl();
		if (!$acl->is_granted($this->get_auth_user(), 'DELETE', $feed['key'])) {
			throw new \Cameleon\Exception\AccessDeniedException();
		}

		$db = $this->get_db();
		$this->query = "DELETE FROM feeds WHERE feed_id = " . $feed_id;
		$db->db_query($this->query);

		$feeds = (new Feed)->get_array('all', null, [
			'object_table' => $feed['object_table'],
			'object_id'    => $feed['object_id'],
		]);
		foreach ($feeds as $feed) {
			$this->delete([
				'feed_id' => $feed,
			]);
		}

		return $this->render([
			'hide_layout' => true
		]);
	}

	public function action_edit()
	{
		$feed = (new Feed)->get_absolutely($this->get_data('id'));

		if ($feed['object_table'] == 'idea' && $feed['action_type'] == 'status_update') {
			return new \Cameleon\Http\Response($feed['content']);
		}
		
		$content = 'description';
		if ($feed['object_table'] === 'post')
			$content = 'content';
		return new \Cameleon\Http\Response($feed['object'][$content]);
	}

	public function action_update()
	{
		$b = new Feed;
		$feed = $b->get_absolutely($this->get_data('id'));

		if ($feed['object_table'] == 'idea' && $feed['action_type'] == 'status_update') {
			(new Feed)->update([
				'content' => $_POST['comment']
			], [
				'feed_id' => $feed['feed_id']
			]);
			return new \Cameleon\Http\Response(nl2br($_POST['comment']));
		}
		
		$this->get_object($feed['object_table'], $feed['object_id'], $obj);

		$content = 'description';
		if ($feed['object_table'] === 'post')
			$content = 'content';
		$obj->update([
			$content => $_POST['comment']
		], [
			$feed['object_table'] . '_id' => $feed['object_id']
		]);

		return new \Cameleon\Http\Response(nl2br($_POST['comment']));
	}

	private function get_object($object_table, $object_id, &$object = null)
	{
		$class_name = ucfirst($object_table);
		$object = new $class_name;
		if (!$object instanceof \Cameleon\Model\AModel) {
			throw new InvalidArgumentException(sprintf('Invalid object_table "%s"', $object_table));
		}

		return $object->get_absolutely($object_id);
	}
}
