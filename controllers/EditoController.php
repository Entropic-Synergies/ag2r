<?php

use Cameleon\Controller\AController;

class EditoController extends AController
{
	use TAG2RController;

	# [GET] /edito
	public function get ()
	{
		return $this->render([
			'user' => $this->me,
			'edito' => $this->get_edito(),
		]);
	}

}
