<?php

use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Validator\Constraints\NotBlank;
use Cameleon\Services;
use Cameleon\Renderer;

class IdeaCategoryAccessAdmin extends \Cameleon\Admin\AdminController
{
	protected $config = [
		'delete' => true,
		'edit'   => true,
		'new'    => true,
	];

	public function configure_renderer(Renderer $renderer)
	{
		$twig = $renderer->getTwig();
		$twig->addGlobal('NEED_VALIDATION', NEED_VALIDATION);
	}

	public function getModel()
	{
		return new IdeaCategoryAccess();
	}

	public function buildForm(FormBuilder $builder)
	{
		$builder
			->add('idea_category_id', 'choice', array(
				'label'       => 'Thématique',
				'choices'     => IdeaCategory::get_categories_tree($filterIdea = false, $all = true),
				'required'    => true,
				'empty_value' => 'Choisir ...',
				'constraints' => [
					new \Symfony\Component\Validator\Constraints\NotNull()
				]
			))
			->add('user_id', new \Cameleon\Admin\Form\UserType(), array(
				'label' => 'Utilisateur',
				'constraints' => [
					new \Symfony\Component\Validator\Constraints\NotNull()
				]
			))
            ->add('jobposition_id', 'choice', array(
				'required' => false,
				'label'   => 'Poste occupé',
				'choices' => $this->get_job_positions(),
				'empty_value' => 'Choisir ...',
			))
            ->add('location', 'choice', array(
                'required' => false,
                'label'   => 'Lieu',
                'choices' => $this->get_locations(),
                'empty_value' => 'Choisir ...',
            ));
	}

	public function get_job_positions()
	{
		$job_positions = (new JobPosition())->get_array('all', ['name' => 'ASC']);
		foreach ($job_positions as $job_position) {
			$items[$job_position['jobposition_id']] = $job_position['name'];
		}

		return $items;
	}

    public function get_locations()
    {
        $location = [
            'AGDE'                   => 'AGDE',
            'AGEN'                   => 'AGEN',
            'AIX EN PROVENCE'        => 'AIX EN PROVENCE',
            'AJACCIO'                => 'AJACCIO',
            'ALBI'                   => 'ALBI',
            'AMIENS'                 => 'AMIENS',
            'ANGERS'                 => 'ANGERS',
            'ANNECY'                 => 'ANNECY',
            'AURILLAC'               => 'AURILLAC',
            'AVIGNON'                => 'AVIGNON',
            'BALMA'                  => 'BALMA',
            'BASTIA'                 => 'BASTIA',
            'BAYONNE'                => 'BAYONNE',
            'BERGERAC'               => 'BERGERAC',
            'BESANCON'               => 'BESANCON',
            'BEZIERS'                => 'BEZIERS',
            'BLOIS'                  => 'BLOIS',
            'BORDEAUX'               => 'BORDEAUX',
            'BOURG EN BRESSE'        => 'BOURG EN BRESSE',
            'BOURGES'                => 'BOURGES',
            'BOVES'                  => 'BOVES',
            'BREST'                  => 'BREST',
            'BRIVE LA GAILLARDE'     => 'BRIVE LA GAILLARDE',
            'CAEN'                   => 'CAEN',
            'CAHORS'                 => 'CAHORS',
            'CALAIS'                 => 'CALAIS',
            'CASTRES'                => 'CASTRES',
            'CAYENNE'                => 'CAYENNE',
            'CHALON SUR SAONE'       => 'CHALON SUR SAONE',
            'CHAMBERY'               => 'CHAMBERY',
            'CHARTRES'               => 'CHARTRES',
            'CHATILLON'              => 'CHATILLON',
            'CLERMONT FERRAND'       => 'CLERMONT FERRAND',
            'COLMAR'                 => 'COLMAR',
            'COMPIEGNE'              => 'COMPIEGNE',
            'CRETEIL'                => 'CRETEIL',
            'DAX'                    => 'DAX',
            'DIGNE'                  => 'DIGNE',
            'DIJON'                  => 'DIJON',
            'EPINAL'                 => 'EPINAL',
            'FLERS EN ESCREBIEUX'    => 'FLERS EN ESCREBIEUX',
            'FOIX'                   => 'FOIX',
            'FORT DE FRANCE'         => 'FORT DE FRANCE',
            'GAP'                    => 'GAP',
            'GRENOBLE'               => 'GRENOBLE',
            'GUERET'                 => 'GUERET',
            'HEROUVILLE SAINT CLAIR' => 'HEROUVILLE SAINT CLAIR',
            'ISLE'                   => 'ISLE',
            'LA MOTTE SERVOLEX'      => 'LA MOTTE SERVOLEX',
            'LA ROCHE SUR YON'       => 'LA ROCHE SUR YON',
            'LA ROCHELLE'            => 'LA ROCHELLE',
            'LA VALETTE DU VAR'      => 'LA VALETTE DU VAR',
            'LANGUEUX'               => 'LANGUEUX',
            'LE HAVRE'               => 'LE HAVRE',
            'LILLE CEDEX'            => 'LILLE CEDEX',
            'LILLE'                  => 'LILLE',
            'LIMOGES'                => 'LIMOGES',
            'LOGNES'                 => 'LOGNES',
            'LORIENT'                => 'LORIENT',
            'LYON'                   => 'LYON',
            'MALAKOFF'               => 'MALAKOFF',
            'MARSEILLE'              => 'MARSEILLE',
            'MAXEVILLE'              => 'MAXEVILLE',
            'MAZAMET'                => 'MAZAMET',
            'MELUN'                  => 'MELUN',
            'METZ'                   => 'METZ',
            'MONACO'                 => 'MONACO',
            'MONS-EN-BAROEUL'        => 'MONS-EN-BAROEUL',
            'MONT DE MARSAN'         => 'MONT DE MARSAN',
            'MONTAUBAN'              => 'MONTAUBAN',
            'MONTELIMAR'             => 'MONTELIMAR',
            'MONTPELLIER'            => 'MONTPELLIER',
            'MOULINS'                => 'MOULINS',
            'MULHOUSE'               => 'MULHOUSE',
            'NANCY'                  => 'NANCY',
            'NANTES'                 => 'NANTES',
            'NARBONNE'               => 'NARBONNE',
            'NEVERS'                 => 'NEVERS',
            'NICE'                   => 'NICE',
            'NIMES'                  => 'NIMES',
            'NIORT'                  => 'NIORT',
            'NOUMEA'                 => 'NOUMEA',
            'ORLEANS'                => 'ORLEANS',
            'PAPEETE'                => 'PAPEETE',
            'PARIS'                  => 'PARIS',
            'PAU'                    => 'PAU',
            'PERIGUEUX'              => 'PERIGUEUX',
            'PERPIGNAN'              => 'PERPIGNAN',
            'PETIT QUEVILLY'         => 'PETIT QUEVILLY',
            'POINTE A PITRE'         => 'POINTE A PITRE',
            'POITIERS'               => 'POITIERS',
            'QUIMPER'                => 'QUIMPER',
            'REIMS'                  => 'REIMS',
            'RENNES'                 => 'RENNES',
            'ROUEN'                  => 'ROUEN',
            'SAINT BENOIT'           => 'SAINT BENOIT',
            'SAINT BRIEUC'           => 'SAINT BRIEUC',
            'SAINT ETIENNE'          => 'SAINT ETIENNE',
            'SAINT MARIE LA REUNION' => 'SAINT MARIE LA REUNION',
            'SAINT NAZAIRE'          => 'SAINT NAZAIRE',
            'SAINT PIERRE'           => 'SAINT PIERRE',
            'SAINT QUENTIN'          => 'SAINT QUENTIN',
            'SAINT SATURNIN'         => 'SAINT SATURNIN',
            'SAINT VIT'              => 'SAINT VIT',
            'SETE'                   => 'SETE',
            'SEYNOD'                 => 'SEYNOD',
            'SAINT HERBLAIN'         => 'SAINT HERBLAIN',
            'STRASBOURG'             => 'STRASBOURG',
            'TAISSY'                 => 'TAISSY',
            'TARBES'                 => 'TARBES',
            'TOULON'                 => 'TOULON',
            'TOULOUSE'               => 'TOULOUSE',
            'TOURS'                  => 'TOURS',
            'TROYES'                 => 'TROYES',
            'VANNES'                 => 'VANNES',
            'VOISINS LE BRETONNEUX'  => 'VOISINS LE BRETONNEUX',
        ];

        return $location;
    }

	public function action_index()
	{
		$columns = [
			'idea_category.name' => [
				'label' => 'Thématique',
			],
			'user.name'          => [
				'label' => 'Auteur',
				'multiple'   => false,
			],
			'jobposition.name'   => [
				'label' => 'Poste occupé',
			],
            'location'           => [
                'label' => 'Lieu',
            ],
		];

		$filters = [
			[
				'name'  => 'user_id',
				'label' => 'Auteur',
				'type'  => 'user',
			], [
				'name'    => 'idea_category_id',
				'label'   => 'Thématique',
				'type'    => 'choices',
				'choices' => (new IdeaCategoryAccess())->get_categories_auhenticate(),
			], [
				'name'    => 'jobposition_id',
				'label'   => 'Poste occupé',
				'type'    => 'choices',
				'choices' => $this->get_job_positions(),
			], [
                'name'    => 'location',
                'label'   => 'Lieu',
                'type'    => 'choices',
                'choices' => $this->get_locations(),
            ],
		];

		return $this->get_action_index($columns, 20, $filters, [
			'selection_actions' => [
				'delete'  => 'Supprimer',
			]
		]);
	}

	protected function perform_action($action, array $items, $message = null)
	{
		switch ($action) {
			case 'delete':
				foreach ($items as $ideacategoryaccess_id) {
					$this->getModel()->delete([
						'ideacategoryaccess_id' => $ideacategoryaccess_id
					]);
				}
				Services::get_flash_messenger()->add_flash('info', 'Les accès ont bien été supprimés.');
				break;
			default:
				throw new \Exception(sprintf('Invalid action "%s"', $action));
				break;
		}
	}
} 