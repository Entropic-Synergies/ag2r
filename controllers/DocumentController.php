<?php

use Cameleon\Exception\FileUploadException;
use Cameleon\Exception\NotFoundHttpException;

class DocumentController extends \Cameleon\Controller\AController
{
	use TAG2RController;

	# # # # # # # # # # # # # # # # # # # # # # # # #
	# [GET] /document/all
	public function action_index()
	{
		$page_size = 10;
		$offset = ($this->get_get('page', 1) - 1) * $page_size;

		$params = [];
		$criterias = [];
		$this->initialize_container($params);
		if (isset($params['experimentation'])) {
			$criterias['object_table'] = 'experimentation';
			$criterias['object_id'] = $params['experimentation']['experimentation_id'];
		} elseif (isset($params['idea'])) {
			$criterias['object_table'] = 'idea';
			$criterias['object_id'] = $params['idea']['idea_id'];
		} elseif (isset($params['innovation'])) {
			$db = $this->get_db();
			$criterias = $db->query_format('(object_table = "innovation" AND object_id = %s) OR
			(
				object_table = "experimentation" AND object_id IN (SELECT experimentation_id FROM experimentations WHERE innovation_id = %s)
			)',
				$params['innovation']['innovation_id'],
				$params['innovation']['innovation_id']
			);
		}

		$documents = (new Document)->get_array($page_size, ['created_at'  => 'DESC',
															'document_id' => 'DESC'], $criterias, $offset, $nb_rows, true, ['document_id']);

		$params['documents_count'] = $nb_rows;
		$params['documents'] = $documents;
		$params['pages_count'] = ceil($nb_rows / $page_size);

		return $this->render($params);
	}

	public function action_create()
	{
		$params = [];
		$this->initialize_container($params);

		$key = isset($params['idea']) ? $params['idea']['key'] : (isset($params['experimentation']) ? $params['experimentation']['key'] : $params['innovation']['key']);
		$flasher = \Cameleon\Services::get_flash_messenger();
		if (!isset($_FILES['file'])) {
			throw new \InvalidArgumentException('Missing file "file"');
		}
		$uploaded_file = $_FILES['file'];
		try {
			$data = $_POST;
			$data['user_id'] = $this->me['user_id'];
			$file = (new Document())->upload($uploaded_file, $key, $data);
			$flasher->add_flash('success', 'Le document a bien été ajouté');
			if (isset($_GET['page'])) {
				unset($_GET['page']);
			}
		} catch (FileUploadException $e) {
			$flasher->add_flash('error', $e->getMessage());
		}

		return $this->redirect('/document/all?' . http_build_query($_GET));
	}

	public function download()
	{
		$document_id = $this->get_get('document_id');

		$document = (new Document())->get_absolutely($document_id);

		$file = $document['file_src'];
		if (!file_exists($file)) {
			throw new NotFoundHttpException('Fichier introuvable');
		}
		$size = filesize($file);

		$headers['Content-Type'] = 'application/octet-stream';
		$headers['Content-Transfer-Encoding'] = 'binary';
		$headers['Content-Disposition'] = 'attachment; filename="' . $document['original_filename'] . '"';
		$headers['Expires'] = '0';
		$headers['Cache-Control'] = 'must-revalidate';
		$headers['Pragma'] = 'public';
		$headers['Content-Length'] = (string)$size;

		return new \Cameleon\Http\StreamedResponse(function () use ($file, $size) {
				readfile($file);
			},
			200,
			'OK',
			$headers);
	}

	public function action_destroy()
	{
		(new Document())->delete([
			'document_id' => $this->get_data('id')
		]);;

		return new \Cameleon\Http\Response();
	}

	protected function initialize_container(array &$params)
	{
		if ($filter = $this->get_get('experimentation_id')) {
			$params['experimentation'] = (new Experimentation())->no_depth()->get_absolutely($filter, 'Expérimentation introuvable');
			$_GET['innovation_id'] = $params['experimentation']['innovation_id'];
		} elseif ($filter = $this->get_get('idea_id')) {
			$params['idea'] = (new Idea())->no_depth()->get_absolutely($filter, 'Idée introuvable');

			return;
		}

		if (!($filter = $this->get_get('innovation_id'))) {
			throw new \InvalidArgumentException('Missing param "innovation_id"');
		}
		$params['innovation'] = (new Innovation())->no_depth()->get_absolutely($filter, 'Innovation introuvable');
	}
}
