<?php

use Cameleon\Controller\AController;
use Cameleon\Services;

class PostController extends AController
{

	use TAG2RController;

	# [POST] /post/new
	public function action_create()
	{
		$data = $_POST;
		$data['user_id'] = $this->me['user_id'];
		try {
			(new Post)->create($data);
		} catch (InvalidArgumentException $e) {
			Services::get_flash_messenger()->add_flash('error', $e->getMessage());
		}

		return $this->redirect($_SERVER['HTTP_REFERER']);
	}

	# [POST] /post/embedly
	public function embedly()
	{
		$url = urlencode($_POST['url']);
		$embedly = 'http://api.embed.ly/1/oembed?key=a085b61c3d3f4242a8351e66f362c96e&width=530&url=' . $url . '&thumbnail_width=100';
		$curl = curl_init($embedly);

		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$object = curl_exec($curl);
		curl_close($curl);

		$response = new \Cameleon\Http\Response($object);
		$response->setHeader('Content-Type', 'application/json');

		return $response;
	}

	# [POST] /post/get_iframe
	public function get_iframe()
	{
		$feed = (new Feed($_POST['feed_id']))->get_current();
		$post = (new Post($feed['object_id']))->get_current();
		$post['feed_id'] = $feed['feed_id'];

		$response = new \Cameleon\Http\Response(json_encode($post));
		$response->setHeader('Content-Type', 'application/json');

		return $response;
	}
}