<?php
use Cameleon\Controller\AuthController as AuthControllerBase;

class AuthController extends AuthControllerBase
{
	static protected $saml2_vendor_path = '/../vendor/saml/simplesamlphp/www/saml2/sp/';

	# [GET] /auth/login
	public function login()
	{
		/*
		if ($user_id = $this->get_get('user_id')) {
			$_SESSION['user_id']      = $user_id;
			$_SESSION['login_method'] = 'login';

			(new ActivityLog)->user_login($user_id);

			return $this->redirect('/');
		}
		*/

		/*
		$users = (new User())->no_depth()->get_array(20, 'user_id ASC');
		$users_list = '';
		foreach ($users as $user) {
			$users_list .= sprintf('<li><a href="?user_id=%s">%s</a> (%s)</li>', $user['user_id'], $user['full_name'], implode(', ', $user['roles']));
		}

		return new \Cameleon\Http\Response(sprintf('
		<h1>Log as</h1>
		<ul>
			%s
		</ul>
		<hr>
		<a href="/auth/saml_endpoint">Connexion SAML</a>
		', $users_list));
		*/
		return $this->render([], 200, 'OK', 'layout/c.html.twig');
	}

	# [GET] /auth/lostpassword
	public function lostpassword()
	{
		if ($_POST) {
			if (!$this->get_post('email', false)) {
				\Cameleon\Services::get_flash_messenger()->add_flash('error', 'Identifiant inconnu.');

				return $this->redirect('/auth/lostpassword');
			}
			$user = (new User)->get_one('user_id', "`email`='" . $this->get_post('email', '') . "'");
			if ($user) {
				$password = $this->generate_password();
				(new User)->update([
					'password' => (new User)->hash_password($password)
				], [
					'user_id' => $user['user_id'],
				]);
				(new Mail)->send(['email' => $user['email']], 'lost_password.html.twig', ['email'    => $user['email'],
																						  'password' => $password]);
				\Cameleon\Services::get_flash_messenger()->add_flash('success', 'Consultez votre email pour connaitre votre nouveau mot de passe.');

				return $this->redirect('/auth/login');
			} else {
				\Cameleon\Services::get_flash_messenger()->add_flash('error', 'Utilisateur inconnu.');

				return $this->redirect('/auth/lostpassword');
			}
		}

		return $this->render([], 200, 'OK', 'layout/c.html.twig');
	}

	# [POST] /auth/action_login
	public function action_login()
	{
		if(!$this->get_post('email', false) && !$this->get_post('password', false)){
			return $this->redirect('http://idees');
		}

		$status = (new User)->login(
			$this->get_post('email', false),
			$this->get_post('password', false)
		);

		if (!User::is_signed_in()) {
			\Cameleon\Services::get_flash_messenger()->add_flash('error', 'Identifiant ou mot de passe incorrect. Veuillez réessayer.');

			return $this->redirect('/auth/login');
		}

		if (!isset($_COOKIE['remember_me'])) {
			setcookie('remember_me', $_SESSION['user_id'] . '-' . md5($this->get_post('email')), strtotime("2 days"), '/');
		}
		$_SESSION['login_method'] = 'login';
        if(isset($_SESSION['idea_id'])){
            return $this->redirect('/idea/'.$_SESSION['idea_id']);
        }

		return $this->redirect('/');
	}

	public function saml_endpoint()
	{
		$attributes = SamlController::auth_init();

		$first_name = $attributes['prenom'][0];
		$last_name  = $attributes['nom'][0];
		$email      = $attributes['emailAddress'][0];
		$login_name = $attributes['login'][0];
		$fields = explode('@', $login_name);
		$domain = 'comptes\\';
		if ($fields[1] == 'AG2RPRD.GIE.NET')
			$domain = 'ag2rprd\\';
		if ($fields[1] == 'LAMONDIALE.COM')
			$domain = 'mons\\';
		$login_name = $domain . $fields[0];
		// Split with @
		$data  = [
			'last_connection' => new DateTime(),
			'login_name'      => $login_name
		];
		$users = (new User)->get_array(1, [], [
			'email' => $email
		]);

		if (count($users) === 0) {
			// User does not exist, we create it
			$password = $this->generate_password();
			$user     = (new User)->create(array_merge($data, [
				'first_name' => $first_name,
				'last_name'  => $last_name,
				'roles'    => [],
				'email'    => $email,
				'password' => (new User)->hash_password($password),
			]));
			// Send email
			(new Mail)->send(['email' => $email], 'create_password.html.twig', ['email'    => $email,
																				'password' => $password]);
			$_SESSION['ask_cgu'] = '1';
		} else {
			// User already exists
			$user = reset($users);
			(new User)->update($data, [
				'user_id' => $user['user_id'],
			]);
			if ($user['cgu'] == 0)
				$_SESSION['ask_cgu'] = '1';
		}
		$_SESSION['user_id']      = $user['user_id'];
		$_SESSION['login_method'] = 'saml';

		return $this->redirect('/');
	}

	# [GET] /auth/logout
	public function logout()
	{
		(new ActivityLog)->user_logout($_SESSION['user_id']);

		(new User())->logout();

		\SamlController::logout();

		return $this->redirect(
			$this->get_post('return_to', '/')
		);
	}

	private function generate_password()
	{
		return substr(md5(uniqid(time())), 0, 6);
	}
}