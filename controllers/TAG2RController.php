<?php

use Cameleon\Exception\AccessDeniedException;
use Cameleon\Renderer;

trait TAG2RController
{
	public function __construct()
	{
		try {
			$this->app();
		} catch (AccessDeniedException $e) {
            if(isset($_SERVER['REDIRECT_URL'])) {
                $param = explode('/', $_SERVER['REDIRECT_URL']);
                if($param[1] == 'idea' && isset($param[2])){
                    $_SESSION['idea_id'] = $param[2];
                }
            }
			header('Location: /auth/login');
			exit;
		}
	}

	public function app()
	{
		if (!User::is_signed_in()) {
			throw new AccessDeniedException('L\'utilisateur doit être authentifié');
		}

		$this->me = false;
		if ($user_id = (new User)->get_signed_id()) {
			$this->me = (new User)->get_signed_in();
		}
		if (!$this->me) {
			(new User)->logout();
			throw new AccessDeniedException('L\'utilisateur doit être authentifié');
		}
		\Cameleon\Services::set_me($this->me);

		return $this;
	}

	public function configure_renderer(Renderer $renderer)
	{
		$twig = $renderer->getTwig();

		$me = $this->me;

		$idea_categories = (new IdeaController)->get_categories($me);
		$twig->addGlobal('sidebar_idea_categories', $idea_categories);

		$recommended_users = (new User)->get_recommended();
		$twig->addGlobal('recommended_users', $recommended_users);

		$recommended_groups = (new Group)->get_recommended();
		$twig->addGlobal('recommended_groups', $recommended_groups);

		$twig->addGlobal('SOCIAL_ACTIVE', SOCIAL_ACTIVE);
		$twig->addGlobal('NEED_VALIDATION', NEED_VALIDATION);

		$function = new \Twig_SimpleFunction('uniqid', 'uniqid');
		$twig->addFunction($function);

		$unread_messages_count = null;
		$function = new \Twig_SimpleFunction('get_unread_messages_count', function () use (&$me, &$unread_messages_count) {
			if (null !== $unread_messages_count) {
				return $unread_messages_count;
			}

			return $unread_messages_count = (new Message())->count(['user_id'    => $me['user_id'],
																	'to_user_id' => $me['user_id'],
																	'status'     => 'unread']);
		});
		$twig->addFunction($function);

		$getedito = null;
		$function = new \Twig_SimpleFunction('getedito', function () use (&$me, &$getedito) {
			if (null !== $getedito) {
				return $getedito;
			}

			return $getedito = $this->get_edito();
		});
		$twig->addFunction($function);

		$pending_requests_count = null;
		$function = new \Twig_SimpleFunction('get_pending_requests_count', function () use (&$me, &$pending_requests_count) {
			if (null !== $pending_requests_count) {
				return $pending_requests_count;
			}

			return $pending_requests_count = (new User())->get_pending_requests_count($me['user_id']);
		});
		$twig->addFunction($function);

		$pending_ideas_count = null;
		$function = new \Twig_SimpleFunction('get_pending_ideas_count', function () use (&$me, &$pending_ideas_count) {
			if (null !== $pending_ideas_count) {
				return $pending_ideas_count;
			}

			return $pending_ideas_count = (new User())->get_pending_ideas_count($me['user_id']);
		});
		$twig->addFunction($function);

		$pending_drafts_count = null;
		$function = new \Twig_SimpleFunction('get_pending_drafts_count', function () use (&$me, &$pending_drafts_count) {
			if (null !== $pending_drafts_count) {
				return $pending_drafts_count;
			}

			return $pending_drafts_count = (new User())->get_pending_drafts_count($me['user_id']);
		});
		$twig->addFunction($function);

		$pending_validations_count = null;
		$function = new \Twig_SimpleFunction('get_pending_validations_count', function () use (&$me, &$pending_validations_count) {
			if (null !== $pending_validations_count) {
				return $pending_validations_count;
			}

			return $pending_validations_count = (new User())->get_pending_validations_count($me['user_id']);
		});
		$twig->addFunction($function);

		$self = $this;
		$filter = new \Twig_SimpleFilter('stripAccents', function ($str) use ($self) {
			return $self->stripAccents($str);
		});
		$twig->addFilter($filter);
		
		$intranet_profile_url_template = 'http://intranet.ag2rlamondiale.fr/EspaceAnnuaire/Pages/VisuProfil.aspx?LoginName=';
		$function = new \Twig_SimpleFunction('get_intranet_profile_url', function ($login_name) use ($intranet_profile_url_template) {
			return $intranet_profile_url_template . $login_name;
		});
		$twig->addFunction($function);
		
		$function = new \Twig_SimpleFunction('get_idea_status_name', function($status) {
			return Idea::$statuses[$status];
		});
		$twig->addFunction($function);
	}

	public function stripAccents($str)
	{
		$str = str_replace(
			array(
				'à', 'â', 'ä', 'á', 'ã', 'å',
				'î', 'ï', 'ì', 'í',
				'ô', 'ö', 'ò', 'ó', 'õ', 'ø',
				'ù', 'û', 'ü', 'ú',
				'é', 'è', 'ê', 'ë',
				'ç', 'ÿ', 'ñ',
				'À', 'Â', 'Ä', 'Á', 'Ã', 'Å',
				'Î', 'Ï', 'Ì', 'Í',
				'Ô', 'Ö', 'Ò', 'Ó', 'Õ', 'Ø',
				'Ù', 'Û', 'Ü', 'Ú',
				'É', 'È', 'Ê', 'Ë',
				'Ç', 'Ÿ', 'Ñ',
			),
			array(
				'a', 'a', 'a', 'a', 'a', 'a',
				'i', 'i', 'i', 'i',
				'o', 'o', 'o', 'o', 'o', 'o',
				'u', 'u', 'u', 'u',
				'e', 'e', 'e', 'e',
				'c', 'y', 'n',
				'A', 'A', 'A', 'A', 'A', 'A',
				'I', 'I', 'I', 'I',
				'O', 'O', 'O', 'O', 'O', 'O',
				'U', 'U', 'U', 'U',
				'E', 'E', 'E', 'E',
				'C', 'Y', 'N',
			), $str);

		return $str;
	}

	public function get_edito ()
	{
		$json_path = CAMELEON_DIR_WEB . 'files/edito.json';
		if(!is_file($json_path))
			file_put_contents($json_path, json_encode([
				'content' => 'Le 14 mai 2013, le gouvernement a adopté la loi sur la sécurisation de l’emploi qualifié d’ « accord historique ». Cet accord va permettre aux entreprises de retrouver une compétitivité au niveau international et aux salariés de bénéficier d’un marché du travail plus réactif avec des garanties renforcées.Le 14 mai 2013, le gouvernement a adopté la loi sur la sécurisation de l’emploi qualifié d’ « accord historique ». Cet accord va permettre aux entreprises de retrouver une compétitivité au niveau international et aux salariés de bénéficier d’un marché du travail plus réactif avec des garanties renforcées.Le 14 mai 2013, le gouvernement a adopté la loi sur la sécurisation de l’emploi qualifié d’ « accord historique ». Cet accord va permettre aux entreprises de retrouver une compétitivité au niveau international et aux salariés de bénéficier d’un marché du travail plus réactif avec des garanties renforcées.Le 14 mai 2013, le gouvernement a adopté la loi sur la sécurisation de l’emploi qualifié d’ « accord historique ». Cet accord va permettre aux entreprises de retrouver une compétitivité au niveau international et aux salariés de bénéficier d’un marché du travail plus réactif avec des garanties renforcées.Le 14 mai 2013, le gouvernement a adopté la loi sur la sécurisation de l’emploi qualifié d’ « accord historique ». Cet accord va permettre aux entreprises de retrouver une compétitivité au niveau international et aux salariés de bénéficier d’un marché du travail plus réactif avec des garanties renforcées.Le 14 mai 2013, le gouvernement a adopté la loi sur la sécurisation de l’emploi qualifié d’ « accord historique ». Cet accord va permettre aux entreprises de retrouver une compétitivité au niveau international et aux salariés de bénéficier d’un marché du travail plus réactif avec des garanties renforcées.',
				'date' => '2014-01-22 17:45:12',
			]));

		$edito = json_decode(file_get_contents($json_path), true);

		$remaining_sec = strtotime($edito['date']) - time();

		$edito['timer_html'] = false;

		if ($remaining_sec > (48*3600)) {
			$d = floor($remaining_sec / (24*3600));
			$remaining_sec -= $d * 24*3600;
			$h = floor($remaining_sec / 3600);
			$edito['timer_html'] = '<span>' . $d . '</span> j <span>' . $h . '</span> h&nbsp;&nbsp;';

		}elseif($remaining_sec > 0){
			$h = floor($remaining_sec / 3600);
			$remaining_sec -= $h * 3600;
			$m = floor($remaining_sec / 60);
			$edito['timer_html'] = '<span>' . $h . '</span> h <span>' . $m . '</span> min';
		}

		return $edito;

	}

	public function get_instructions ()
	{
		$json_path = CAMELEON_DIR_WEB . 'files/instructions.json';
		if(!is_file($json_path))
			file_put_contents($json_path, json_encode([
				'content' => 'Mode d\'emploi.',
			]));

		$instructions = json_decode(file_get_contents($json_path), true);

		return $instructions;

	}

}