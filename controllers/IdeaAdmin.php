<?php

use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Validator\Constraints\NotBlank;
use Cameleon\Services;
use Cameleon\Renderer;

class IdeaAdmin extends \Cameleon\Admin\AdminController
{
	protected $config = [
		'delete' => true,
		'edit'   => true,
		'new'    => false,
	];

	public function configure_renderer(Renderer $renderer)
	{
		$twig = $renderer->getTwig();
		$twig->addGlobal('NEED_VALIDATION', NEED_VALIDATION);
	}

	public function getModel()
	{
		return new Idea();
	}

	protected function transform_post_data(&$data)
	{
		$data['closed'] = isset($data['closed']) ? $data['closed'] : false;
	}

	public function buildForm(FormBuilder $builder)
	{
		$builder
			->add('name', 'text', array(
				'label'       => 'Nom',
				'constraints' => array(
					new NotBlank(),
				),
			))
			->add('description', 'textarea', array(
				'required' => false,
			))
			->add('idea_category_id', 'choice', array(
				'label'    => 'Thématique',
				'choices'  => (new IdeaCategory())->get_all_categories(),
				'required' => false,
			))->add('operators', new \Cameleon\Admin\Form\UsersType(), array(
				'label' => 'Pilotes',
			))->add('closed', 'checkbox', array(
				'label'    => 'Close',
				'required' => false,
			));
	}

	public function action_destroy()
	{
		$model = $this->getModel();

		$idea = $model->get_absolutely($this->get_data('id'));
		if ($idea['status'] === 'experiment') {
			Services::get_flash_messenger()->add_flash('error', 'Vous ne pouvez pas supprimer une idée qui a été expérimentée.');
			return $this->redirect('/admin/' . $this->get_chapter() . '/all');
		}

		return parent::action_destroy();
	}

	protected function perform_action($action, array $items)
	{
		$message = $this->get_post('message', null);
		$create_feed = false;
		switch ($action) {
			case 'experiment':
			case 'accept':
				foreach ($items as $idea_id) {
					(new Innovation())->import_idea($idea_id, $this->me);
				}
				Services::get_flash_messenger()->add_flash('success', 'Les idées ont bien été retenues.');
				$this->notify($idea_id, $this->me, 'experiment');
				break;
			case 'refused':
            case 'deny':
				foreach ($items as $idea_id) {
					(new Idea())->deny($idea_id, $this->me, $message);
				}
				Services::get_flash_messenger()->add_flash('info', 'Les idées ont bien été refusées.');
				$this->notify($idea_id, $this->me, 'refused');
				$create_feed = true;
				break;
			case 'waiting':
				foreach ($items as $idea_id) {
					(new Idea())->waiting($idea_id, $this->me);
				}
				Services::get_flash_messenger()->add_flash('info', 'Les idées ont bien été passées en statut : '.(Idea::$statuses[$action]));
				$this->notify($idea_id, $this->me, 'waiting');
				$create_feed = true;
				break;
			case 'duplicate':
				foreach ($items as $idea_id) {
					(new Idea())->duplicate($idea_id, $this->me);
				}
				Services::get_flash_messenger()->add_flash('info', 'Les idées ont bien été passées en statut : '.(Idea::$statuses[$action]));
				$this->notify($idea_id, $this->me, 'duplicate');
				$create_feed = true;
				break;
			case 'estimate':
				foreach ($items as $idea_id) {
					(new Idea())->estimate($idea_id, $this->me);
				}
				Services::get_flash_messenger()->add_flash('info', 'Les idées ont bien été passées en statut : '.(Idea::$statuses[$action]));
				$this->notify($idea_id, $this->me, 'estimate');
				$create_feed = true;
				break;
			case 'confirmed':
				foreach ($items as $idea_id) {
					(new Idea())->confirm($idea_id, $this->me);
				}
				Services::get_flash_messenger()->add_flash('info', 'Les idées ont bien été passées en statut : '.(Idea::$statuses[$action]));
				$this->notify($idea_id, $this->me, 'confirmed');
				$create_feed = true;
				break;
			case 'close':
			case 'unclose':
				$closed = $action === 'close';
				foreach ($items as $idea_id) {
					(new Idea())->update([
						'closed' => $closed,
					], [
						'idea_id' => $idea_id
					]);
				}
				$msg = $closed ? 'Les idées ont bien été clôturées.' : 'Les idées ont bien été réouvertes.';
				Services::get_flash_messenger()->add_flash('info', $msg);
				//$this->notify($idea_id, $this->me, ($closed ? 'close' : 'unclose'));
				break;
            case 'attribute_idea':
                $operator = $this->get_post('attribute_idea', null);
                foreach ($items as $idea_id) {
                    (new Idea())->add_operators($idea_id, [$operator]);
                }
                $msg = 'Les pilotes ont bien été assigner aux idées sélectionnées.';
                Services::get_flash_messenger()->add_flash('info', $msg);
                $create_feed = true;
                break;
            case 'update_status':
                $status = $this->get_post('update_status', null);
                $items = explode(',', $items[0]);
                if(count($items) > 0){
                    foreach ($items as $idea_id) {
                        (new Idea())->update([
                            'status' => $status,
                        ], [
                            'idea_id' => $idea_id
                        ]);
                    }
                    $msg = 'La mise à jour des status à réussi';
                    Services::get_flash_messenger()->add_flash('info', $msg);
                    $create_feed = true;
                }
                break;
			default:
				throw new \Exception(sprintf('Invalid action "%s"', $action));
				break;
		}

		if ($create_feed == true) {
			$user = $this->me;
			foreach ($items as $idea_id) {
				$idea = (new Idea)->get_current($idea_id);

				(new Feed)->create([
					'user_id' => $user['user_id'],
					'object_table' => 'idea',
					'object_id' => $idea_id,
					'action_type' => 'status_update',
					'content' => $message,
					'name' => $idea['status'],
				]);

				$comment = $user['first_name'].' '.$user['last_name'].' vient de passer l\'idée en statut ';
				$comment = $comment.(Idea::$statuses[$idea['status']]);
				$comment = $comment."\n";
				$comment = $comment.$message;
				
				(new ObjectComment)->add_comment(new Idea, $idea['idea_id'], $user, $comment);
			}
		}
	}

	public function notify($idea_id, $user, $status) {
		$idea = (new Idea())->get_absolutely($idea_id);
		if ($user['user_id'] != $idea['user_id'])
			(new Notification())->notify($user['user_id'], 'idea/' . $idea_id, $status, null, null, [['user_id' => $idea['user_id']]]);
	}

	public function action_index()
	{
		$status = [
			'waiting'   => 'A l\'étude',
			'confirmed' => 'Mise en oeuvre',
			'refused'   => 'Refusée',
			'draft'   => 'Brouillon',
			'duplicate' => 'Déjà existant',
			'study' => 'Idée à étudier',
			'validation' => 'Attente validation',
			'estimate' => 'En cours d\'estimation',
			'experiment' => 'Retenue',
			'deleted' => 'Supprimée'
		];
		$columns = [
			'name'               => [
				'label' => 'Nom',
			],
			'description'        => [
				'label'    => 'Description',
				'truncate' => 50
			],
			'idea_category.name' => [
				'label' => 'Thématique',
			],
			'user.name'          => [
				'label' => 'Auteur',
			],
			'status'             => [
				'label'        => 'Etat',
				'translations' => $status,
				'type'         => 'idea_status'
			],
			'_operators'         => [
				'label' => 'Pilotes',
				'type'  => 'mailto',
			],
			'closed'             => [
				'label' => 'Close',
				'type'  => 'closed'
			],
			'workflow'           => [
				'label' => 'Actions'
			]
		];

		$filters = [
			[
				'name'  => 'name',
				'label' => 'Titre',
				'type'  => 'query',
			], [
				'name'  => 'description',
				'label' => 'Mot clé',
				'type'  => 'query',
			],
			[
				'name'  => 'created_at',
				'label' => 'Date de création',
				'type'  => 'date',
			], [
				'name'  => 'user_id',
				'label' => 'Auteur',
				'type'  => 'user',
			], [
				'name'  => 'operator',
				'label' => 'Pilote',
				'type'  => 'user',
			], [
				'name'    => 'idea_category_id',
				'label'   => 'Thématique',
				'type'    => 'choices',
				'choices' => (new Idea())->get_distinct_categories_idea(),
			], [
				'name'    => 'status',
				'label'   => 'Etat',
				'type'    => 'choices',
				'choices' => $status,
			],
			[
				'name'  => 'ratings_count',
				'label' => 'Nombre de votes',
				'type'  => 'text',
			], [
				'name'  => 'closed',
				'label' => 'Fermées',
				'type'  => 'boolean',
			],
		];

		return $this->get_action_index($columns, 20, $filters, $status, [
			'selection_actions' => [
				'close'   => 'Clôturer',
				'unclose' => 'Réouvrir',
                'attribute_idea' => 'Attribuer les idées à ',
                'update_status' => 'Passer en statut '
			]
		]);
	}

	protected function get_action_index(array $columns, $limit = 20, array $filters = [], array $status = [], array $params = [])
	{
		$page = $this->get_get('page', 1);
		$model = $this->getModel();
		$conditions = $this->get_filters_conditions($model, $filters, $joins, $having);
		$offset = ($page - 1) * $limit;
		$id_name = $model->table_name . '_id';
		$data = $model->get_array($limit, 'updated_at DESC', $conditions, $offset, $nb_rows, true, [$model->table_name . 's.' . $id_name], $joins, $having);
		$path =  explode('?', $_SERVER['REQUEST_URI']);
		$path = (isset($path[1]))?$path[1]:"";

		return $this->render(array_merge([
			'data'        => $data,
			'nb_rows'     => $nb_rows,
			'pages_count' => ceil($nb_rows / $limit),
			'columns'     => $columns,
			'filters'     => $filters,
			'current_path'=> $path,
            'status'      => $status,
		], $params));
	}

	protected function handle_custom_filter(\Cameleon\Model\AModel $model, $filter_name, $operator, $value, $value2, array &$joins, array &$having)
	{
		switch ($filter_name) {
			case 'ratings_count':
				$joins[] = 'LEFT JOIN object_ratings r ON r.object_table = "' . $model->table_name . '" AND r.object_id = ideas.idea_id';
				$having = $this->get_conditions('text', 'COUNT(object_rating_id)', $operator, $value, $value2);

				return false;
				break;
			case 'operator':
				$joins[] = 'INNER JOIN ideas_operators io ON io.idea_id = ideas.idea_id AND io.user_id = ' . intval($value);

				return false;
				break;
		}
	}
}
