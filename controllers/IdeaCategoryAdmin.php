<?php

use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Validator\Constraints\NotBlank;
use Cameleon\Services;
use Cameleon\Renderer;

class IdeaCategoryAdmin extends \Cameleon\Admin\AdminController
{
	protected $config = [
		'delete' => false,
		'edit'   => true,
		'new'    => true,
	];

	public function configure_renderer(Renderer $renderer)
	{
		$twig = $renderer->getTwig();
		$twig->addGlobal('NEED_VALIDATION', NEED_VALIDATION);
	}

	public function getModel()
	{
		return new IdeaCategory();
	}

	protected function transform_post_data(&$data)
	{
		$data['allow'] = isset($data['allow']) ? $data['allow'] : false;
		if ($data['user_id'] == 0)
			unset($data['user_id']);
	}

	public function buildForm(FormBuilder $builder)
	{
		$builder
			->add('name', 'text', array(
				'label'       => 'Nom',
				'constraints' => array(
					new NotBlank(),
				),
				'required' => true,
			))
			->add('description', 'text', array(
				'label'       => 'Description',
				'required'    => false,
			))
			->add('start_date', 'date', array(
				'label'       => 'Début',
				'required'    => true,
				'widget'	  => 'single_text',
				'format' 	  => 'yyyy-MM-dd',
			))
			->add('end_date', 'date', array(
				'label'       => 'Fin',
				'required'    => true,
				'widget'	  => 'single_text',
				'format' 	  => 'yyyy-MM-dd',
			))->add('user_id', new \Cameleon\Admin\Form\UsersType(), array(
				'label' => 'Pilote',
				'multiple'   => false,
				'required' => true,
			))->add('allow', 'checkbox', array(
				'label'    => 'Autoriser pour tous',
				'required' => false,
			));
	}

	public function action_index()
	{
		$columns = [
			'name'        => [
				'label' => 'Nom',
			],
			'parent.name' => [
				'label' => 'Catégorie mère',
			],
			'start_date'  => [
				'label' => 'Début',
			],
			'end_date'  => [
				'label' => 'Fin',
			],
			'user.name' => [
				'label' => 'Pilote',
			],
			'allow'       => [
				'label' => 'Autorisé pour tous',
				'type'  => 'allow'
			],
		];

		$filters = [
			[
				'name'  => 'allow',
				'label' => 'Autorisé pour tous',
				'type'  => 'boolean',
			],
		];

		return $this->get_action_index($columns, 10, $filters, [
			'selection_actions' => [
				'allow'    => 'Autoriser',
				'disallow' => 'Interdire',
			]
		]);
	}

	protected function perform_action($action, array $items, $message = null)
	{
		switch ($action) {
			case 'allow':
			case 'disallow':
				$allow = $action === 'allow';
				foreach ($items as $idea_category_id) {
					$this->getModel()->update([
						'allow' => $allow,
					], [
						'idea_category_id' => $idea_category_id
					]);
				}
				$msg = $allow ? 'Les catégories ont bien été autorisées.' : 'Les catégories ont bien été interdites.';
				Services::get_flash_messenger()->add_flash('info', $msg);
				break;
			default:
				throw new \Exception(sprintf('Invalid action "%s"', $action));
				break;
		}
	}
}
