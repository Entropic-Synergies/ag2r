<?php

use Cameleon\Controller\AController;

class HomeController extends AController
{
	use TAG2RController;

	# [GET] /home/abc
	public function get ()
	{
		if ($askcgu = isset($_SESSION['ask_cgu']))
			unset($_SESSION['ask_cgu']);
		return $this->render([
			'user' => $this->me,
			'edito' => $this->get_edito(),
			'askcgu' => $askcgu,
			'login_method' => (isset($_SESSION['login_method']))? $_SESSION['login_method'] : 'none'
		]);
	}

}
