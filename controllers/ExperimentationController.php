<?php

use Cameleon\Controller\AController;
use Cameleon\Exception\AccessDeniedException;
use Cameleon\Exception\NotFoundHttpException;

class ExperimentationController extends AController
{
	use TAG2RController;

	# [GET] /experimentation/all
	public function action_index()
	{
		$params = [];
		$page_size = 5;
		$offset = ($this->get_get('page', 1) - 1) * $page_size;

		$criterias = [];
		if ($filter = $this->get_get('innovation_id')) {
			$params['innovation'] = (new Innovation())->get_absolutely($filter, 'Innovation introuvable');
			$criterias['innovation_id'] = $filter;
		}
		if ($filter = $this->get_get('user_id')) {
			$params['user'] = $user = (new User())->get_absolutely($filter, 'Utilisateur introuvable');
			$user['experimentations'] = (new Experimentation)->get_existing_objects('user', $user['user_id'], true);
			$criterias['experimentation_id'] = (new Experimentation)->get_ids($user['experimentations'], 'experimentation_id');
		}

		$experimentations = (new Experimentation)->no_depth()->get_array($page_size, ['created_at'         => 'DESC',
																					  'experimentation_id' => 'DESC'], $criterias, $offset, $nb_rows, true);

		$params['experimentations_count'] = $nb_rows;
		$params['experimentations'] = $experimentations;
		$params['pages_count'] = ceil($nb_rows / $page_size);

		return $this->render($params);
	}

	# # # # # # # # # # # # # # # # # # # # # # # # #
	# [GET] /experimentation/new
	public function action_new()
	{
		$innovation = $this->validate_innovation($this->get_get('innovation_id'));
		$experimentations = (new Experimentation)->get_array(10, ['created_at' => 'DESC'], [
				'innovation_id' => $innovation['innovation_id']
			]
		);

		return $this->render([
			'innovation'       => $innovation,
			'experimentations' => $experimentations
		]);
	}

	private function validate_innovation($innovation_id)
	{
		if (!$innovation_id || !($innovation = (new Innovation())->get_current($innovation_id))) {
			throw new NotFoundHttpException('Innovation introuvable');
		}

		$acl = \Cameleon\Services::get_acl();
		if (!$acl->is_granted($this->me, 'EDIT', $innovation['key'])) {
			throw new AccessDeniedException('Vous n\'avez pas les droits suffisants');
		}

		return $innovation;
	}

	public function action_create()
	{
		$innovation = $this->validate_innovation($this->get_get('innovation_id'));
		$step = 2;
		if ($this->get_post('from') === 'xp' && ($from_experimentation_id = $this->get_post('from_experimentation'))) {
			$experimentation = (new Experimentation())->duplicate($from_experimentation_id);
			$step = 1;
		} else {
			$data = $_POST;
			if (!isset($data['name']) or !$data['name']) {
				$count = (new Experimentation())->count([
					'innovation_id' => $innovation['innovation_id']
				]);
				$data['name'] = 'Expérimentation n°' . ($count + 1);
			}
			$data['innovation_id'] = $innovation['innovation_id'];
			$data['user_id'] = $this->me['user_id'];
			$experimentation = (new Experimentation())->create($data, false, true, true);
		}

		return $this->redirect('/experimentation/' . $experimentation['experimentation_id'] . '?edit&step=' . $step);
	}

	# # # # # # # # # # # # # # # # # # # # # # # # #
	# [GET] /experimentation/1
	public function action_show()
	{
		$experimentation = (new Experimentation())->get_absolutely($this->get_data('id'));
		$innovation = $experimentation['innovation'];

		return $this->render([
			'innovation'      => $innovation,
			'experimentation' => $experimentation,
		]);
		// note that on this exemple we don't try to get data from the model.
	}

	# # # # # # # # # # # # # # # # # # # # # # # # #
	# [GET] /experimentation/1?edit
	public function action_edit()
	{
		$step = (int)$this->get_get('step', 1);
		$experimentation = $this->get_experimentation();
		$innovation = $this->validate_innovation($experimentation['innovation_id']);

		$acl = \Cameleon\Services::get_acl();
		if (!$acl->is_granted($this->me, 'EDIT', 'innovation/' . $experimentation['innovation_id'])) {
			throw new AccessDeniedException('Vous n\'avez pas les droits suffisants');
		}

		$params = [
			'innovation'      => $innovation,
			'step'            => $this->get_get('step', 1),
			'experimentation' => &$experimentation,
			'tags'            => (new Tag)->get_tags(),
		];

		if ($step === 4) {
			$experimentation['milestones'] = (new Milestone())->get_array('all', ['due_date' => 'DESC'], [
				'experimentation_id' => $experimentation['experimentation_id']
			]);

			$perimeters = (new Perimeter())->get_perimeters($experimentation);
			$params['perimeters'] = $perimeters;
		}

		return $this->render($params);
	}

	protected function get_experimentation()
	{
		$experimentation = (new Experimentation())->get_current($this->get_data('id'));
		if (!$experimentation) {
			throw new NotFoundHttpException('Innovation introuvable');
		}

		return $experimentation;
	}

	# # # # # # # # # # # # # # # # # # # # # # # # #
	# [POST] /experimentation/1?edit
	public function action_update()
	{

		$experimentation = $this->get_experimentation();
		$innovation = $this->validate_innovation($experimentation['innovation_id']);

		$step = (int)$this->get_get('step', 1);
		$data = $_POST;
		switch ($step) {
			case 1:
			case 2:
			case 3:
				if (isset($_POST['tags'])) {
					$tags = explode(',', substr($this->get_post('tags'), 1, -1));
					(new Tag)->set_xp_tags($tags, $experimentation['key']);
				}

				(new Experimentation())->update($data, [
					'experimentation_id' => $experimentation['experimentation_id']
				]);

				if ($step === 3) {
					(new User())->sync('experimentation', 'user', $experimentation['experimentation_id'], $this->get_post('users', []) ? : []);
				}

				return $this->redirect(sprintf('/experimentation/%d?edit&step=%d', $experimentation['experimentation_id'], $step + 1));
				break;
			case 4:
				if (isset($data['perimeters'])) {
					$data['perimeters'] = (new Perimeter)->handle_new_perimeters($data['perimeters']);
				};
				(new Experimentation())->update($data, [
					'experimentation_id' => $experimentation['experimentation_id']
				]);

				\Cameleon\Services::get_flash_messenger()->add_flash('success', 'L\'expérimentation a bien été sauvegardée');

				return $this->redirect('/experimentation/' . $experimentation['experimentation_id']);
				break;
			default:
				throw new NotFoundHttpException();
				break;
		}
	}

	# # # # # # # # # # # # # # # # # # # # # # # # #
	# [GET] /experimentation/1?destroy
	public function action_delete()
	{
		// (new Experimentation($this->get_data('id')))->delete();
		// ... but, you don't want to do that??!!
		return $this->redirect("experimentation/all");
	}

	# # # # # # # # # # # # # # # # # # # # # # # # #
	# [POST] /experimentation/ajax
	public function action_ajax()
	{
		header('Content-Type: application/json');
		echo json_encode((new Experimentation)->get_array('all'));
	}

	# # # # # # # # # # # # # # # # # # # # # # # # #
	# [GET] /experimentation/abc
	public function get()
	{
		return $this->render("experimentation/index.html", [
			"experimentations" => (new Experimentation)->get_array('all')
		]);
	}

	# # # # # # # # # # # # # # # # # # # # # # # # #
	# [POST] /experimentation/abc
	public function post()
	{
		return $this->redirect("/experimentation/abc");
	}

	public function resources()
	{
		$experimentation = (new Experimentation())->get_absolutely($this->get_get('experimentation_id'));

		return $this->render([
			'experimentation' => $experimentation,
			'users' => $experimentation['users'],
		]);
	}
}
