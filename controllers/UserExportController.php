<?php

use Cameleon\Controller\AController;

class UserExportController extends AController
{
    use TAg2rController;

    public function chapter () {}

    # [GET] /userexport/all
    public function action_index()
    {
        $this->app()->chapter();
        return $this->render();
    }
}