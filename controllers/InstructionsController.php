<?php

use Cameleon\Controller\AController;

class InstructionsController extends AController
{
	use TAG2RController;

	# [GET] /instructions
	public function get ()
	{
		return $this->render([
			'user' => $this->me,
			'instructions' => $this->get_instructions(),
		]);
	}

}
