<?php

use Cameleon\Controller\ReportController as ReportControllerBase;

class ReportController extends ReportControllerBase
{
	use TAG2RController;

	public function getObjectModel()
	{
		return new Report();
	}

	protected function get_auth_user()
	{
		return $this->me;
	}
}
