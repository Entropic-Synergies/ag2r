<?php
use Cameleon\Controller\AController;

class PerimeterController extends AController
{
	use TAG2RController;

	public function get_perimeters_from_type()
	{
		$perimeter_type = $this->get_get('type');
		$perimeters = (new Perimeter)->get_perimeter_list($perimeter_type);
		$response = new \Cameleon\Http\Response(json_encode($perimeters));
		$response->setHeader('Content-Type', 'application/json');

		return $response;
	}
}
