<?php

use Cameleon\Renderer;

//use Symfony\Component\HttpFoundation\Response;

class UserExportAdmin extends \Cameleon\Admin\AdminController
{
	public function configure_renderer(Renderer $renderer)
	{
		$twig = $renderer->getTwig();
		$twig->addGlobal('NEED_VALIDATION', NEED_VALIDATION);
	}

	public function getModel()
	{
		return new User();
	}

	//action qui genere un export en CSV
	public function action_new()
	{
		$header     = array('Nom', utf8_decode('Prénom'), 'Mail', 'Direction', 'Site');
		$userExport = (new User())->get_export();

		$headers['Content-Type']        = 'application/force-download';
		$headers['Content-Disposition'] = 'attachment; filename="export_user.csv"';

		return new \Cameleon\Http\StreamedResponse(function () use ($header, $userExport) {
				$handle = fopen('php://output', 'w');
				fputcsv($handle, $header, ';');
				foreach ($userExport as $user) {
					foreach ($user as $k => $u) {
						$user[$k] = utf8_decode($u);
					}
					fputcsv($handle, $user, ';');
				}
				fclose($handle);
			},
			200,
			'OK',
			$headers);
	}
}