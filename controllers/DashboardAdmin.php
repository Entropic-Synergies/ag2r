<?php

use Cameleon\Admin\DashboardAdmin as DashboardAdminBase;
use Cameleon\Renderer;

class DashboardAdmin extends DashboardAdminBase
{

	public function configure_renderer(Renderer $renderer)
	{
		$twig = $renderer->getTwig();
		$twig->addGlobal('NEED_VALIDATION', NEED_VALIDATION);
	}

	public function getModel()
	{
		return null;
	}

	public function get ()
	{
		return $this->render([
			'edito' => json_decode(file_get_contents(CAMELEON_DIR_WEB . 'files/edito.json')),
			'instructions' => json_decode(file_get_contents(CAMELEON_DIR_WEB . 'files/instructions.json')),
		]);
	}

	public function post ()
	{
		if(isset($_GET['update_edito'])){
			$date = $this->get_post('date');
			if(!$date) $date = date('Y-m-d H:i:s', time()+(7*24*3600));
			file_put_contents(CAMELEON_DIR_WEB . 'files/edito.json', json_encode([
				'date' => $date,
				'content' => $this->get_post('content'),
			]));
			move_uploaded_file($_FILES['image']['tmp_name'], CAMELEON_DIR_WEB . 'files/edito.jpg');
		}
		if(isset($_GET['update_instructions'])){
			file_put_contents(CAMELEON_DIR_WEB . 'files/instructions.json', json_encode([
				'content' => $this->get_post('instructionscontent'),
			]));
		}
		return $this->redirect('/admin');
	}
}