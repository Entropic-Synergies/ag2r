<?php

use Cameleon\Controller\AController;
use Cameleon\Security\MaskBuilder;

class GroupController extends AController
{
	use TAG2RController;

	# [GET] /group/all
	public function action_index()
	{
		$acl_manager = \Cameleon\Services::get_acl();
		$db = $this->get_db();
		$limit = 10;
		$offset = ($this->get_get('page', 1) - 1) * $limit;

		$my_groups = (new Group)->no_depth()->get_array('all', ['created_at' => 'DESC'], [
			'user_id' => $this->me['user_id']
		]);

		$user_groups = (new Group)->no_depth()->get_user_groups($this->me['user_id']);

		$joins = $acl_manager->get_sql_inner_join($this->me, 'group', 'group_id', MaskBuilder::MASK_VIEW);

		$groups = (new Group)->no_depth()->get_array($limit, [], [
			$db->query_format('group_id NOT IN (SELECT group_id FROM groups_users WHERE user_id = %s)', $this->me['user_id'])
		], $offset, $nb_rows, true, [], $joins);

		return $this->render([
			'my_groups'    => $my_groups,
			'user_groups'  => $user_groups,
			'groups'       => $groups,
			'groups_count' => $nb_rows,
			'pages_count'  => ceil($nb_rows / $limit),
		]);
	}

	public function action_new()
	{
		return $this->render();
	}

	public function action_edit()
	{
		$group = (new Group)->get_absolutely($this->get_data('id'));

		$acl = \Cameleon\Services::get_acl();
		if (!$acl->is_granted($this->me, 'OWNER', $group['key'])) {
			throw new \Cameleon\Exception\AccessDeniedException('Vous n\'avez pas les droits suffisants');
		}

		return $this->render([
			'group' => $group,
		]);
	}

	public function action_create()
	{
		$users = $this->get_post('users') ? : [];
		$data = $_POST;
		if (isset($data['users'])) {
			unset($data['users']);
			unset($data['users-mg']);
		}

		$data['user_id'] = $this->me['user_id'];
		$group = (new Group)->create($data);
		$users[] = $this->me['user_id'];
		(new User())->sync('group', 'user', $group['group_id'], $users, ['status' => 'accepted']);
		(new Group)->update_counter_field($group['group_id']);

		\Cameleon\Services::get_flash_messenger()->add_flash('success', 'Votre communauté a bien été créée.');

		return $this->redirect('/' . $group['key']);
	}

	public function action_update()
	{
		$group = (new Group())->get_absolutely($this->get_data('id'));
		$acl = \Cameleon\Services::get_acl();
		if (!$acl->is_granted($this->me, 'OWNER', $group['key'])) {
			throw new \Cameleon\Exception\AccessDeniedException('Vous n\'avez pas les droits suffisants');
		}

		$data = $_POST;
		$data['user_id'] = $this->me['user_id'];
		(new Group)->update($data, [
			'group_id' => $group['group_id']
		]);
		(new Group)->update_counter_field($group['group_id']);

		\Cameleon\Services::get_flash_messenger()->add_flash('success', 'Votre communauté a bien mise à jour.');

		return $this->redirect('/' . $group['key']);
	}

	# [GET] /group/1
	public function action_show()
	{
		$group = (new Group)->get_absolutely($this->get_data('id'));
		$group['users'] = (new Group())->get_users($group['group_id']);

		$group['pending_invites'] = (new Group())->get_users_by_status($group['group_id'], 'invited');
		$group['requests'] = (new Group())->get_users_by_status($group['group_id'], 'pending');

		return $this->render([
			'group' => $group,
		]);
	}

	public function invite()
	{
		$users = $this->get_post('users', []);
		$group = (new Group())->get_absolutely($this->get_get('group_id'));

		$acl = \Cameleon\Services::get_acl();
		if (!($group['privacy'] === 'open' || $acl->is_granted($this->me, 'OWNER', $group['key']))) {
			throw new \Cameleon\Exception\AccessDeniedException('Vous n\'avez pas les droits suffisants');
		}

		$db = $this->get_db();
		foreach ($users as $user_id) {
			$user = (new User())->get_absolutely($user_id);
			if ($db->count('groups_users', [
						'group_id' => $group['group_id'],
						'user_id'  => $user['user_id']
					]
				) === 0
			) {
				(new Group)->invite_user($group, $user['user_id'], $this->me['user_id']);
			}
		}
		if (count($users)) {
			\Cameleon\Services::get_flash_messenger()->add_flash('success', 'Les invitations ont bien été envoyées.');
		}

		return $this->redirect('/group/' . $group['group_id'] . '#users');
	}

	public function remove_user()
	{
		list($group, $user) = $this->init_user_action();

		$this->get_db()->delete('groups_users', [
				'group_id' => $group['group_id'],
				'user_id'  => $user['user_id']
			]
		);
		(new Group())->update_counter_field($group['group_id']);
		\Cameleon\Services::get_flash_messenger()->add_flash('notice', 'Le membre a bien été retiré du groupe.');

		return $this->redirect('/group/' . $group['group_id'] . '#users');
	}

	public function accept_user()
	{
		list($group, $user) = $this->init_user_action();

		$this->get_db()->update('groups_users', ['status' => 'accepted'], [
				'group_id' => $group['group_id'],
				'user_id'  => $user['user_id']
			]
		);
		(new Group())->update_counter_field($group['group_id']);
		\Cameleon\Services::get_flash_messenger()->add_flash('success', 'Le membre a bien été accepté dans le groupe.');
		(new Notification())->notify($this->me['user_id'], 'group/' . $group['group_id'], 'accepted', null, null, [['user_id' => $user['user_id']]]);
		return $this->redirect('/group/' . $group['group_id'] . '#users');
	}

	private function init_user_action()
	{
		$user_id = $this->get_get('user_id');
		$group = (new Group())->get_absolutely($this->get_get('group_id'));

		$acl = \Cameleon\Services::get_acl();
		if (!$acl->is_granted($this->me, 'OWNER', $group['key'])) {
			throw new \Cameleon\Exception\AccessDeniedException('Vous n\'avez pas les droits suffisants');
		}

		$user = (new User())->get_absolutely($user_id);

		return [$group, $user];
	}

	public function join()
	{
		$group = (new Group)->get_absolutely($this->get_get('id'));
		$db = $this->get_db();

		$flasher = \Cameleon\Services::get_flash_messenger();
		if (null !== $group['user_status']) {
			if ($group['user_status'] === 'pending') {
				$flasher->add_flash('error', 'Vous avez déjà demandé l\'intégration à ce groupe.');
			} elseif ($group['user_status'] === 'banned') {
				$flasher->add_flash('error', 'Vous êtes banni(e) du groupe.');
			} elseif ($group['user_status'] === 'invited') {
				$db->update('groups_users', ['status' => 'accepted'], [
					'group_id' => $group['group_id'],
					'user_id'  => $this->me['user_id'],
					'status'   => 'invited',
				]);
				$flasher->add_flash('success', 'Vous avez accepté l\'invitation.');
				(new Notification())->notify($this->me['user_id'], 'group/' . $group['group_id'], 'join');
				(new \NotificationSubscription())->follow($this->me['user_id'], 'group/' . $group['group_id'], 'all');
				(new Group())->update_counter_field($group['group_id']);
			}
		} else {
			(new Group)->add_user($group, $this->me['user_id'], $group['privacy'] === 'public' ? 'accepted' : 'pending');
			if ($group['privacy'] === 'public') {
				$flasher->add_flash('success', 'Vous avez rejoint le groupe.');
			} else {
				$flasher->add_flash('success', 'Votre demande a bien été envoyée.');
			}
		}

		return $this->redirect('/group/' . $group['group_id']);
	}

	public function quit()
	{
		$group = (new Group)->get_absolutely($this->get_get('id'));
		$db = $this->get_db();
		$flasher = \Cameleon\Services::get_flash_messenger();
		if (null !== $group['user_status']) {
			if ($group['user_status'] === 'pending') {
				$flasher->add_flash('success', 'Vous avez annulé votre demande.');
			} elseif ($group['user_status'] === 'invited') {
				$flasher->add_flash('success', 'Vous avez refusé l\'invitation.');
			} elseif ($group['user_status'] === 'accepted') {
				$flasher->add_flash('success', 'Vous avez quitté le groupe.');
			} elseif ($group['user_status'] === 'banned') {
				$flasher->add_flash('error', 'Vous êtes banni(e) du groupe.');
				return $this->redirect('/group/all');
			}

			$db->delete('groups_users', [
				'group_id' => $group['group_id'],
				'user_id'  => $this->me['user_id'],
			]);
			(new NotificationSubscription())->unfollow($this->me['user_id'], 'group/' . $group['group_id'], 'all');
			(new Notification())->notify($this->me['user_id'], 'group/' . $group['group_id'], 'quit');
			(new Group())->update_counter_field($group['group_id']);
		} else {
			$flasher->add_flash('error', 'Vous n\'êtes pas dans ce groupe.');
		}

		return $this->redirect('/group/all');
	}
}
