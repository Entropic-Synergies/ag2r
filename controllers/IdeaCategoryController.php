<?php

use Cameleon\Controller\AController;

class IdeaCategoryController extends AController
{
	use TAg2rController;

	public function chapter()
	{
	}

	# [GET] /ideacategory/all
	public function action_index()
	{
		$this->app()->chapter();

		$limit = 10;
		$offset = ($this->get_get('page', 1) - 1) * $limit;

		$ideacategorys = (new IdeaCategory())->get_array($limit, [], [], $offset, $nb_rows, true);

		return $this->render([
			'ideacategorys' => $ideacategorys,
			'nb_rows' => $nb_rows,
			'pages_count' => ceil($nb_rows / $limit),
		]);
	}

	# [GET] /ideacategory/new
	public function action_new()
	{
		$this->app()->chapter();
		return $this->render();
	}

	# [POST] /ideacategory/new
	public function action_create()
	{
		$this->app()->chapter();
		(new IdeaCategory)->create([
			'name' => $this->get_post('name'),
			'size' => $this->get_post('size'),
			'is_cute' => 1,
		]);
		return $this->redirect('/ideacategory/all');
	}

	# [GET] /ideacategory/1
	public function action_show()
	{
		$this->app()->chapter();
		$ideacategory_id = $this->get_data('id');
		return $this->render([
			'ideacategory' => (new IdeaCategory)->get_absolutely($ideacategory_id),
		]);
	}

	# [GET] /ideacategory/1?edit
	public function action_edit()
	{
		$this->app()->chapter();
		$b = new IdeaCategory($this->get_data('id'));
		return $this->render($b->get_current());
	}

	# [POST] /ideacategory/1?edit
	public function action_update()
	{
		$this->app()->chapter();
		$b = (new IdeaCategory())->get_absolutely($this->get_data('id'));
		$data = $_POST;
		$b->update($data, [
			'ideacategory_id' => $this->get_data('id')
		]);
		return $this->redirect('/ideacategory/' . $this->get_data('id') . '?edit');
	}

	# [GET] /ideacategory/1?destroy
	public function action_destroy()
	{
		$this->app()->chapter();
//		(new IdeaCategory($this->get_data('id')))->delete([
//			'ideacategory_id' => $this->get_data('id')
//		]);
		return $this->redirect('/ideacategory/all');
	}
}
