<?php

use Cameleon\Controller\FileController as FileControllerBase;

class FileController extends FileControllerBase
{
	use TAG2RController;

	public function upload_image()
	{
		$key = $this->get_get('key');
		if (!$key) {
			throw new \InvalidArgumentException('Missing param "key"');
		}

		list($object_table, $object_id) = explode('/', $key);
		$object_id_name = $object_table . '_id';
		$item = $this->get_object($object_table, $object_id, $object);

		if (!(isset($_FILES['file']) || isset($_FILES['files']['error'][0]))) {
			throw new \InvalidArgumentException('Missing file "file" or "files"');
		}
		if (isset($_FILES['files'])) {
			foreach ($_FILES['files'] as $k => $value) {
				$uploaded_file[$k] = $value[0];
			}
		} else {
			$uploaded_file = $_FILES['file'];
		}
		$status_code = 200;
		$status_text = 'OK';
		try {
			$file = (new File())->upload($uploaded_file, $key, [], [
				'image/jpeg',
				'image/gif',
				'image/png',
			]);

			$file['url'] = \Cameleon\Controller\AssetController::get_cached_image_url('crop', $file['url'], 150);
			$data = [
				'files' => [$file]
			];

			if (isset($object->fields['file_picture_id'])) {
				$object->update(['file_picture_id' => $file['file_id']], [$object_id_name => $item[$object_id_name]]);
			}
		} catch (\Cameleon\Exception\HttpException $e) {
			$status_code = $e->get_status_code();
			$status_text = $e->get_status_text();
			$data = [
				'error' => $e->getMessage()
			];
		}

		return new \Cameleon\Http\Response(json_encode($data), $status_code, $status_text);
	}

	public function delete_image()
	{
		$key = $this->get_get('key');
		if (!$key) {
			throw new \InvalidArgumentException('Missing param "key"');
		}
		list($object_table, $object_id) = explode('/', $key);
		$object_id_name = $object_table . '_id';
		$item = $this->get_object($object_table, $object_id, $object);

		(new File)->delete_files_by_key($key);
		if (isset($object->fields['file_picture_id'])) {
			$object->update(['file_picture_id' => null], [$object_id_name => $item[$object_id_name]]);
		}

		$item = \Cameleon\Model\AModel::get_object_from_key($key)->get_current();

		return new \Cameleon\Http\Response(json_encode([
			'picture' => $item['picture']
		]));
	}

	private function get_object($object_table, $object_id, &$object = null)
	{
		$class_name = ucfirst($object_table);
		$object = new $class_name;
		if (!$object instanceof \Cameleon\Model\AModel) {
			throw new InvalidArgumentException(sprintf('Invalid object_table "%s"', $object_table));
		}

		return $object->get_absolutely($object_id);
	}
}
