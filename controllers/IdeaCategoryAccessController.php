<?php

use Cameleon\Controller\AController;

class IdeaCategoryAccessController extends AController
{
	use TAg2rController;

	public function chapter()
	{
	}

	# [GET] /ideacategoryaccess/all
	public function action_index()
	{
		$this->app()->chapter();

		$limit = 10;
		$offset = ($this->get_get('page', 1) - 1) * $limit;

		$ideacategoryaccesss = (new IdeaCategoryAccess())->get_array($limit, [], [], $offset, $nb_rows, true);

		return $this->render([
			'ideacategoryaccesss' => $ideacategoryaccesss,
			'nb_rows' => $nb_rows,
			'pages_count' => ceil($nb_rows / $limit),
		]);
	}

	# [GET] /ideacategoryaccess/new
	public function action_new()
	{
		$this->app()->chapter();
		return $this->render();
	}

	# [POST] /ideacategoryaccess/new
	public function action_create()
	{
		$this->app()->chapter();
		(new IdeaCategoryAccess)->create([
			'name' => $this->get_post('name'),
			'size' => $this->get_post('size'),
			'is_cute' => 1,
		]);
		return $this->redirect('/ideacategoryaccess/all');
	}

	# [GET] /ideacategoryaccess/1
	public function action_show()
	{
		$this->app()->chapter();
		$ideacategoryaccess_id = $this->get_data('id');
		return $this->render([
			'ideacategoryaccess' => (new IdeaCategoryAccess)->get_absolutely($ideacategoryaccess_id),
		]);
	}

	# [GET] /ideacategoryaccess/1?edit
	public function action_edit()
	{
		$this->app()->chapter();
		$b = new IdeaCategoryAccess($this->get_data('id'));
		return $this->render($b->get_current());
	}

	# [POST] /ideacategoryaccess/1?edit
	public function action_update()
	{
		$this->app()->chapter();
		$b = (new IdeaCategoryAccess())->get_absolutely($this->get_data('id'));
		$data = $_POST;
		$b->update($data, [
			'ideacategoryaccess_id' => $this->get_data('id')
		]);
		return $this->redirect('/ideacategoryaccess/' . $this->get_data('id') . '?edit');
	}

	# [GET] /ideacategoryaccess/1?destroy
	public function action_destroy()
	{
		$this->app()->chapter();
		return $this->redirect('/ideacategoryaccess/all');
	}
}
