<?php

use Cameleon\Controller\AController;
use Cameleon\Exception\AccessDeniedException;
use Cameleon\Exception\NotFoundHttpException;
use Cameleon\Services;

class InnovationController extends AController
{
	use TAG2RController;

	# [GET] /innovation/all
	public function action_index()
	{
		$limit = 10;

		$offset = ($this->get_get('page', 1) - 1) * $limit;
		if ($offset < 0) {
			$offset = 0;
		}
		$innovations = (new Innovation)->in_depth(false)->get_array($limit, ['created_at'    => 'DESC',
																			 'innovation_id' => 'DESC'], '', $offset, $nb_rows, true);

		return $this->render([
			'innovations'   => $innovations,
			'n_innovations' => $nb_rows,
			'pages_count'   => ceil($nb_rows / $limit)
		]);
	}

	# [GET] /innovation/new
	public function action_new()
	{
		$idea_id = $this->get_get('from_idea_id');
		if (!$idea_id) {
			throw new \InvalidArgumentException('Missing from_idea_id param');
		}
		$acl = Services::get_acl();
		if (!$acl->is_granted($this->me, 'OPERATOR', 'idea/' . $idea_id)) {
			throw new AccessDeniedException('Vous n\'avez pas les droits suffisants');
		}
		$innovations = (new Innovation)->get_array(1, null, ['idea_id' => $idea_id]);
		if (count($innovations) === 0) {
			$innovation = (new Innovation)->import_idea($idea_id, $this->me);
			Services::get_flash_messenger()->add_flash('success', 'L\'innovation a bien été créée à partir de l\'idée');
		} else {
			$innovation = current($innovations);
		}

		return $this->redirect('/innovation/' . $innovation['innovation_id'] . '?edit');
	}

	# [GET] /innovation/1
	public function action_show()
	{
		$innovation = (new Innovation())->get_absolutely($this->get_data('id'));

		return $this->render([
			'innovation' => $innovation,
		]);
	}

	# [GET] /innovation/1?edit
	public function action_edit()
	{
		$innovation = (new Innovation())->get_absolutely($this->get_data('id'));

		$acl = Services::get_acl();
		if (!$acl->is_granted($this->me, 'EDIT', $innovation['key'])) {
			throw new AccessDeniedException('Vous n\'avez pas les droits suffisants');
		}
		$perimeters = (new Perimeter())->get_perimeters($innovation);

		return $this->render([
			'perimeters' => $perimeters,
			'innovation' => $innovation,
			'edit_type'  => $this->get_get('edit', 'classic'),
			'tags'       => (new Tag)->get_tags(),
		]);
	}

	# [POST] /innovation/1?edit
	public function action_update()
	{
		$innovation = (new Innovation())->get_absolutely($this->get_data('id'));

		$acl = Services::get_acl();
		if (!$acl->is_granted($this->me, 'EDIT', $innovation['key'])) {
			throw new AccessDeniedException('Vous n\'avez pas les droits suffisants');
		}

		$data = $_POST;
		if(isset($data['perimeters'])){
			$data['perimeters'] = (new Perimeter)->handle_new_perimeters($data['perimeters']);
		};

		(new Innovation())->update($data, ['innovation_id' => $innovation['innovation_id']]);

		(new User())->sync('innovation', 'user', $innovation['innovation_id'], $this->get_post('users') ? : []);

		$tags = explode(',', substr($this->get_post('tags'), 1, -1));
		(new Tag)->set_xp_tags($tags, $innovation['key']);

		Services::get_flash_messenger()->add_flash('success', 'L\'innovation a bien été enregistrée');

		return $this->redirect('/innovation/' . $innovation['innovation_id']);
	}

	# [POST] /innovation/ajax
	public function action_ajax()
	{
		if ($this->get_get('method') == 'change_budget') {
			(new Innovation($this->get_post('innovation_id', 0)))->update([
				'budget' => $this->get_post('budget', 0),
			]);
		}
		header('Content-Type: application/json');
	}

	public function resources()
	{
		$innovation = (new Innovation())->get_absolutely($this->get_get('innovation_id'));
		$limit = 5;
		$offset = ($this->get_get('page', 1) - 1) * $limit;
		if ($offset < 0) {
			$offset = 0;
		}
		$experimentations = (new Experimentation)->get_array($limit,
			[
				'created_at' => 'DESC'
			], [
				'innovation_id' => $innovation['innovation_id']
			], $offset, $nb_rows, true);

		return $this->render([
			'innovation'       => $innovation,
			'experimentations' => $experimentations,
			'n_innovations'    => $nb_rows,
			'pages_count'      => ceil($nb_rows / $limit)
		]);
	}
}
