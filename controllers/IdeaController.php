<?php

use Cameleon\Controller\AController;
use Cameleon\Exception\AccessDeniedException;
use Cameleon\Services;

class IdeaController extends AController
{
    use TAG2RController;

    # [GET] /idea/all
    public function action_index()
    {
        $page_size = 10;
        $offset    = ($this->get_get('page', 1) - 1) * $page_size;

        $db    = $this->get_db();
        $where = [];
        if (($filters = $this->get_get('filters')) && is_array($filters)) {
            $mapping = ['category' => 'c.idea_category_id',
                        'status'   => 'ideas.status',
            ];
            foreach ($filters as $key => $value) {
                if ($value) {
                    $where[$mapping[$key]] = $value;
                }
            }
        }
        $joins = $db->query_format('INNER JOIN idea_categorys c ON ideas.idea_category_id = c.idea_category_id
		LEFT JOIN idea_categorys pc ON pc.idea_category_id = c.parent_id
		LEFT JOIN ideacategoryaccesss icau ON (icau.idea_category_id = c.idea_category_id OR icau.idea_category_id = c.parent_id) AND icau.user_id = %1$s
		LEFT JOIN ideacategoryaccesss ica ON ica.idea_category_id = c.idea_category_id AND ica.user_id IS NULL', $this->me['user_id']);

        if ($this->get_get('onlyme')) {
            $joins .= ' INNER JOIN ideas_operators io ON io.idea_id = ideas.idea_id';
            $where[] = 'io.user_id = ' . intval($this->me['user_id']);
        }

        $where[] = "ideas.status NOT IN ('draft', 'deleted', 'validation')";
        $where[] = $db->query_format('(c.allow = 1 OR pc.allow = 1 OR icau.user_id = %1$s
		                                OR (ica.jobposition_id = %2$s AND ica.location = %3$s)
		                                OR (ica.location IS NULL AND ica.jobposition_id = %2$s)
		                                OR (ica.jobposition_id IS NULL AND ica.location = %3$s))',
            $this->me['user_id'],
            $this->me['jobposition_id'],
            $this->me['location']
        );

        $ideas = (new Idea)->in_depth(false)->get_array($page_size, ['created_at' => 'DESC'], $where, $offset, $nb_rows, true, ['idea_id'], $joins);

        $idea_statuses     = Idea::$statuses;
        $new_idea_statuses = [];

        foreach ($idea_statuses as $k => $v) {
            if ($k != 'experiment') {
                $new_idea_statuses[$k] = $v;
            }
        }

        return $this->render([
            'ideas'           => $ideas,
            'n_ideas'         => $nb_rows,
            'pages_count'     => ceil($nb_rows / $page_size),
            'idea_statuses'   => $new_idea_statuses,
            'idea_categories' => IdeaCategory::get_categories_tree(),
        ]);
    }

    # [GET] /idea/pending
    public function pending()
    {
        $page_size = 10;
        $offset    = ($this->get_get('page', 1) - 1) * $page_size;

        $db    = $this->get_db();
        $where = [];
        if (($filters = $this->get_get('filters')) && is_array($filters)) {
            $mapping = ['category' => 'idea_categorys.idea_category_id',
                        'status'   => 'ideas.status',
            ];
            foreach ($filters as $key => $value) {
                if ($value) {
                    $where[$mapping[$key]] = $value;
                }
            }
        }
        $joins = '
		INNER JOIN idea_categorys ON ideas.idea_category_id = idea_categorys.idea_category_id
		LEFT JOIN idea_categorys children ON children.parent_id = idea_categorys.idea_category_id
		LEFT JOIN ideacategoryaccesss ica ON (ica.idea_category_id = idea_categorys.idea_category_id
			OR ica.idea_category_id = children.idea_category_id
			OR ica.idea_category_id = idea_categorys.parent_id)';

        $joins .= ' INNER JOIN ideas_operators io ON io.idea_id = ideas.idea_id';
        $where[] = 'io.user_id = ' . intval($this->me['user_id']);

        $where[] = "ideas.status IN ('waiting', 'study', 'estimate')";
        $where[] = $db->query_format('(idea_categorys.allow = 1 OR children.allow = 1 OR ica.user_id = %1$s
		                                OR (ica.jobposition_id IS NOT NULL AND ica.jobposition_id = %2$s AND ica.location = %3$s)
		                                OR (ica.jobposition_id IS NOT NULL AND ica.jobposition_id = %2$s AND ica.location = "")
		                                OR (ica.location IS NOT NULL AND ica.location = %3$s AND ica.jobposition_id = %2$s)
		                                OR (ica.location IS NOT NULL AND ica.location = %3$s AND ica.jobposition_id = ""))',
            $this->me['user_id'],
            $this->me['jobposition_id'],
            $this->me['location']
        );

        $ideas = (new Idea)->in_depth(false)->get_array($page_size, ['created_at' => 'DESC'], $where, $offset, $nb_rows, true, ['idea_id'], $joins);

        return $this->render([
            'ideas'           => $ideas,
            'n_ideas'         => $nb_rows,
            'pages_count'     => ceil($nb_rows / $page_size),
            'idea_statuses'   => Idea::$statuses,
            'idea_categories' => IdeaCategory::get_categories_tree(),
        ]);
    }

    # [GET] /idea/drafts
    public function drafts()
    {
        $page_size = 10;
        $offset    = ($this->get_get('page', 1) - 1) * $page_size;

        $db = $this->get_db();

        $joins = '
		INNER JOIN idea_categorys ON ideas.idea_category_id = idea_categorys.idea_category_id
		LEFT JOIN idea_categorys children ON children.parent_id = idea_categorys.idea_category_id
		LEFT JOIN ideacategoryaccesss ica ON (ica.idea_category_id = idea_categorys.idea_category_id
			OR ica.idea_category_id = children.idea_category_id
			OR ica.idea_category_id = idea_categorys.parent_id)';

        if ($this->get_get('onlyme')) {
            $joins .= ' INNER JOIN ideas_operators io ON io.idea_id = ideas.idea_id';
            $where[] = 'io.user_id = ' . intval($this->me['user_id']);
        }

        $where[] = "ideas.status = 'draft'";
        $where[] = "ideas.user_id = " . $this->me['user_id'];

        $ideas = (new Idea)->in_depth(false)->get_array($page_size, ['created_at' => 'DESC'], $where, $offset, $nb_rows, true, ['idea_id'], $joins);

        return $this->render([
            'ideas'           => $ideas,
            'n_ideas'         => $nb_rows,
            'pages_count'     => ceil($nb_rows / $page_size),
            'idea_statuses'   => Idea::$statuses,
            'idea_categories' => IdeaCategory::get_categories_tree(),
        ]);
    }

    # [POST] /idea/apply_action
    public function apply_action()
    {
        if ($_POST) {
            $action      = $this->get_post('action');
            $idea_id     = $this->get_post('idea_id');
            $message     = $this->get_post('message');
            $items       = [$idea_id];
            $create_feed = false;

            switch ($action) {
                case 'experiment':
                case 'accept':
                    foreach ($items as $idea_id) {
                        (new Innovation())->import_idea($idea_id, $this->me);
                    }
                    Services::get_flash_messenger()->add_flash('success', 'Votre idée a bien été retenue.');
                    $this->notify($idea_id, $this->me, 'experiment');
                    break;
                case 'refused':
                case 'deny':
                    foreach ($items as $idea_id) {
                        (new Idea())->deny($idea_id, $this->me, $message);
                    }
                    Services::get_flash_messenger()->add_flash('info', 'Votre idée a bien été refusée.');
                    $this->notify($idea_id, $this->me, 'refused');
                    $create_feed = true;
                    break;
                case 'waiting':
                    foreach ($items as $idea_id) {
                        (new Idea())->waiting($idea_id, $this->me);
                    }
                    Services::get_flash_messenger()->add_flash('info', 'Votre idée a bien été passée en statut : ' . (Idea::$statuses[$action]));
                    $this->notify($idea_id, $this->me, 'waiting');
                    $create_feed = true;
                    break;
                case 'duplicate':
                    foreach ($items as $idea_id) {
                        (new Idea())->duplicate($idea_id, $this->me);
                    }
                    Services::get_flash_messenger()->add_flash('info', 'Votre idée a bien été passée en statut : ' . (Idea::$statuses[$action]));
                    $this->notify($idea_id, $this->me, 'duplicate');
                    $create_feed = true;
                    break;
                case 'estimate':
                    foreach ($items as $idea_id) {
                        (new Idea())->estimate($idea_id, $this->me);
                    }
                    Services::get_flash_messenger()->add_flash('info', 'Votre idée a bien été passée en statut : ' . (Idea::$statuses[$action]));
                    $this->notify($idea_id, $this->me, 'estimate');
                    $create_feed = true;
                    break;
                case 'confirmed':
                    foreach ($items as $idea_id) {
                        (new Idea())->confirm($idea_id, $this->me);
                    }
                    Services::get_flash_messenger()->add_flash('info', 'Votre idée a bien été passée en statut : ' . (Idea::$statuses[$action]));
                    $this->notify($idea_id, $this->me, 'confirmed');
                    $create_feed = true;
                    break;
                default:
                    throw new \Exception(sprintf('Invalid action "%s"', $action));
                    break;
            }

            if ($create_feed == true) {
                $user = $this->me;
                foreach ($items as $idea_id) {
                    $idea = (new Idea)->get_current($idea_id);

                    (new Feed)->create([
                        'user_id'      => $user['user_id'],
                        'object_table' => 'idea',
                        'object_id'    => $idea_id,
                        'action_type'  => 'status_update',
                        'content'      => $message,
                        'name'         => $idea['status'],
                    ]);

                    $comment = $user['first_name'] . ' ' . $user['last_name'] . ' vient de passer l\'idée en statut ';
                    $comment = $comment . (Idea::$statuses[$idea['status']]);
                    $comment = $comment . "\n";
                    $comment = $comment . $message;

                    (new ObjectComment)->add_comment(new Idea, $idea['idea_id'], $user, $comment);
                }
            }
        }

        return new \Cameleon\Http\Response('OK');
    }

    public function notify($idea_id, $user, $status)
    {
        $idea = (new Idea())->get_absolutely($idea_id);
        if ($user['user_id'] != $idea['user_id']) {
            (new Notification())->notify($user['user_id'], 'idea/' . $idea_id, $status, null, null, [['user_id' => $idea['user_id']]]);
        }
    }

    # [GET] /idea/validations
    public function validations()
    {
        $page_size = 10;
        $offset    = ($this->get_get('page', 1) - 1) * $page_size;

        $db = $this->get_db();

        $joins = '
		INNER JOIN idea_categorys ON ideas.idea_category_id = idea_categorys.idea_category_id
		LEFT JOIN idea_categorys children ON children.parent_id = idea_categorys.idea_category_id
		LEFT JOIN ideacategoryaccesss ica ON (ica.idea_category_id = idea_categorys.idea_category_id
			OR ica.idea_category_id = children.idea_category_id
			OR ica.idea_category_id = idea_categorys.parent_id)';

        if ($this->get_get('onlyme')) {
            $joins .= ' INNER JOIN ideas_operators io ON io.idea_id = ideas.idea_id';
            $where[] = 'io.user_id = ' . intval($this->me['user_id']);
        }

        $where[] = "ideas.status = 'validation'";
        $where[] = "ideas.user_id = " . $this->me['user_id'];

        $ideas = (new Idea)->in_depth(false)->get_array($page_size, ['created_at' => 'DESC'], $where, $offset, $nb_rows, true, ['idea_id'], $joins);

        return $this->render([
            'ideas'           => $ideas,
            'n_ideas'         => $nb_rows,
            'pages_count'     => ceil($nb_rows / $page_size),
            'idea_statuses'   => Idea::$statuses,
            'idea_categories' => IdeaCategory::get_categories_tree(),
        ]);
    }

    # [GET] /idea/1
    public function action_show()
    {
        $idea         = (new Idea())->get_absolutely($this->get_data('id'));
        $nb_documents = (new Document)->count(['`object_table`="idea"',
            '`object_id`="' . $idea['idea_id'] . '"',
            '`uploaded`=1']);

        $db = $this->get_db();

        $where_prev[] = "ideas.idea_id < " . $idea['idea_id'];
        $where_next[] = "ideas.idea_id > " . $idea['idea_id'];

        $prev_idea = (new Idea)->get_one(['idea_id' => 'DESC'], $where_prev);
        $next_idea = (new Idea)->get_one(['idea_id' => 'ASC'], $where_next);

        return $this->render([
            'idea'         => $idea,
            'prev_idea'    => $prev_idea,
            'next_idea'    => $next_idea,
            'nb_documents' => $nb_documents,
            'social'       => true
        ]);
    }

    # [GET] /idea/new
    public function action_new()
    {
        return $this->render([
            'user'       => (new User)->get_signed_in(),
            'categories' => $this->get_all_categories_active()
        ]);
    }

    public function get_categories(array $user = null, $filterIdea = false)
    {
        $model = new IdeaCategory();
        $db    = $this->get_db();
        if ($filterIdea) {
            $where = [];
        } else {
            $where = ['idea_categorys.start_date <= NOW()', 'idea_categorys.end_date >= NOW()'];
        }
        $user       = $this->me;
        $joins      = 'LEFT JOIN idea_categorys children ON children.parent_id = idea_categorys.idea_category_id
		LEFT JOIN ideacategoryaccesss ica ON (ica.idea_category_id = idea_categorys.idea_category_id
			OR ica.idea_category_id = children.idea_category_id
			OR ica.idea_category_id = idea_categorys.parent_id)';
        $where[]    = $db->query_format('(idea_categorys.allow = 1 OR children.allow = 1 OR ica.user_id = %1$s
		                                OR (ica.jobposition_id IS NOT NULL AND ica.jobposition_id = %2$s AND ica.location = %3$s)
		                                OR (ica.jobposition_id IS NOT NULL AND ica.jobposition_id = %2$s AND ica.location = "")
		                                OR (ica.location IS NOT NULL AND ica.location = %3$s AND ica.jobposition_id = %2$s)
		                                OR (ica.location IS NOT NULL AND ica.location = %3$s AND ica.jobposition_id = ""))',
            $user['user_id'],
            $user['jobposition_id'],
            $user['location']);
        $categories = $model->get_array('all', ['parent_id' => 'ASC',
                                                'name'      => 'ASC'], $where, 0, $nb_rows, false, ['idea_category_id'], $joins);
        foreach ($categories as $key => $category) {
            if ($category['parent_id']) {
                $parent = &$categories[$model->table_name . '/' . $category['parent_id']];

                if (!isset($parent['children'])) {
                    $parent['children'] = [];
                }
                $parent['children'][] = $category;
                unset($categories[$key]);
            }
        }

        return $categories;
    }

    public function get_all_categories_active()
    {
        $model      = new IdeaCategory();
        $db         = $this->get_db();
        $where      = ['idea_categorys.start_date <= NOW()', 'idea_categorys.end_date >= NOW()'];
        $joins      = '';
        $categories = $model->get_array('all', [], $where, 0, $nb_rows, false, [], $joins);

        return $categories;
    }

    public function rate()
    {
        $idea = (new Idea)->get_absolutely($this->get_post('idea_id'));

        $rating = (int)$this->get_post('rating');
        if ($rating < 1 || $rating > 5) {
            throw new InvalidArgumentException('Invalid rating value');
        }

        try {
            (new Idea)->rate($idea['idea_id'], $this->me['user_id'], $rating);
        } catch (\InvalidArgumentException $e) {
        }

        // Reload the idea
        $idea = (new Idea)->get_absolutely($this->get_post('idea_id'));

        return $this->render([
            'idea' => $idea,
        ], 200, 'OK', 'partial/idea/action_rating.html.twig');
    }

    # [POST] /idea/new
    public function action_create()
    {
        $this->app();
        $data = $_POST;
        unset($data['documents_name']);
        $data['user_id'] = $this->me['user_id'];

        $doc_names = [];
        $tmp_files = [];
        if (isset($_POST['documents_name'])) {
            $doc_names = $_POST['documents_name'];
        }
        if (isset($_FILES['documents_file'])) {
            $tmp_files = $_FILES['documents_file'];
        }

        $doc_files = [];
        foreach ($tmp_files as $key1 => $value1) {
            foreach ($value1 as $key2 => $value2) {
                $doc_files[$key2][$key1] = $value2;
            }
        }

        $idea = (new Idea)->create($data);

        $key = $idea['key'];

        for ($i = 0; $i < count($doc_files); $i++) {
            $tmp_data            = [];
            $tmp_data['name']    = $doc_names[$i];
            $tmp_data['user_id'] = $data['user_id'];

            if (isset($doc_files[$i]) and !empty($doc_files[$i]) and $doc_files[$i]['size']) {
                $file = (new Document())->upload($doc_files[$i], $key, $tmp_data);
            }
        }

        if ($idea['status'] == 'draft') {
            Services::get_flash_messenger()->add_flash('success', 'Le brouillon a bien été créé.');
        } else {
            if (NEED_VALIDATION) {
                Services::get_flash_messenger()->add_flash('success', 'L\'idée est en attente de validation.');
            } else {
                Services::get_flash_messenger()->add_flash('success', 'L\'idée a bien été enregistrée.');
            }
        }

        if (!NEED_VALIDATION) {
            (new Idea)->validate($idea['idea_id'], []);
        }

        return $this->redirect("/idea/all");
    }

    # [GET] /idea/1?edit
    public function action_edit()
    {
        $idea = (new Idea())->get_absolutely($this->get_data('id'));

        $acl = Services::get_acl();
        if (!$acl->is_granted($this->me, 'EDIT', $idea['key'])) {
            throw new AccessDeniedException('Vous n\'avez pas les droits suffisants');
        }

        return $this->render([
            'user'       => (new User)->get_signed_in(),
            'idea'       => $idea,
            'categories' => $this->get_categories($this->me),
        ]);
    }

    # [POST] /idea/1?edit
    public function action_update()
    {
        $idea = (new Idea())->get_absolutely($this->get_data('id'));

        $acl = Services::get_acl();
        if (!$acl->is_granted($this->me, 'EDIT', $idea['key'])) {
            throw new AccessDeniedException('Vous n\'avez pas les droits suffisants');
        }
        $data = $_POST;

        (new Idea())->update($data, ['idea_id' => $idea['idea_id']]);
        if ($data["idea_category_id"] != $idea["idea_category_id"]) {
            $operators = (new Idea)->get_operators($idea['idea_id']);
            $users_id  = [];
            foreach ($operators as $operator) {
                if ((int)$operator['user_id'] !== (int)$this->me['user_id']) {
                    $users_id[] = $operator['user_id'];
                }
            }
            if ($idea["idea_category_id"]) {
                $cat = (new IdeaCategory)->get_one('idea_category_id', 'idea_category_id = ' . $idea["idea_category_id"]);
            }
            if (isset($cat) and !empty($cat) and $cat["user_id"]) {
                $users_id[] = $cat['user_id'];
            }
            (new Idea)->update_operators($idea['idea_id'], $users_id);
        }
        if ($data["status"] != $idea["status"]) {
            $this->notify($idea["idea_id"], $this->me, $data["status"]);
        }

        if ($data['status'] == 'validation') {
            Services::get_flash_messenger()->add_flash('success', 'L\'idée est en attente de validation.');
        } else {
            Services::get_flash_messenger()->add_flash('success', 'L\'idée a bien été enregistrée');
        }

        return $this->redirect('/idea/' . $idea['idea_id']);
    }

    public function action_destroy()
    {
        $idea = (new Idea())->get_absolutely($this->get_data('id'));

        $acl = \Cameleon\Services::get_acl();
        if (!$acl->is_granted($this->get_me(), 'DELETE', $idea['key'])) {
            throw new \Cameleon\Exception\AccessDeniedException();
        }

        (new Idea())->delete([
            'idea_id' => $idea['idea_id']
        ]);

        \Cameleon\Services::get_flash_messenger()->add_flash('notice', 'L\'idée a bien été supprimée.');

        return $this->redirect('/idea/all');
    }

    public function assign()
    {
        $idea = (new Idea)->get_absolutely($this->get_get('id'));
        $acl  = Services::get_acl();
        if (!$acl->is_granted($this->me, 'OPERATOR', $idea['key'])) {
            throw new AccessDeniedException('Vous n\'êtes pas pilote de l\'idée.');
        }

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $users = $this->get_post('users');
            if (!$users) {
                Services::get_flash_messenger()->add_flash('error', 'L\'idée n\'a pas été réaffectée.');

                return $this->redirect('/idea/' . $idea['idea_id']);
            }
            $user      = (new User)->get_absolutely(reset($users));
            $operators = (new Idea)->get_operators($idea['idea_id']);
            $users_id  = [];
            foreach ($operators as $operator) {
                if ((int)$operator['user_id'] !== (int)$this->me['user_id']) {
                    $users_id[] = $operator['user_id'];
                }
            }
            $users_id[] = $user['user_id'];
            (new Idea)->update_operators($idea['idea_id'], $users_id);
            Services::get_flash_messenger()->add_flash('success', 'L\'idée a bien été réaffectée.');

            return $this->redirect('/idea/' . $idea['idea_id']);
        }

        return $this->render([
            'idea' => $idea
        ]);
    }

    public function deny()
    {
        $acl = \Cameleon\Services::get_acl();
        if (!$acl->is_granted($this->me, 'CREATE', 'innovation')) {
            throw new AccessDeniedException();
        }
        if (!($idea_id = $this->get_post('idea_id'))) {
            throw new InvalidArgumentException('Missing param idea_id');
        }

        (new Idea)->deny($idea_id, $this->me, $this->get_post('message'));

        \Cameleon\Services::get_flash_messenger()->add_flash('notice', 'L\'idée a bien été refusée.');

        return new \Cameleon\Http\Response();
    }

    public function search()
    {
        $search      = $this->get_get('search');
        $search_type = $this->get_get('search_type', 'idea');

        if (!$search) {
            throw new InvalidArgumentException('Missing param search');
        }

        if ($search_type == 'idea') {
            $page_size = 10;
            $offset    = ($this->get_get('page', 1) - 1) * $page_size;

            $db    = $this->get_db();
            $where = [];
            if (($filters = $this->get_get('filters')) && is_array($filters)) {
                $mapping = ['category' => 'idea_categorys.idea_category_id',
                            'status'   => 'ideas.status',
                ];
                foreach ($filters as $key => $value) {
                    if ($value) {
                        $where[$mapping[$key]] = $value;
                    }
                }
            }
            $joins = '
				INNER JOIN idea_categorys ON ideas.idea_category_id = idea_categorys.idea_category_id
				LEFT JOIN idea_categorys children ON children.parent_id = idea_categorys.idea_category_id
				LEFT JOIN ideacategoryaccesss ica ON (ica.idea_category_id = idea_categorys.idea_category_id
				OR ica.idea_category_id = children.idea_category_id
				OR ica.idea_category_id = idea_categorys.parent_id)';

            if ($this->get_get('onlyme')) {
                $joins .= ' INNER JOIN ideas_operators io ON io.idea_id = ideas.idea_id';
                $where[] = 'io.user_id = ' . intval($this->me['user_id']);
            }

            $where[] = "ideas.status != 'draft'";
            $where[] = "ideas.status != 'deleted'";
            $where[] = "ideas.status != 'validation'";

            $where[] = "((ideas.name LIKE '%" . $db->real_escape_string($search) . "%') OR (ideas.description LIKE '%" . $db->real_escape_string($search) . "%'))";

            $where[] = $db->query_format('(idea_categorys.allow = 1 OR children.allow = 1 OR ica.user_id = %1$s
		                                OR (ica.jobposition_id IS NOT NULL AND ica.jobposition_id = %2$s AND ica.location = %3$s)
		                                OR (ica.jobposition_id IS NOT NULL AND ica.jobposition_id = %2$s AND ica.location = "")
		                                OR (ica.location IS NOT NULL AND ica.location = %3$s AND ica.jobposition_id = %2$s)
		                                OR (ica.location IS NOT NULL AND ica.location = %3$s AND ica.jobposition_id = ""))',
                $this->me['user_id'],
                $this->me['jobposition_id'],
                $this->me['location']
            );

            $ideas = (new Idea)->in_depth(false)->get_array($page_size, ['created_at' => 'DESC'], $where, $offset, $nb_rows, true, ['idea_id'], $joins);

            return $this->render([
                'search'          => $search,
                'search_type'     => $search_type,
                'ideas'           => $ideas,
                'n_ideas'         => $nb_rows,
                'pages_count'     => ceil($nb_rows / $page_size),
                'idea_statuses'   => Idea::$statuses,
                'idea_categories' => IdeaCategory::get_categories_tree(),
            ]);
        }
        if ($search_type == 'innovation') {
            $limit = 10;

            $offset = ($this->get_get('page', 1) - 1) * $limit;
            if ($offset < 0) {
                $offset = 0;
            }
            $db          = $this->get_db();
            $where       = [];
            $where[]     = "(innovations.name LIKE '%" . $db->real_escape_string($search) . "%') OR (innovations.description LIKE '%" . $db->real_escape_string($search) . "%')";
            $innovations = (new Innovation)->in_depth(false)->get_array($limit, ['created_at'    => 'DESC',
                                                                                 'innovation_id' => 'DESC'], $where, $offset, $nb_rows, true);

            return $this->render([
                'search'        => $search,
                'search_type'   => $search_type,
                'innovations'   => $innovations,
                'n_innovations' => $nb_rows,
                'pages_count'   => ceil($nb_rows / $limit)
            ]);
        }
    }
}
