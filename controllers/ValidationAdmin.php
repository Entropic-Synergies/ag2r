<?php

use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Validator\Constraints\NotBlank;
use Cameleon\Services;
use Cameleon\Renderer;

class ValidationAdmin extends \Cameleon\Admin\AdminController
{
	protected $config = [
		'delete' => false,
		'edit'   => false,
		'new'    => false,
	];

	public function configure_renderer(Renderer $renderer)
	{
		$twig = $renderer->getTwig();
		$twig->addGlobal('NEED_VALIDATION', NEED_VALIDATION);
	}

	public function getModel()
	{
		return new Idea();
	}

	protected function transform_post_data(&$data)
	{
		$data['closed'] = isset($data['closed']) ? $data['closed'] : false;
	}

	public function buildForm(FormBuilder $builder)
	{
		$builder
			->add('name', 'text', array(
				'label'       => 'Nom',
				'constraints' => array(
					new NotBlank(),
				),
			))
			->add('description', 'textarea', array(
				'required' => false,
			))
			->add('idea_category_id', 'choice', array(
				'label'    => 'Thématique',
				'choices'  => IdeaCategory::get_categories_tree(),
				'required' => false,
			))->add('operators', new \Cameleon\Admin\Form\UsersType(), array(
				'label' => 'Pilotes',
			))->add('closed', 'checkbox', array(
				'label'    => 'Close',
				'required' => false,
			));
	}

	public function action_destroy()
	{
		$model = $this->getModel();

		$idea = $model->get_absolutely($this->get_data('id'));
		if ($idea['status'] === 'experiment') {
			Services::get_flash_messenger()->add_flash('error', 'Vous ne pouvez pas supprimer une idée qui a été expérimentée.');
			return $this->redirect('/admin/' . $this->get_chapter() . '/all');
		}

		return parent::action_destroy();
	}

	protected function perform_action($action, array $items, $message = null)
	{
		switch ($action) {
			case 'accept':
				foreach ($items as $idea_id) {
					(new Idea())->validate($idea_id, $this->me);
				}
				Services::get_flash_messenger()->add_flash('success', 'L\'idée a bien été validée.');
				$this->notify($idea_id, $this->me, 'validation');
				break;
			case 'refused':
			case 'deny':
				foreach ($items as $idea_id) {
					(new Idea())->deleteidea($idea_id, $this->me, $message);
				}
				Services::get_flash_messenger()->add_flash('info', 'L\'idée a bien été refusée.');
				$this->notify($idea_id, $this->me, 'refused');
				break;
			default:
				throw new \Exception(sprintf('Invalid action "%s"', $action));
				break;
		}
	}

	public function notify($idea_id, $user, $status) {
		$idea = (new Idea())->get_absolutely($idea_id);
		if ($user['user_id'] != $idea['user_id'])
			(new Notification())->notify($user['user_id'], 'idea/' . $idea_id, $status, null, null, [['user_id' => $idea['user_id']]]);
	}

	public function action_index()
	{
		$status = [
			'waiting'   => 'A l\'étude',
			'confirmed' => 'Mise en oeuvre',
			'refused'   => 'Refusée',
			'draft'   => 'Brouillon',
			'duplicate' => 'Déjà existant',
			'study' => 'Idée à étudier',
			'validation' => 'Attente validation',
			'estimate' => 'En cours d\'estimation',
		];
		$columns = [
			'name'               => [
				'label' => 'Nom',
			],
			'description'        => [
				'label'    => 'Description',
			],
			'idea_category.name' => [
				'label' => 'Thématique',
			],
			'user.name'          => [
				'label' => 'Auteur',
			],
			'validation'         => [
				'label' => 'Actions'
			]
		];

		$filters = [];
		$limit = 10;

		$params = [];
		
		$page = $this->get_get('page', 1);
		$model = $this->getModel();
		$conditions = $this->get_filters_conditions($model, $filters, $joins, $having);
		$conditions[] = '`ideas`.`status` = "validation"';
		$offset = ($page - 1) * $limit;
		$id_name = $model->table_name . '_id';
		$data = $model->get_array($limit, [], $conditions, $offset, $nb_rows, true, [$model->table_name . 's.' . $id_name], $joins, $having);

		return $this->render(array_merge([
			'data'        => $data,
			'nb_rows'     => $nb_rows,
			'pages_count' => ceil($nb_rows / $limit),
			'columns'     => $columns,
			'filters'     => $filters,
			'selection_actions' => [
				'accept'   => 'Valider',
				'deny' => 'Refuser'
			]
		], $params));
	}
}
