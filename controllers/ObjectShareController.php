<?php

use Cameleon\Controller\AController;

class ObjectShareController extends AController
{
	use TAG2RController;

	public function share()
	{
		$model = \Cameleon\Model\AModel::get_object_from_key($this->get_post('oid'));
		$xxx_id = $model->table_name . '_id';
		$object = $model->get_absolutely($model->$xxx_id);
		$users = $this->get_post('users');
		foreach ($users as $user_id => $checked) {
			if ($checked) {
				(new ObjectShare())->share($model->table_name, $object[$xxx_id], $user_id, $this->get_post('comment'));
			}
		}
		\Cameleon\Services::get_flash_messenger()->add_flash('success', 'L\'élément a bien été partagé.');

		return $this->redirect(sprintf('/%s', $object['key']));
	}
}
