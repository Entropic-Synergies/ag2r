<?php

class FooterController extends \Cameleon\Controller\AController
{
	use TAG2RController;

	# [POST] /footer/contact
	public function contact()
	{
		$name    = $this->get_post('name');
		$message = $this->get_post('message');
		if ($name and $message) {
			(new Mail)->send(['email' => WEBMASTER_EMAIL], 'contact.html.twig', ['name'    => $name,
																				 'message' => $message]);
			\Cameleon\Services::get_flash_messenger()->add_flash('success', 'Votre message a bien été envoyé.');
		} else {
			\Cameleon\Services::get_flash_messenger()->add_flash('error', 'Une erreur s\'est produite, votre message n\'a pas été envoyé.');
		}
		$url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/';

		return $this->redirect($url);
	}

	# [POST] /footer/cgu
	public function cgu()
	{
		$accepted = $this->get_post('accepted');
		if ($accepted) {
			(new User)->update(["cgu" => true], ['user_id' => $this->me["user_id"]]);
			\Cameleon\Services::get_flash_messenger()->add_flash('success', 'Vous avez bien validé les Conditions Générales d\'Utilisation.');
		}
		$url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/';

		return $this->redirect($url);
	}
}
