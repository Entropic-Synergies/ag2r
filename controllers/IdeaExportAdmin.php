<?php

use Cameleon\Renderer;
//use Symfony\Component\HttpFoundation\Response;

class IdeaExportAdmin extends \Cameleon\Admin\AdminController
{
    public function configure_renderer(Renderer $renderer)
    {
        $twig = $renderer->getTwig();
        $twig->addGlobal('NEED_VALIDATION', NEED_VALIDATION);
    }

    public function getModel()
    {
        return new Idea();
    }

	public function action_index()
	{
		$filters = [
			[
				'name'  => 'ideas.idea_id',
				'label' => 'URL',
				'type'  => 'boolean',
                'checked' => 'checked',
			], [
                'name'    => 'idea_categorys.name',
                'label'   => 'Catégorie',
                'type'    => 'boolean',
                'checked' => 'checked',
            ], [
				'name'    => 'ideas.name',
				'label'   => 'Nom de l\'idée',
				'type'    => 'boolean',
                'checked' => 'checked',
			],
            [
                'name'    => 'ideas.description',
                'label'   => 'Contenu',
                'type'    => 'boolean',
                'checked' => 'checked',
            ],
            [
                'name'    => 'users.last_name',
                'label'   => 'Nom auteur',
                'type'    => 'boolean',
                'checked' => 'checked',
            ],
            [
                'name'    => 'users.first_name',
                'label'   => 'Prénom auteur',
                'type'    => 'boolean',
                'checked' => 'checked',
            ],
            [
                'name'    => 'users.email',
                'label'   => 'Mail auteur',
                'type'    => 'boolean',
                'checked' => 'checked',
            ],
            [
                'name'    => 'ideas.created_at',
                'label'   => 'Date de dépôt',
                'type'    => 'boolean',
                'checked' => 'checked',
            ],
            [
                'name'    => 'ideas.status',
                'label'   => 'Statut de l\'idée',
                'type'    => 'boolean',
                'checked' => 'checked',
            ],
            [
                'name'    => 'operator',
                'label'   => 'Identité(s) pilote(s)',
                'type'    => 'boolean',
                'checked' => 'checked',
            ],
		];

        return $this->get_action_index([], 10, $filters, $params = ['export' => true]);
	}


    //action qui genere un export en CSV
    public function action_new()
    {
        $header = [];
        $operator = [];
        $url = '';
        if (isset($_POST['filters'])) {
            array_push($header, 'ideas.idea_id');
            if(isset($_POST['filters']['ideas.idea_id'])) {
                $url = CAMELEON_BASEURL.'/idea/';
            }
            foreach($_POST['filters'] as $filter => $value) {
                if($filter != 'ideas.idea_id' && $filter != 'operator') {
                    array_push($header, $filter);
                }
            }
            $nb_max_operator = (new Idea()) ->get_max_count_operator();
            for($i=1; $i<=$nb_max_operator; $i++){
                array_push($header, 'Pilote '.$i);
            }
            if(isset($_POST['filters']['operator']) && count($_POST['filters']) != 1) {
                unset($_POST['filters']['operator']);
                $operator = (new Idea())->get_operator();
            }
            elseif(isset($_POST['filters']['operator']) && count($_POST['filters']) == 1) {
                $operator = (new Idea())->get_operator();
            }
            $ideaExport = (new Idea())->get_export($_POST['filters']);
        }else{
            $url = CAMELEON_BASEURL.'/idea/';
            $header = ['ideas.idea_id', 'idea_categorys.name', 'ideas.name', 'ideas.description', 'users.first_name', 'users.last_name', 'users.email', 'ideas.created_at', 'ideas.status'];
            $nb_max_operator = (new Idea())->get_max_count_operator();
            for($i=1; $i<=$nb_max_operator; $i++){
                array_push($header, 'Pilote '.$i);
            }
            $ideaExport = (new Idea())->get_export();
            $operator = (new Idea())->get_operator();
        }

        $headers['Content-Type'] = 'application/force-download';
        $headers['Content-Disposition'] = 'attachment; filename="export.csv"';

        return new \Cameleon\Http\StreamedResponse(function () use ($header, $ideaExport, $operator, $url) {
                $handle = fopen('php://output', 'w');
                fputcsv($handle, $header, ';');
                foreach ($ideaExport as $idea) {

                    foreach ($idea as $k => $v) {
                        $idea[$k] = utf8_decode($v);
                    }
                    if(isset($operator) && !empty($operator)) {
                        $i = 0;
                        if(isset($operator[$idea[0]])) {
                            foreach($operator[$idea[0]] as $ope) {
                                $info_operator = utf8_decode($ope['Prénom']).' '.utf8_decode($ope['Nom']).' => '.$ope['Email'];
                                array_push($idea, $info_operator);
                                $i++;
                            }
                        }
                    }
                    if(!empty($url)) {
                        $idea[0] = $url.$idea[0];
                    }
                    fputcsv($handle, $idea, ';');
                }
                fclose($handle);
            },
            200,
            'OK',
            $headers);
    }
}