<?php

use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Cameleon\Renderer;

class UserAdmin extends \Cameleon\Admin\AdminController
{
	protected $config = [
		'delete' => false,
		'edit'   => true,
		'new'    => true,
	];

	public function getModel()
	{
		return new User();
	}

	public function configure_renderer(Renderer $renderer)
	{
		$twig = $renderer->getTwig();
		$twig->addGlobal('NEED_VALIDATION', NEED_VALIDATION);
	}

	public function buildForm(FormBuilder $builder)
	{
		$id= $this->get_data('id');
		$builder->add('first_name', 'text', array(
			'label'       => 'Prénom',
			'constraints' => array(
				new NotBlank(),
			),
		))
			->add('last_name', 'text', array(
				'label'       => 'Nom',
				'constraints' => array(
					new NotBlank(),
				),
			))->add('email', 'email', array(
				'constraints' => array(
					new NotBlank(),
					new \Symfony\Component\Validator\Constraints\Email(),
					new \Symfony\Component\Validator\Constraints\Callback(function ($object, \Symfony\Component\Validator\ExecutionContextInterface $context) use ($id) {
							$users = (new User)->get_array(1, null, ['user_id != "' . $id .'"', 'email' => $object]);
							if (count($users) === 1) {
								$context->addViolationAt('email', 'Cet utilisateur existe déjà.');
							}
					}),
				),
			))->add('jobposition_id', 'choice', array(
				'label'       => 'Direction',
				'choices'     => $this->get_job_positions(),
				'required'    => false,
				'empty_value' => 'Choisir ...',
				'constraints' => [
				]
			))->add('location', 'choice', array(
				'label'       => 'Site',
				'choices'     => $this->get_locations(),
				'required'    => false,
				'empty_value' => 'Choisir ...',
				'constraints' => [
				]
			))
			->add('roles', 'choice', array(
				'choices'  => array(
					'USER'       => 'Utilisateur',
					'SUPERADMIN' => 'Administrateur'
				),
				'multiple' => true,
				'expanded' => true
			));
	}

	public function get_job_positions()
	{
		$job_positions = (new JobPosition())->get_array('all', ['name' => 'ASC']);
		$items         = [];
		foreach ($job_positions as $job_position) {
			$items[$job_position['jobposition_id']] = $job_position['name'];
		}

		return $items;
	}

	public function get_locations()
	{
		$sites = User::get_sites_list();

		return array_combine($sites, $sites);
	}

	public function action_index()
	{
		$location = [
			'AGDE'                   => 'AGDE',
			'AGEN'                   => 'AGEN',
			'AIX EN PROVENCE'        => 'AIX EN PROVENCE',
			'AJACCIO'                => 'AJACCIO',
			'ALBI'                   => 'ALBI',
			'AMIENS'                 => 'AMIENS',
			'ANGERS'                 => 'ANGERS',
			'ANNECY'                 => 'ANNECY',
			'AURILLAC'               => 'AURILLAC',
			'AVIGNON'                => 'AVIGNON',
			'BALMA'                  => 'BALMA',
			'BASTIA'                 => 'BASTIA',
			'BAYONNE'                => 'BAYONNE',
			'BERGERAC'               => 'BERGERAC',
			'BESANCON'               => 'BESANCON',
			'BEZIERS'                => 'BEZIERS',
			'BLOIS'                  => 'BLOIS',
			'BORDEAUX'               => 'BORDEAUX',
			'BOURG EN BRESSE'        => 'BOURG EN BRESSE',
			'BOURGES'                => 'BOURGES',
			'BOVES'                  => 'BOVES',
			'BREST'                  => 'BREST',
			'BRIVE LA GAILLARDE'     => 'BRIVE LA GAILLARDE',
			'CAEN'                   => 'CAEN',
			'CAHORS'                 => 'CAHORS',
			'CALAIS'                 => 'CALAIS',
			'CASTRES'                => 'CASTRES',
			'CAYENNE'                => 'CAYENNE',
			'CHALON SUR SAONE'       => 'CHALON SUR SAONE',
			'CHAMBERY'               => 'CHAMBERY',
			'CHARTRES'               => 'CHARTRES',
			'CHATILLON'              => 'CHATILLON',
			'CLERMONT FERRAND'       => 'CLERMONT FERRAND',
			'COLMAR'                 => 'COLMAR',
			'COMPIEGNE'              => 'COMPIEGNE',
			'CRETEIL'                => 'CRETEIL',
			'DAX'                    => 'DAX',
			'DIGNE'                  => 'DIGNE',
			'DIJON'                  => 'DIJON',
			'EPINAL'                 => 'EPINAL',
			'FLERS EN ESCREBIEUX'    => 'FLERS EN ESCREBIEUX',
			'FOIX'                   => 'FOIX',
			'FORT DE FRANCE'         => 'FORT DE FRANCE',
			'GAP'                    => 'GAP',
			'GRENOBLE'               => 'GRENOBLE',
			'GUERET'                 => 'GUERET',
			'HEROUVILLE SAINT CLAIR' => 'HEROUVILLE SAINT CLAIR',
			'ISLE'                   => 'ISLE',
			'LA MOTTE SERVOLEX'      => 'LA MOTTE SERVOLEX',
			'LA ROCHE SUR YON'       => 'LA ROCHE SUR YON',
			'LA ROCHELLE'            => 'LA ROCHELLE',
			'LA VALETTE DU VAR'      => 'LA VALETTE DU VAR',
			'LANGUEUX'               => 'LANGUEUX',
			'LE HAVRE'               => 'LE HAVRE',
			'LILLE CEDEX'            => 'LILLE CEDEX',
			'LILLE'                  => 'LILLE',
			'LIMOGES'                => 'LIMOGES',
			'LOGNES'                 => 'LOGNES',
			'LORIENT'                => 'LORIENT',
			'LYON'                   => 'LYON',
			'MALAKOFF'               => 'MALAKOFF',
			'MARSEILLE'              => 'MARSEILLE',
			'MAXEVILLE'              => 'MAXEVILLE',
			'MAZAMET'                => 'MAZAMET',
			'MELUN'                  => 'MELUN',
			'METZ'                   => 'METZ',
			'MONACO'                 => 'MONACO',
			'MONS-EN-BAROEUL'        => 'MONS-EN-BAROEUL',
			'MONT DE MARSAN'         => 'MONT DE MARSAN',
			'MONTAUBAN'              => 'MONTAUBAN',
			'MONTELIMAR'             => 'MONTELIMAR',
			'MONTPELLIER'            => 'MONTPELLIER',
			'MOULINS'                => 'MOULINS',
			'MULHOUSE'               => 'MULHOUSE',
			'NANCY'                  => 'NANCY',
			'NANTES'                 => 'NANTES',
			'NARBONNE'               => 'NARBONNE',
			'NEVERS'                 => 'NEVERS',
			'NICE'                   => 'NICE',
			'NIMES'                  => 'NIMES',
			'NIORT'                  => 'NIORT',
			'NOUMEA'                 => 'NOUMEA',
			'ORLEANS'                => 'ORLEANS',
			'PAPEETE'                => 'PAPEETE',
			'PARIS'                  => 'PARIS',
			'PAU'                    => 'PAU',
			'PERIGUEUX'              => 'PERIGUEUX',
			'PERPIGNAN'              => 'PERPIGNAN',
			'PETIT QUEVILLY'         => 'PETIT QUEVILLY',
			'POINTE A PITRE'         => 'POINTE A PITRE',
			'POITIERS'               => 'POITIERS',
			'QUIMPER'                => 'QUIMPER',
			'REIMS'                  => 'REIMS',
			'RENNES'                 => 'RENNES',
			'ROUEN'                  => 'ROUEN',
			'SAINT BENOIT'           => 'SAINT BENOIT',
			'SAINT BRIEUC'           => 'SAINT BRIEUC',
			'SAINT ETIENNE'          => 'SAINT ETIENNE',
			'SAINT MARIE LA REUNION' => 'SAINT MARIE LA REUNION',
			'SAINT NAZAIRE'          => 'SAINT NAZAIRE',
			'SAINT PIERRE'           => 'SAINT PIERRE',
			'SAINT QUENTIN'          => 'SAINT QUENTIN',
			'SAINT SATURNIN'         => 'SAINT SATURNIN',
			'SAINT VIT'              => 'SAINT VIT',
			'SETE'                   => 'SETE',
			'SEYNOD'                 => 'SEYNOD',
			'SAINT HERBLAIN'         => 'SAINT HERBLAIN',
			'STRASBOURG'             => 'STRASBOURG',
			'TAISSY'                 => 'TAISSY',
			'TARBES'                 => 'TARBES',
			'TOULON'                 => 'TOULON',
			'TOULOUSE'               => 'TOULOUSE',
			'TOURS'                  => 'TOURS',
			'TROYES'                 => 'TROYES',
			'VANNES'                 => 'VANNES',
			'VOISINS LE BRETONNEUX'  => 'VOISINS LE BRETONNEUX',
		];

		$columns = [
			'name'     => [
				'label' => 'Nom'
			], 'roles' => [
				'label' => 'Rôles',
				'type'  => 'array',
			],
		];

		$filters = [
			[
				'name'  => 'last_name',
				'label' => 'Nom',
				'type'  => 'query',
			], [
				'name'  => 'first_name',
				'label' => 'Prénom',
				'type'  => 'query',
			], [
				'name'    => 'jobposition_id',
				'label'   => 'Position',
				'type'    => 'choices',
				'choices' => JobPosition::get_jobPosition_tree(),
			], [
				'name'    => 'location',
				'label'   => 'Site',
				'type'    => 'choices',
				'choices' => $location,
			],
		];

		return $this->get_action_index($columns, 10, $filters);
	}
}