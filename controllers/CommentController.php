<?php

use Cameleon\Controller\CommentController as CommentControllerBase;
use Cameleon\Exception\NotFoundHttpException;

class CommentController extends CommentControllerBase
{
	use TAG2RController;

	protected function get_auth_user()
	{
		return $this->me;
	}

	public function action_destroy()
	{
		$comment_id = $this->get_data('id');

		$comment = (new ObjectComment())->get_current($comment_id);
		if (!$comment) {
			throw new NotFoundHttpException('Comment not found');
		}

		$acl = \Cameleon\Services::get_acl();
		if (!$acl->is_granted($this->get_auth_user(), 'DELETE', $comment['key'])) {
			throw new \Cameleon\Exception\AccessDeniedException();
		}

		parent::action_destroy();
		$object_name = ucfirst($comment['object_table']);
		$comments = (new ObjectComment())->get_all(new $object_name, $comment['object_id']);

		return $this->render([
			'comments'    => $comments,
			'hide_layout' => true
		]);
	}

	public function action_create()
	{
		parent::action_create();

		$model = $this->get_model();
		$comments = (new ObjectComment())->get_all($model['model'], $model['id']);

		return $this->render([
			'comments'    => $comments,
			'hide_layout' => true
		]);
	}

	# [GET] /ObjectComment/1?edit
	public function action_edit()
	{
		$comment = (new ObjectComment)->get_absolutely($this->get_data('id'));

		return new \Cameleon\Http\Response($comment['content']);
	}

	# [POST] /ObjectComment/1?edit
	public function action_update()
	{
		$b = new ObjectComment;
		$comment = $b->get_absolutely($this->get_data('id'));
		$b->update([
			'content' => $_POST['comment']
		], [
			'object_comment_id' => $comment['object_comment_id']
		]);

		return new \Cameleon\Http\Response(nl2br($_POST['comment']));
	}
}