<?php

use Cameleon\Controller\LikeController as LikeControllerBase;

class LikeController extends LikeControllerBase
{
	use TAG2RController;

	protected function get_auth_user()
	{
		return $this->me;
	}
}