<?php

use Cameleon\Controller\AController;

class NotificationController extends AController
{
	use TAG2RController;

	public function chapter()
	{
	}

	# [GET] /notification/all
	public function action_index()
	{
		$this->app()->chapter();

		$limit = 30;
		$offset = ($this->get_get('page', 1) - 1) * $limit;

		$notifications = (new Notification())->get_array($limit, ['created_at' => 'DESC'], [
			'user_id' => $this->me['user_id']
		], $offset, $nb_rows, true);

		(new Notification())->set_notified($this->me['user_id']);

		return $this->render([
			'notifications' => $notifications,
			'nb_rows'       => $nb_rows,
			'pages_count'   => ceil($nb_rows / $limit),
		]);
	}

	# [GET] /notification/new
	public function action_new()
	{
		$this->app()->chapter();

		return $this->render();
	}

	# [POST] /notification/new
	public function action_create()
	{
		$this->app()->chapter();
		(new Notification)->create([
			'name'    => $this->get_post('name'),
			'size'    => $this->get_post('size'),
			'is_cute' => 1,
		]);

		return $this->redirect('/notification/all');
	}

	# [GET] /notification/1
	public function action_show()
	{
		$this->app()->chapter();
		$notification_id = $this->get_data('id');

		return $this->render([
			'notification' => (new Notification)->get_absolutely($notification_id),
		]);
	}

	# [GET] /notification/1?edit
	public function action_edit()
	{
		$this->app()->chapter();
		$b = new Notification($this->get_data('id'));

		return $this->render($b->get_current());
	}

	# [POST] /notification/1?edit
	public function action_update()
	{
		$this->app()->chapter();
		$b = (new Notification())->get_absolutely($this->get_data('id'));
		$data = $_POST;
		$b->update($data, [
			'notification_id' => $this->get_data('id')
		]);

		return $this->redirect('/notification/' . $this->get_data('id') . '?edit');
	}

	# [GET] /notification/1?destroy
	public function action_destroy()
	{
		$this->app()->chapter();

//		(new Notification($this->get_data('id')))->delete([
//			'notification_id' => $this->get_data('id')
//		]);
		return $this->redirect('/notification/all');
	}
}
