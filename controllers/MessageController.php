<?php

use Cameleon\Controller\AController;
use Cameleon\Exception\NotFoundHttpException;

class MessageController extends AController
{
	use TAG2RController;

	# [GET] /message/all
	public function action_index()
	{
		$page_size = 10;
		$offset = ($this->get_get('page', 1) - 1) * $page_size;

		$db = \Cameleon\Services::get_db();
		$sql = 'SELECT SQL_CALC_FOUND_ROWS *
				FROM messages m
				INNER JOIN (
					SELECT message_id FROM messages WHERE user_id = %user_id% ORDER BY created_at DESC
				) m2 ON m2.message_id = m.message_id
				WHERE m.user_id = %user_id%
				GROUP BY m.thread_id
				ORDER BY m.created_at DESC
				LIMIT %offset%, %limit%';

		$sql = str_replace(['%user_id%', '%offset%', '%limit%'], [$db->real_escape_string($this->me['user_id']),
			$offset, $page_size], $sql);

		$messages = (new Message)->readall($sql, $nb_rows);
		foreach ($messages as &$message) {
			// Hack sender to display the user other
			$message['from_user'] = $message['from_user_id'] == $this->me['user_id'] ? $message['to_user'] : $message['from_user'];
		}

		return $this->render([
			'messages'       => $messages,
			'messages_count' => $nb_rows,
			'pages_count'    => ceil($nb_rows / $page_size),
		]);
	}

	# [GET] /message/show
	public function action_show()
	{
		$messages = (new Message)->get_array(100, ['created_at' => 'ASC'],
			[
				'thread_id' => $this->get_data('id'),
				'user_id'   => $this->me['user_id'],
			]
		);
		$thread = reset($messages);
		if (!$thread) {
			return $this->redirect('/message/all');
		}
		(new Message)->update(['status' => 'read'], [
			'thread_id' => $this->get_data('id'),
			'user_id'   => $this->me['user_id'],
		]);

		return $this->render([
			'user'     => $this->get_other_user($thread),
			'thread'   => $thread,
			'messages' => $messages
		]);
	}

	private function get_other_user(array $message)
	{
		return $message['to_user_id'] != $this->me['user_id'] ? $message['to_user'] : $message['from_user'];
	}

	# [GET] /message/new
	public function action_new()
	{
		$params = [];
		if ($to = $this->get_get('to')) {
			$user = (new User)->get_absolutely($to);
			$params['to'] = (new User)->get_magicsuggest_mapped_users([$user]);
		}

		return $this->render($params);
	}

	# [POST] /message/new
	public function action_create()
	{
		$data = $_POST;

		if (isset($data['thread_id'])) {
			$data['to_user_id'] = $data['thread_id'];
		} elseif (isset($data['to'])) {
			$to = json_decode($data['to']);
			if (!isset($to[0])) {
				\Cameleon\Services::get_flash_messenger()->add_flash('error', 'Vous devez choisir le destinataire');

				return $this->redirect('/message/new');
			}
			$data['to'] = $to[0];
			$data['to_user_id'] = $data['to'];
			unset($data['to']);
		} else {
			throw new \Exception('Missing "thread_id" or "to" param');
		}

		$data['from_user_id'] = $this->me['user_id'];

		$thread = (new Message)->send_message($data);

		return $this->redirect('/message/' . $thread['thread_id']);
	}

	# [GET] /message/1?destroy
	public function action_destroy()
	{
		$id = $this->get_data('id');
		if ($this->get_get('all_thread')) {
			(new Message())->delete(['thread_id' => $id, 'user_id' => $this->me['user_id']]);
			\Cameleon\Services::get_flash_messenger()->add_flash('notice', 'La conversation a bien été supprimée.');

			return $this->redirect('/message/all');
		} else {
			$message = (new Message())->get_absolutely($id);
			(new Message())->delete(['message_id' => $message['message_id'], 'user_id' => $this->me['user_id']]);
			\Cameleon\Services::get_flash_messenger()->add_flash('notice', 'Le message a bien été supprimé.');

			return $this->redirect('/message/' . $message['thread_id']);
		}
	}
}
