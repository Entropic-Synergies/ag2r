<?php
namespace Cameleon\Controller;

class TSocialController
{

	use \Cameleon\Controller, CURRENTAPPController;

	public function chapter () {}

	# [GET] /TSocial/all
	public function action_index ()
	{
		$this->app()->chapter();
		$this->render([
			'TSocials' => (new TSocial)->get_array('all'),
		]);
	}

	# [GET] /TSocial/new
	public function action_new ()
	{
		$this->app()->chapter();
		$this->render();
	}

	# [POST] /TSocial/new
	public function action_create ()
	{
		$this->app()->chapter();
		(new TSocial)->create([
			'name' => $this->get_post('name'),
			'size' => $this->get_post('size'),
			'is_cute' => 1,
		]);
		$this->redirect('/TSocial/all');
	}

	# [GET] /TSocial/1
	public function action_show ()
	{
		$this->app()->chapter();
		$TSocial_id = $this->get_data('id');
		$this->render([
			'TSocial' => (new TSocial)->get_current($TSocial_id),
		]);
	}

	# [GET] /TSocial/1?edit
	public function action_edit ()
	{
		$this->app()->chapter();
		$b = new TSocial($this->get_data('id'));
		$this->render($b->get_current());
	}

	# [POST] /TSocial/1?edit
	public function action_update ()
	{
		$this->app()->chapter();
		$b = new TSocial($this->get_data('id'));
		$b->update($_POST);
		$this->redirect('/TSocial/' . $b->TSocial_id . '?edit');
	}

	# [GET] /TSocial/1?destroy
	public function action_destroy ()
	{
		$this->app()->chapter();
		// (new TSocial($this->get_data('id')))->delete();
		$this->redirect('/TSocial/all');
	}

	# [POST] /TSocial/ajax
	public function action_ajax ()
	{
		$this->app()->chapter();
		header('Content-Type: application/json');
		echo json_encode((new TSocial)->get_array('all'));
	}

	# [GET] /TSocial/abc
	public function get ()
	{
		$this->app()->chapter();

	}

	# [POST] /TSocial/abc
	public function post ()
	{
		$this->app()->chapter();
		// some actions
		$this->redirect('/TSocial/abc');
	}

}
