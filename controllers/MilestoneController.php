<?php

class MilestoneController extends \Cameleon\Controller\AController
{
	use TAG2RController;

	# [GET] /milestone/all
	public function action_index()
	{
		$params = [];
		$experimentation_id = $this->get_get('experimentation_id');
		if (!$experimentation_id) {
			return $this->experimentation_index();
		}
		$params['experimentation'] = (new Experimentation())->in_depth(true)->get_absolutely($experimentation_id, 'Expérimentation introuvable');

		$page_size = 5;

		$offset = ($this->get_get('page', 1) - 1) * $page_size;
		if ($offset < 0) {
			$offset = 0;
		}
		$milestones = (new Milestone())->get_array($page_size, ['updated_at' => 'DESC'], [
			'experimentation_id' => $experimentation_id
		], $offset, $nb_rows, true);

		$params['milestones'] = $milestones;
		$params['milestones_count'] = $nb_rows;
		$params['pages_count'] = ceil($nb_rows / $page_size);

		return $this->render($params, 200, 'OK', 'layout/b.html.twig');
	}

	# [GET] /milestone/1
	public function action_show()
	{
		$milestone = (new Milestone())->get_absolutely($this->get_data('id'));
		if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) and strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest') {
			$jsonData = json_encode(
				array(
					'milestone_id' => $milestone['milestone_id'],
					'description' => $milestone['description'],
					'name' => $milestone['name'],
					'due_date' => $milestone['due_date']->format('Y-m-d')
				)
			);
			$response = new \Cameleon\Http\Response($jsonData);
			$response->setHeader('Content-Type', 'application/json');
			return $response;
		}
		return $this->render($milestone, 200, 'OK', 'layout/b.html.twig');
	}

	# [POST] /milestone/ajax
	public function action_ajax()
	{
		$data = [];
		foreach (['description', 'name', 'due_date'] as $field) {
			if (isset($_POST[$field])) {
				$data[$field] = $this->get_post($field);
			}
		}
		if ($milestone_id = $this->get_post('milestone_id')) {
			(new Milestone())->update($data, [
				'milestone_id' => $milestone_id
			]);
			$milestone = [];
		} else {
			$data['user_id'] = $this->me['user_id'];
			$data['experimentation_id'] = $this->get_get('experimentation_id');
			$milestone = (new Milestone())->create($data);
		}

		$response = new \Cameleon\Http\Response(json_encode(array('milestone' => $milestone)));
		$response->setHeader('Content-Type', 'application/json');
		return $response;
	}

	public function experimentation_index()
	{
		$innovation_id = $this->get_get('innovation_id');

		$params = [];

		$criterias = [];
		$params['innovation'] = (new Innovation())->get_absolutely($innovation_id, 'Innovation introuvable');
		$criterias['innovation_id'] = $innovation_id;

		$experimentations = (new Experimentation)->get_array('all', ['created_at' => 'DESC', 'experimentation_id' => 'DESC'], $criterias);

		$inprogress = $ended = [];
		foreach ($experimentations as $experimentation) {
			if ($experimentation['progress'] === 100) {
				$ended[] = $experimentation;
			} else {
				$inprogress[] = $experimentation;
			}
		}

		$params['experimentations_ended'] = $ended;
		$params['experimentations_inprogress'] = $inprogress;

		return $this->render($params);
	}

	public function timeline()
	{
		$experimentation_id = $this->get_get('experimentation_id');
		$experimentation = (new Experimentation())->in_depth(true)->get_absolutely($experimentation_id, 'Expérimentation introuvable');

		$milestones = (new Milestone())->get_array(100, ['due_date' => 'DESC'], [
			'experimentation_id' => $experimentation['experimentation_id']
		]);

		$dates = [];
		foreach ($milestones as $milestone) {
			$due_date = $milestone['due_date']->format('Y,m,d');
			$dates[] = [
				'startDate' => $due_date,
				'endDate' => $due_date,
				'headline' => $milestone['name'],
				'text' => $milestone['description'],
			];
		}
		$data = [
			'timeline' => [
				'headline' => $experimentation['name'],
				'type' => 'default',
				'text' => '<p>Cliquez sur les jalons ci-dessous pour en voir leurs détails</p>',
				'date' => $dates,
				'era' => []
			]
		];
		$response = new \Cameleon\Http\Response(json_encode($data));
		$response->setHeader('Content-Type', 'application/json');

		return $response;
	}
}
