<?php

use Cameleon\Controller\AController;

class ObjectLikeController extends AController
{
	use TAg2rController;

	public function chapter()
	{
	}

	# [GET] /object_like/all
	public function action_index()
	{
		$this->app()->chapter();

		$limit = 10;
		$offset = ($this->get_get('page', 1) - 1) * $limit;

		$object_likes = (new ObjectLike())->get_array($limit, [], [], $offset, $nb_rows, true);

		return $this->render([
			'object_likes' => $object_likes,
			'nb_rows' => $nb_rows,
			'pages_count' => ceil($nb_rows / $limit),
		]);
	}

	# [GET] /object_like/new
	public function action_new()
	{
		$this->app()->chapter();
		return $this->render();
	}

	# [POST] /object_like/new
	public function action_create()
	{
		$this->app()->chapter();
		(new ObjectLike)->create([
			'name' => $this->get_post('name'),
			'size' => $this->get_post('size'),
			'is_cute' => 1,
		]);
		return $this->redirect('/object_like/all');
	}

	# [GET] /object_like/1
	public function action_show()
	{
		$this->app()->chapter();
		$object_like_id = $this->get_data('id');
		return $this->render([
			'object_like' => (new ObjectLike)->get_absolutely($object_like_id),
		]);
	}

	# [GET] /object_like/1?edit
	public function action_edit()
	{
		$this->app()->chapter();
		$b = new ObjectLike($this->get_data('id'));
		return $this->render($b->get_current());
	}

	# [POST] /object_like/1?edit
	public function action_update()
	{
		$this->app()->chapter();
		$b = (new ObjectLike())->get_absolutely($this->get_data('id'));
		$data = $_POST;
		$b->update($data, [
			'object_like_id' => $this->get_data('id')
		]);
		return $this->redirect('/object_like/' . $this->get_data('id') . '?edit');
	}

	# [GET] /object_like/1?destroy
	public function action_destroy()
	{
		$this->app()->chapter();
//		(new ObjectLike($this->get_data('id')))->delete([
//			'object_like_id' => $this->get_data('id')
//		]);
		return $this->redirect('/object_like/all');
	}
}
