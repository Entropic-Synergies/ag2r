<?php

use Cameleon\Controller\AController;
use Cameleon\Exception\AccessDeniedException;
use Cameleon\Exception\NotFoundHttpException;
use Cameleon\Services;

class UserController extends AController
{
	use TAG2RController;

	public function action_index()
	{
		$params = [];
		$limit = 10;
		$offset = ($this->get_get('page', 1) - 1) * $limit;
		$criterias = [];
		$joins = '';
		$orders = [];
		$fields = [
			'first_name', 'last_name'
		];
		foreach ($fields as $field) {
			if (isset($_GET[$field]) && $_GET[$field]) {
				$criterias[] = sprintf('`%s` LIKE "%%%s%%"', $field, $this->get_db()->real_escape_string($_GET[$field]));
			}
		}
		if (isset($_GET['jobposition_id']) && $_GET['jobposition_id']) {
			$criterias['jobposition_id'] = $_GET['jobposition_id'];
		}
		if (isset($_GET['skill']) && $_GET['skill']) {
			$joins = 'INNER JOIN users_skills s ON s.user_id = users.user_id';
			$criterias[] = sprintf('s.`skill` LIKE "%%%s%%"', $this->get_db()->real_escape_string($_GET['skill']));
			$orders['s.value'] = 'DESC';
		}
		$orders['last_name'] = 'ASC';
		if (count($criterias) > 0) {
			$users = (new User)->get_array($limit, $orders, $criterias, $offset, $nb_rows, true, ['users.user_id'], $joins);
			$params['users'] = $users;
			$params['n_users'] = $nb_rows;
			$params['pages_count'] = ceil($nb_rows / $limit);
		}
		$params['job_positions'] = (new JobPosition())->get_array('all', ['name' => 'ASC']);

		return $this->render($params);
	}

	# [GET] /user/1
	public function action_show()
	{
		$user = (new User)->get_current($this->get_data('id'));

		if (!$user) {
			throw new NotFoundHttpException('Utilisateur introuvable');
		}

		$user['experimentations'] = (new Experimentation)->get_existing_objects('user', $user['user_id'], true, 4, $experimentations_count, true);
		$user['experimentations_count'] = $experimentations_count;

		$user['contacts_count'] = (new User())->get_contacts_count($user['user_id']);

		$relation = (new User($this->me['user_id']))->get_relation($user['user_id']);
		$status = !empty($relation['status']) ? $relation['status'] : "";
		$invited = (!empty($relation['user_id']) && $relation['user_id'] == $this->me['user_id']) ? true : false;

		return $this->render([
			"user"    => $user,
			"status"  => $status,
			"invited" => $invited
		]);
	}

	# [GET] /user/1?edit
	public function action_edit()
	{
		$user = (new User)->get_current($this->get_data('id'));

		if (!$user) {
			throw new NotFoundHttpException('Utilisateur introuvable');
		}
		if (!$this->get_me() or $user['user_id'] !== $this->get_me()['user_id']) {
			throw new AccessDeniedException('Accès interdit');
		}

		return $this->render([
			'user_id'       => $this->get_data('id'),
			'user'          => $user,
			'tags'          => (new Tag)->get_tags(),
			'job_positions' => (new JobPosition())->get_array('all', ['name' => 'ASC']),
			'list_sites' => User::get_sites_list(),
		]);
	}

	# [POST] /user/1
	public function action_update()
	{
		$user = (new User)->get_current($this->get_data('id'));

		if (!$user) {
			throw new NotFoundHttpException('Utilisateur introuvable');
		}
		if (!$this->get_me() or $user['user_id'] !== $this->get_me()['user_id']) {
			throw new AccessDeniedException('Accès interdit');
		}

		$data = $_POST;

		$skills = [];
		
		if (!isset($data['cgu'])) {
			$data['cgu'] = 0;
		}
		
		if (isset($data['skills'])) {
			foreach ($data['skills'] as $key => $skill) {
				if ($skill) {
					$skills[$skill] = $data['skill-level'][$key];
				}
			}
		}
		$data['skills'] = serialize($skills);

		if (isset($data['password']) && isset($data['confirm']) && !empty($data['password']) && !empty($data['confirm']) && $data['password'] == $data['confirm'])
			$data['password'] = (new User)->hash_password($data['password']);
		else
			unset($data['password']);
		unset($data['confirm']);

		(new User())->update($data, ['user_id' => $user['user_id']]);

		$tags = explode(',', substr($this->get_post('tags'), 1, -1));
		(new Tag)->set_xp_tags($tags, $user['key']);

		Services::get_flash_messenger()->add_flash('success', 'Les informations ont bien été enregistrées');

		return $this->redirect('/user/' . $user['user_id']);
	}

	public function ajax_filter_user()
	{
		$query = trim($this->get_post('query'));
		$query = \Cameleon\Services::get_db()->real_escape_string($query);
		$db = $this->get_db();
		$criterias = [
			'name LIKE "%' . $db->real_escape_string(str_replace(' ', '%', $query)) . '%"'
		];
		if ($this->get_get('not_me')) {
			$criterias[] = 'user_id != ' . intval($this->me['user_id']);
		}
		$users = (new User)->get_array(10, ['name' => 'ASC'], $criterias);

		if ($this->get_post('as_html')) {
			return $this->render([
				'users' => $users
			]);
		}
		foreach ($users as &$user) {
			$user['picture']['url'] = \Cameleon\Controller\AssetController::get_cached_image_url('crop', $user['picture']['url'], 60);
		}
		$data = (new User)->get_magicsuggest_mapped_users($users);
		$response = new \Cameleon\Http\Response(json_encode($data));
		$response->setHeader('Content-Type', 'application/json');

		return $response;
	}

	public function recommended()
	{
		$limit = 10;
		$offset = ($this->get_get('page', 1) - 1) * $limit;
		if ($offset < 0) {
			$offset = 0;
		}
		$users = (new User())->get_recommended($limit, $offset, $nb_rows);

		return $this->render([
			'users'       => $users,
			'n_users'     => $nb_rows,
			'pages_count' => ceil($nb_rows / $limit)
		]);
	}

	public function invitations()
	{
		$pending_requests = (new User())->get_pending_contacts_details($this->me['user_id']);

		return $this->render([
			'pending_requests' => $pending_requests
		]);
	}

	public function contact()
	{
		list($_SERVER['REQUEST_URI']) = explode('?', $_SERVER['REQUEST_URI']);
		$limit = 10;
		$offset = 0;
		$query = $this->get_get('query');
		$contacts = (new User())->get_contacts($this->me['user_id'], $limit, $offset, $nb_rows, $query);

		$requests = (new User)->get_pending_contacts_details($this->me['user_id']);

		return $this->render([
			'requests'    => $requests,
			'query'       => $query,
			'contacts'    => $contacts,
			'contacts_nb' => $nb_rows
		]);
	}

	public function contact_request()
	{
		(new User($this->me['user_id']))->contact_request($this->get_get('contact'));
		Services::get_flash_messenger()->add_flash('notice', 'Demande envoyée');

		return $this->redirect('/user/' . $this->get_get('contact'));
	}

	public function accept_request()
	{
		(new User($this->me['user_id']))->accept_request($this->get_get('contact'));
		Services::get_flash_messenger()->add_flash('success', 'Demande acceptée');

		return $this->redirect($_SERVER['HTTP_REFERER']);
	}

	public function decline_request()
	{
		(new User($this->me['user_id']))->decline_request($this->get_get('contact'));
		Services::get_flash_messenger()->add_flash('notice', 'Demande refusée');

		return $this->redirect($_SERVER['HTTP_REFERER']);
	}

	public function delete_relation()
	{
		(new User($this->me['user_id']))->delete_relation($this->get_get('contact'));
		Services::get_flash_messenger()->add_flash('notice', 'Contact supprimé');

		return $this->redirect($_SERVER['HTTP_REFERER']);
	}
}
