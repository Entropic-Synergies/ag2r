<?php

use Cameleon\Controller\AController;

class NotificationSubscriptionController extends AController
{
	use TAg2rController;

	public function chapter()
	{
	}

	# [GET] /notification_subscription/all
	public function action_index()
	{
		$this->app()->chapter();

		$limit = 10;
		$offset = ($this->get_get('page', 1) - 1) * $limit;

		$notification_subscriptions = (new NotificationSubscription())->get_array($limit, [], [], $offset, $nb_rows, true);

		return $this->render([
			'notification_subscriptions' => $notification_subscriptions,
			'nb_rows' => $nb_rows,
			'pages_count' => ceil($nb_rows / $limit),
		]);
	}

	# [GET] /notification_subscription/new
	public function action_new()
	{
		$this->app()->chapter();
		return $this->render();
	}

	# [POST] /notification_subscription/new
	public function action_create()
	{
		$this->app()->chapter();
		(new NotificationSubscription)->create([
			'name' => $this->get_post('name'),
			'size' => $this->get_post('size'),
			'is_cute' => 1,
		]);
		return $this->redirect('/notification_subscription/all');
	}

	# [GET] /notification_subscription/1
	public function action_show()
	{
		$this->app()->chapter();
		$notification_subscription_id = $this->get_data('id');
		return $this->render([
			'notification_subscription' => (new NotificationSubscription)->get_absolutely($notification_subscription_id),
		]);
	}

	# [GET] /notification_subscription/1?edit
	public function action_edit()
	{
		$this->app()->chapter();
		$b = new NotificationSubscription($this->get_data('id'));
		return $this->render($b->get_current());
	}

	# [POST] /notification_subscription/1?edit
	public function action_update()
	{
		$this->app()->chapter();
		$b = (new NotificationSubscription())->get_absolutely($this->get_data('id'));
		$data = $_POST;
		$b->update($data, [
			'notification_subscription_id' => $this->get_data('id')
		]);
		return $this->redirect('/notification_subscription/' . $this->get_data('id') . '?edit');
	}

	# [GET] /notification_subscription/1?destroy
	public function action_destroy()
	{
		$this->app()->chapter();
//		(new NotificationSubscription($this->get_data('id')))->delete([
//			'notification_subscription_id' => $this->get_data('id')
//		]);
		return $this->redirect('/notification_subscription/all');
	}
}
