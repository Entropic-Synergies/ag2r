<?php

class SamlController extends \Cameleon\Controller\AController
{
	static public function auth_init()
	{
		self::get_config();
		$authSource = self::get_sp_id();
		$as = new SimpleSAML_Auth_Simple($authSource);
		$as->requireAuth();

		$attributes = $as->getAttributes();

		$session = SimpleSAML_Session::getInstance();
		$emailAddress = $session->getAuthData($authSource, 'saml:sp:NameID')['Value'];
		$attributes['emailAddress'] = array($emailAddress);

		return $attributes;
	}

	static protected function get_sp_id()
	{
		return CAMELEON_ENVIRONMENT === 'dev' ? 'AG2R-SP-dev' : 'AG2R-SP';
	}

	public function acs()
	{
		$config = $this->get_config();

		$session = SimpleSAML_Session::getInstance();
		SimpleSAML_Logger::info('SAML2.0 - SP.AssertionConsumerService: Accessing SAML 2.0 SP endpoint AssertionConsumerService');

		if (!$config->getBoolean('enable.saml20-sp', true)) {
			throw new SimpleSAML_Error_Error('NOACCESS');
		}

		if (array_key_exists(SimpleSAML_Auth_ProcessingChain::AUTHPARAM, $_REQUEST)) {
			/* We have returned from the authentication processing filters. */

			$authProcId = $_REQUEST[SimpleSAML_Auth_ProcessingChain::AUTHPARAM];
			$authProcState = SimpleSAML_Auth_ProcessingChain::fetchProcessedState($authProcId);
			$this->finish_login($session, $authProcState);
		}

		$metadataHandler = SimpleSAML_Metadata_MetaDataStorageHandler::getMetadataHandler();
		$sp = $metadataHandler->getMetaDataCurrentEntityID();
		$spMetadata = $metadataHandler->getMetaDataConfig($sp, 'saml20-sp-hosted');

		$b = SAML2_Binding::getCurrentBinding();
		if ($b instanceof SAML2_HTTPArtifact) {
			$b->setSPMetadata($spMetadata);
		}

		$response = $b->receive();
		if (!($response instanceof SAML2_Response)) {
			throw new SimpleSAML_Error_BadRequest('Invalid message received to AssertionConsumerService endpoint.');
		}

		$idp = $response->getIssuer();
		if ($idp === null) {
			throw new Exception('Missing <saml:Issuer> in message delivered to AssertionConsumerService.');
		}

		$idpMetadata = $metadataHandler->getMetaDataConfig($idp, 'saml20-idp-remote');

		/* Fetch the request information if it exists, fall back to RelayState if not. */
		$requestId = $response->getInResponseTo();
		$info = $session->getData('SAML2:SP:SSO:Info', $requestId);
		if ($info === null) {
			/* Fall back to RelayState. */
			$info = array();
			$info['RelayState'] = $response->getRelayState();
			if (empty($info['RelayState'])) {
				$info['RelayState'] = $spMetadata->getString('RelayState', null);
			}
			if (empty($info['RelayState'])) {
				/* RelayState missing. */
				throw new SimpleSAML_Error_Error('NORELAYSTATE');
			}
		}

		try {
			$assertion = sspmod_saml_Message::processResponse($spMetadata, $idpMetadata, $response);
			if (count($assertion) > 1) {
				throw new SimpleSAML_Error_Exception('More than one assertion in received response.');
			}
			$assertion = $assertion[0];
		} catch (sspmod_saml_Error $e) {
			/* The status of the response wasn't "success". */

			$status = $response->getStatus();
			if (array_key_exists('OnError', $info)) {
				/* We have an error handler. Return the error to it. */
				SimpleSAML_Utilities::redirect($info['OnError'], array('StatusCode' => $status['Code']));
			}

			/* We don't have an error handler. Show an error page. */
			throw new SimpleSAML_Error_Error('RESPONSESTATUSNOSUCCESS', $e);
		}

		SimpleSAML_Logger::info('SAML2.0 - SP.AssertionConsumerService: Successful response from IdP');

		/*
		 * Attribute handling
		 */
		$attributes = $assertion->getAttributes();

		SimpleSAML_Logger::stats('saml20-sp-SSO ' . $metadataHandler->getMetaDataCurrentEntityID() . ' ' . $idp . ' NA');

		$nameId = $assertion->getNameId();

		/* Begin module attribute processing */

		$spMetadataArray = $spMetadata->toArray();
		$idpMetadataArray = $idpMetadata->toArray();

		$pc = new SimpleSAML_Auth_ProcessingChain($idpMetadataArray, $spMetadataArray, 'sp');

		$authProcState = array(
			'core:saml20-sp:NameID'       => $nameId,
			'core:saml20-sp:SessionIndex' => $assertion->getSessionIndex(),
			'core:saml20-sp:TargetURL'    => $info['RelayState'],
			'ReturnURL'                   => SimpleSAML_Utilities::selfURLNoQuery(),
			'Attributes'                  => $attributes,
			'Destination'                 => $spMetadataArray,
			'Source'                      => $idpMetadataArray,
		);

		$pc->processState($authProcState);
		/* Since this function returns, processing has completed and attributes have
		 * been updated.
		 */

		$this->finish_login($session, $authProcState);
	}

	protected function finish_login(SimpleSAML_Session $session, array $authProcState)
	{
		assert('is_array($authProcState)');
		assert('array_key_exists("Attributes", $authProcState)');
		assert('array_key_exists("core:saml20-sp:NameID", $authProcState)');
		assert('array_key_exists("core:saml20-sp:SessionIndex", $authProcState)');
		assert('array_key_exists("core:saml20-sp:TargetURL", $authProcState)');
		assert('array_key_exists("Source", $authProcState)');
		assert('array_key_exists("entityid", $authProcState["Source"])');

		$authData = array(
			'Attributes'           => $authProcState['Attributes'],
			'saml:sp:NameID'       => $authProcState['core:saml20-sp:NameID'],
			'saml:sp:SessionIndex' => $authProcState['core:saml20-sp:SessionIndex'],
			'saml:sp:IdP'          => $authProcState['Source']['entityid'],
		);

		$session->doLogin(self::get_sp_id(), $authData);

		SimpleSAML_Utilities::redirect($authProcState['core:saml20-sp:TargetURL']);
	}

	static protected function get_config()
	{
		$_SERVER['SCRIPT_NAME'] = $_SERVER['REQUEST_URI'];
		SimpleSAML_Configuration::setConfigDir(__DIR__ . '/../config/saml2/config');
		$config = SimpleSAML_Configuration::getInstance();
		SimpleSAML_Logger::notice(\Cameleon\Logger\Logger::get_notice_header());
		return $config;
	}

	static public function logout()
	{
		$config = self::get_config();
		$auth = new SimpleSAML_Auth_Simple(self::get_sp_id()); // or the auth source you using at your SP
		//$auth->logout();
	}

	public function metadata()
	{

		/* Load simpleSAMLphp, configuration and metadata */
		$config = self::get_config();
		$metadata = SimpleSAML_Metadata_MetaDataStorageHandler::getMetadataHandler();

		if (!$config->getValue('enable.saml20-sp', true)) {
			throw new SimpleSAML_Error_Error('NOACCESS');
		}

		/* Check if valid local session exists.. */
		if ($config->getBoolean('admin.protectmetadata', false)) {
			SimpleSAML_Utilities::requireAdmin();
		}

		try {

			$spentityid = isset($_GET['spentityid']) ? $_GET['spentityid'] : $metadata->getMetaDataCurrentEntityID();
			$spmeta = $metadata->getMetaDataConfig($spentityid, 'saml20-sp-hosted');

			$metaArray = array(
				'metadata-set'             => 'saml20-sp-remote',
				'entityid'                 => $spentityid,
				'AssertionConsumerService' => $metadata->getGenerated('AssertionConsumerService', 'saml20-sp-hosted'),
				'SingleLogoutService'      => $metadata->getGenerated('SingleLogoutService', 'saml20-sp-hosted'),
			);

			$metaArray['NameIDFormat'] = $spmeta->getString('NameIDFormat', 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient');

			if ($spmeta->hasValue('OrganizationName')) {
				$metaArray['OrganizationName'] = $spmeta->getLocalizedString('OrganizationName');
				$metaArray['OrganizationDisplayName'] = $spmeta->getLocalizedString('OrganizationDisplayName', $metaArray['OrganizationName']);

				if (!$spmeta->hasValue('OrganizationURL')) {
					throw new SimpleSAML_Error_Exception('If OrganizationName is set, OrganizationURL must also be set.');
				}
				$metaArray['OrganizationURL'] = $spmeta->getLocalizedString('OrganizationURL');
			}

			if ($spmeta->hasValue('attributes')) {
				$metaArray['attributes'] = $spmeta->getArray('attributes');
			}
			if ($spmeta->hasValue('attributes.NameFormat')) {
				$metaArray['attributes.NameFormat'] = $spmeta->getString('attributes.NameFormat');
			}
			if ($spmeta->hasValue('name')) {
				$metaArray['name'] = $spmeta->getLocalizedString('name');
			}
			if ($spmeta->hasValue('description')) {
				$metaArray['description'] = $spmeta->getLocalizedString('description');
			}

			$certInfo = SimpleSAML_Utilities::loadPublicKey($spmeta);
			if ($certInfo !== null && array_key_exists('certData', $certInfo)) {
				$metaArray['certData'] = $certInfo['certData'];
			}

			$metaflat = '$metadata[' . var_export($spentityid, true) . '] = ' . var_export($metaArray, true) . ';';

			$metaBuilder = new SimpleSAML_Metadata_SAMLBuilder($spentityid);
			$metaBuilder->addMetadataSP20($metaArray);
			$metaBuilder->addOrganizationInfo($metaArray);
			$metaBuilder->addContact('technical', array(
				'emailAddress' => $config->getString('technicalcontact_email', null),
				'name'         => $config->getString('technicalcontact_name', null),
			));
			$metaxml = $metaBuilder->getEntityDescriptorText();

			/* Sign the metadata if enabled. */
			$metaxml = SimpleSAML_Metadata_Signer::sign($metaxml, $spmeta->toArray(), 'SAML 2 SP');

			if (array_key_exists('output', $_REQUEST) && $_REQUEST['output'] == 'xhtml') {
				$t = new SimpleSAML_XHTML_Template($config, 'metadata.php', 'admin');
				$t->data['header'] = 'saml20-sp';
				$t->data['metadata'] = htmlspecialchars($metaxml);
				$t->data['metadataflat'] = htmlspecialchars($metaflat);
				$t->data['metaurl'] = SimpleSAML_Utilities::selfURLNoQuery();
				$t->show();
			} else {
				header('Content-Type: application/xml');

				echo $metaxml;
				exit(0);
			}
		} catch (Exception $exception) {

			throw new SimpleSAML_Error_Error('METADATA', $exception);
		}
	}
} 