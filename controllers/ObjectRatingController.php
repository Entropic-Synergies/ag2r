<?php

use Cameleon\Controller\AController;

class ObjectRatingController extends AController
{
	use TAg2rController;

	public function chapter()
	{
	}

	# [GET] /object_rating/all
	public function action_index()
	{
		$this->app()->chapter();

		$limit = 10;
		$offset = ($this->get_get('page', 1) - 1) * $limit;

		$object_ratings = (new ObjectRating())->get_array($limit, [], [], $offset, $nb_rows, true);

		return $this->render([
			'object_ratings' => $object_ratings,
			'nb_rows' => $nb_rows,
			'pages_count' => ceil($nb_rows / $limit),
		]);
	}

	# [GET] /object_rating/new
	public function action_new()
	{
		$this->app()->chapter();
		return $this->render();
	}

	# [POST] /object_rating/new
	public function action_create()
	{
		$this->app()->chapter();
		(new ObjectRating)->create([
			'name' => $this->get_post('name'),
			'size' => $this->get_post('size'),
			'is_cute' => 1,
		]);
		return $this->redirect('/object_rating/all');
	}

	# [GET] /object_rating/1
	public function action_show()
	{
		$this->app()->chapter();
		$object_rating_id = $this->get_data('id');
		return $this->render([
			'object_rating' => (new ObjectRating)->get_absolutely($object_rating_id),
		]);
	}

	# [GET] /object_rating/1?edit
	public function action_edit()
	{
		$this->app()->chapter();
		$b = new ObjectRating($this->get_data('id'));
		return $this->render($b->get_current());
	}

	# [POST] /object_rating/1?edit
	public function action_update()
	{
		$this->app()->chapter();
		$b = (new ObjectRating())->get_absolutely($this->get_data('id'));
		$data = $_POST;
		$b->update($data, [
			'object_rating_id' => $this->get_data('id')
		]);
		return $this->redirect('/object_rating/' . $this->get_data('id') . '?edit');
	}

	# [GET] /object_rating/1?destroy
	public function action_destroy()
	{
		$this->app()->chapter();
//		(new ObjectRating($this->get_data('id')))->delete([
//			'object_rating_id' => $this->get_data('id')
//		]);
		return $this->redirect('/object_rating/all');
	}
}
