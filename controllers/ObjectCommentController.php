<?php
namespace Cameleon\Controller;

class ObjectCommentController
{

	use \Cameleon\Controller, CURRENTAPPController;

	public function chapter () {}

	# [GET] /ObjectComment/all
	public function action_index ()
	{
		$this->app()->chapter();
		$this->render([
			'ObjectComments' => (new ObjectComment)->get_array('all'),
		]);
	}

	# [GET] /ObjectComment/new
	public function action_new ()
	{
		$this->app()->chapter();
		$this->render();
	}

	# [POST] /ObjectComment/new
	public function action_create ()
	{
		$this->app()->chapter();
		(new ObjectComment)->create([
			'name' => $this->get_post('name'),
			'size' => $this->get_post('size'),
			'is_cute' => 1,
		]);
		$this->redirect('/ObjectComment/all');
	}

	# [GET] /ObjectComment/1
	public function action_show ()
	{
		$this->app()->chapter();
		$ObjectComment_id = $this->get_data('id');
		$this->render([
			'ObjectComment' => (new ObjectComment)->get_current($ObjectComment_id),
		]);
	}

	# [GET] /ObjectComment/1?edit
	public function action_edit ()
	{
		$this->app()->chapter();
		$b = new ObjectComment($this->get_data('id'));
		$this->render($b->get_current());
	}

	# [GET] /ObjectComment/1?destroy
	public function action_destroy ()
	{
		$this->app()->chapter();
		// (new ObjectComment($this->get_data('id')))->delete();
		$this->redirect('/ObjectComment/all');
	}

	# [POST] /ObjectComment/ajax
	public function action_ajax ()
	{
		$this->app()->chapter();
		header('Content-Type: application/json');
		echo json_encode((new ObjectComment)->get_array('all'));
	}

	# [GET] /ObjectComment/abc
	public function get ()
	{
		$this->app()->chapter();

	}

	# [POST] /ObjectComment/abc
	public function post ()
	{
		$this->app()->chapter();
		// some actions
		$this->redirect('/ObjectComment/abc');
	}

}
