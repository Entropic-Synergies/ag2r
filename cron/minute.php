<?php

require __DIR__ . '/../vendor/autoload.php';

$workers = [
	'send_email_leaddev',
];

foreach ($workers as $w){
	$worker_file = CAMELEON_DIR_WORKERS . $w . '.php';
	$log_file = CAMELEON_DIR_LOG . date("Y-m-d") . '_' . CAMELEON_ENVIRONMENT . '_' . $w . '.log';
	shell_exec('php ' . $worker_file . ' >> ' . $log_file . ' &' );
}

