<?php

/*
Il est très important que ce script n'ai
AUCUNE dépendance dans les sources de l'application
car si les sources sont buggées,
ce script doit être OK en toutes circonstances
*/

proc_nice(5);

$test_directory = __DIR__ . '/../tests/';
$test_files = array_diff(scandir($test_directory), array('..', '.'));

$output = array();

foreach ($test_files as $test_file) {

	$stdout = shell_exec('php ' . $test_directory . '/' . $test_file);

	if(!empty($stdout)){
		$output[$test_file] = $stdout;
	}

}

if(!empty($output)){

	$mailcontent = '<p style="font-family:Courier;">';
	$mailcontent .= getcwd() . ' : ' . count($output) . '/' . count($test_files) . ' test failed<br>';
	$mailcontent .= @date('l jS \of F Y h:i:s A') . '<br>';
	$mailcontent .= 'on ' . __DIR__;
	$mailcontent .= '<br>';

	foreach ($output as $test_name => $stdout) {
		$mailcontent .= $test_name . ' failed on ' . getcwd() . '<br>';
		$mailcontent .= '-------------------------------------------------------<br>';
		$mailcontent .=  nl2br($stdout) . '<br>';
		$mailcontent .= '<br>';
	}
	$mailcontent .= '</p>';

	$subject = '[cameleon] test failure';


	$params = array(
		'api_user'  => 'es-infra@entropic-synergies.com',
		'api_key'   => 'B34uvo1s!',
		'to'        => 'jroussel@mncc.fr',
		'subject'   => $subject,
		'html'      => $mailcontent,
		'from'      => 'dev@entropic-synergies.com',
	);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, 'https://sendgrid.com/api/mail.send.json');
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  //to suppress the curl output
	echo curl_exec($ch);
	curl_close($ch);

	echo 'email sent';
}
