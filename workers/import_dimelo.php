<?php

// To use this script, please install PHPExcel at root. PHPExcel can be removed after this script execution
//
// WARNING: ugly code below...

require __DIR__ . '/../vendor/autoload.php';

$routes = require __DIR__ . '/../config/routes.php';

$app = new \Cameleon\App();
$obj = [];
$cats = [];
$directions = [];
$direction_DB = [];
$statuses = [];

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

date_default_timezone_set('Europe/Paris');

require_once dirname(__FILE__) . '/../Classes/PHPExcel/IOFactory.php';

function check_files() {
  echo date('H:i:s') , " Check for Excel files" , EOL;
  if (!file_exists('users.xlsx')) {
    echo 'Missing Excel file users.xlsx' , EOL;
    exit (1);
  }
}

function clean_db() {
  echo date('H:i:s') , " Empty DB" , EOL;
  $db = \Cameleon\Services::get_db();

  $db->db_query('alter table `users` AUTO_INCREMENT = 1');
  $db->db_query('alter table `ideas` AUTO_INCREMENT = 1');

  $tables = [
    'documents',
    'experimentations',
    'experimentations_tags',
    'experimentations_users',
    'feeds',
    'activitylogs',
    'groups',
    'idea_categorys',
    'ideacategoryaccesss',
    'ideas',
    'ideas_operators',
    'innovations',
    'innovations_users',
    'jobpositions',
    'mails',
    'messages',
    'milestones',
    'notification_subscriptions',
    'notifications',
    'object_comments',
    'object_likes',
    'object_ratings',
    'object_reports',
    'object_shares',
    'objectcomments',
    'perimeters',
    'posts',
    'samls',
    'tags',
    'tasks',
    'users',
    'users_skills',
    'users_tags',
    'users_users',
  ];
  foreach ($tables as $table) {
    $db->db_query('delete from `' . $table . '`');
    $db->db_query('alter table `' . $table . '` AUTO_INCREMENT = 1');
  }
}

function read_users() {
  echo date('H:i:s') , " Load Data from Excel file" , EOL;
  global $obj;
  global $cats;
  global $statuses;
  global $directions;
  global $direction_DB;
  $objReader = PHPExcel_IOFactory::createReader('Excel2007');
  $objPHPExcel = $objReader->load("users.xlsx");

  foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
    if ($worksheet->getCodeName() == 'Worksheet') {
      foreach ($worksheet->getRowIterator() as $row) {
        if ($row->getRowIndex() != 1) {
          $cellIterator = $row->getCellIterator();
          $cellIterator->setIterateOnlyExistingCells(false);
          $email = '';
          $direction = '';
          foreach ($cellIterator as $cell) {
            if (!is_null($cell)) {
              if ($cell->getColumn() == 'A')
                $email = $cell->getCalculatedValue();
              if ($cell->getColumn() == 'B')
                $direction = $cell->getCalculatedValue();
            }
          }
          $directions[$email] = $direction;
        }
      }
    }
    if ($worksheet->getCodeName() == 'Worksheet_1') {
      foreach ($worksheet->getRowIterator() as $row) {
        $cellIterator = $row->getCellIterator();
        $cellIterator->setIterateOnlyExistingCells(false);
        $direction = '';
        foreach ($cellIterator as $cell) {
          if (!is_null($cell)) {
            if ($cell->getColumn() == 'A')
              $direction = $cell->getCalculatedValue();
          }
        }
        $jp = (new Jobposition)->create(['name' => $direction]);
        $direction_DB[$direction] = $jp['jobposition_id'];
      }
    }
    if ($worksheet->getCodeName() == 'Worksheet_2') {
      foreach ($worksheet->getRowIterator() as $row) {
        if ($row->getRowIndex() != 1) {
          $cellIterator = $row->getCellIterator();
          $cellIterator->setIterateOnlyExistingCells(false);
          $email = '';
          $id = 0;
          $idea = '';
          $content = '';
          $status = '';
          $date = '';
          $firstname = '';
          $lastname = '';
          $category = '';
          foreach ($cellIterator as $cell) {
            if (!is_null($cell)) {
              if ($cell->getColumn() == 'J')
                $email = $cell->getCalculatedValue();
              if ($cell->getColumn() == 'A')
                $id = $cell->getCalculatedValue();
              if ($cell->getColumn() == 'D')
                $idea = $cell->getCalculatedValue();
              if ($cell->getColumn() == 'E')
                $content = $cell->getCalculatedValue();
              if ($cell->getColumn() == 'P') {
                if (!in_array($cell->getCalculatedValue(), $statuses))
                  $statuses[] = $cell->getCalculatedValue();
                $status = $cell->getCalculatedValue();
              }
              if ($cell->getColumn() == 'F')
                $date = PHPExcel_Style_NumberFormat::toFormattedString($cell->getCalculatedValue(), 'YYYY-MM-DD');
              if ($cell->getColumn() == 'H')
                $firstname = $cell->getCalculatedValue();
              if ($cell->getColumn() == 'I')
                $lastname = $cell->getCalculatedValue();
              if ($cell->getColumn() == 'C') {
                if (!in_array($cell->getCalculatedValue(), $cats))
                  $cats[] = $cell->getCalculatedValue();
                $category = $cell->getCalculatedValue();
              }
            }
          }
          if (in_array($email, array_keys($obj))) {
            //echo date('H:i:s') , ' Email ' . $email . ' already exists' , EOL;
          } else {
            $obj[$email] = Array();
            $obj[$email]['idea'] = Array();
            $obj[$email]['firstname'] = $firstname;
            $obj[$email]['lastname'] = $lastname;
            $obj[$email]['jobposition_id'] = (isset($email) and !empty($email) and isset($directions[$email]) and isset($direction_DB[$directions[$email]])) ? $direction_DB[$directions[$email]] : $direction_DB['Autre'];
          }
          if (empty($obj[$email]['firstname']) and !empty($firstname)) {
            $obj[$email]['firstname'] = $firstname;
          }
          if (empty($obj[$email]['lastname']) and !empty($lastname))
            $obj[$email]['lastname'] = $lastname;
          $obj[$email]['idea'][] = [
            'id' => $id,
            'date' => $date,
            'title' => $idea,
            'content' => $content,
            'status' => $status,
            'category' => $category,
          ];
        }
      }
    }
  }
  echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;
  echo date('H:i:s') , " Email count " , count(array_keys($obj)) , EOL;
}

function ucwords_specific ($string, $delimiters = '', $encoding = NULL)
{
  if ($encoding === NULL)
    $encoding = mb_internal_encoding();

  if (is_string($delimiters))
    $delimiters =  str_split( str_replace(' ', '', $delimiters));

  $delimiters_pattern1 = array();
  $delimiters_replace1 = array();
  $delimiters_pattern2 = array();
  $delimiters_replace2 = array();
  foreach ($delimiters as $delimiter)
  {
    $uniqid = uniqid();
    $delimiters_pattern1[]   = '/'. preg_quote($delimiter) .'/';
    $delimiters_replace1[]   = $delimiter.$uniqid.' ';
    $delimiters_pattern2[]   = '/'. preg_quote($delimiter.$uniqid.' ') .'/';
    $delimiters_replace2[]   = $delimiter;
  }

  $return_string = $string;
  $return_string = preg_replace($delimiters_pattern1, $delimiters_replace1, $return_string);

  $words = explode(' ', $return_string);

  foreach ($words as $index => $word)
    $words[$index] = mb_strtoupper(mb_substr($word, 0, 1, $encoding), $encoding).mb_substr($word, 1, mb_strlen($word, $encoding), $encoding);
  $return_string = implode(' ', $words);
  $return_string = preg_replace($delimiters_pattern2, $delimiters_replace2, $return_string);

  return $return_string;
}

function get_category($cat) {
  $k = (new IdeaCategory)->get_array(1, 'name', 'name = \'' . $cat . '\'');
  if (!empty($k)) {
    foreach ($k as $r)
      return $r['idea_category_id'];
  } else {
    $n = (new IdeaCategory)->create([
      'allow' => 1,
      'name' => $cat,
      'start_date' => '2000-01-01 05:00:00',
      'end_date' => '2014-05-05 05:00:00',
      'user_id' => 1
    ]);
    return $n['idea_category_id'];
  }
}

function map_category($cat) {
  if ($cat == '')
    return 'Temps forts précédents';
  if ($cat == 'Temps forts précédents')
    return 'Temps forts précédents';
  if ($cat == 'Nom du site internet destiné aux futurs retraités')
    return 'Site futur retraité';
  if ($cat == 'Nom des produits individuels et collectifs ANI')
    return 'Nom produits ANI';
  if ($cat == 'Nom du site des associations partenaires')
    return 'Site associations partenaires';
  if ($cat == 'Parler de la réforme des retraites de facon innovante')
    return 'Reforme retraite innovante';
  if ($cat == 'Gestion Coll : comment inciter nos clients à télé déclarer ?')
    return 'Inciter clients télédéclaration';
  if ($cat == 'C&apos;est fastidieux... et ca pourrait etre plus simple')
    return 'Plus simple';
  if ($cat == 'C\'est fastidieux... et ca pourrait etre plus simple')
    return 'Plus simple';
  if ($cat == 'Nom de la communauté des aidants')
    return 'Communauté aidants';
  if ($cat == 'Priorité CLIENTS : comment obtenir les adresses mails de nos clients ?')
    return 'Obtenir mails clients';
  if ($cat == 'Priorité CLIENTS : inciter nos clients à venir (et revenir) sur nos sites internet?')
    return 'Venir sur internet';
  if ($cat == 'Idées Marketing / Commerciales')
    return 'Idées Marketing / Commerciales';
  if ($cat == 'Comment transformer un client mécontent en client satisfait?')
    return 'Devenir client satisfait';
  if ($cat == 'Maitriser le volume d&apos;appels en CRC')
    return 'Baisse appels CRC';
  if ($cat == 'Maitriser le volume d\'appels en CRC')
    return 'Baisse appels CRC';
  if ($cat == 'Améliorer l&apos;efficacité la relation commercial / gestion')
    return 'Efficacité commercial/gestion';
  if ($cat == 'Améliorer l\'efficacité la relation commercial / gestion')
    return 'Efficacité commercial/gestion';
  if ($cat == 'Comment mieux faire connaitre nos outils et services auprès de nos assurés?')
    return 'Faire connaitre nos services';
  if ($cat == 'ANI : Prescripteurs')
    return 'ANI : Prescripteurs';
  if ($cat == 'Améliorer les courriers pour éviter le rappel de clients')
    return 'Eviter rappel clients';
  if ($cat == 'Améliorer la diffusion d&apos;informations métiers (notice, processus,...)')
    return 'Améliorer diffusion informations';
  if ($cat == 'Améliorer la diffusion d\'informations métiers (notice, processus,...)')
    return 'Améliorer diffusion informations';
  if ($cat == 'Priorité CLIENTS : quels simulateurs proposer à nos clients sur internet? ')
    return 'Quels simulateurs internet';
  if ($cat == 'Appli iPad pour les experts-comptables : quelles fonctionnalités imaginer pour les aider à vendre les missions sociales et à nous recommande')
    return 'iPad expert-comptable';
  if ($cat == 'Faciliter la collaboration entre directions')
    return 'Collaboration entre directions';
  if ($cat == 'Quels outils et services imaginer pour développer notre positionnement sur la retraite et répondre aux besoins de nos clients ?')
    return 'Répondre besoins clients';
  if ($cat == 'Nom de l&apos;appli mobile Retraite')
    return 'Nom appli Retraite';
  if ($cat == 'Nom de l\'appli mobile Retraite')
    return 'Nom appli Retraite';
  if ($cat == 'ANI : Gestion')
    return 'ANI : Gestion';
  if ($cat == 'ANI : Distribution pour les réseaux de proximité MDP, MDE et MDPro')
    return 'ANI : Distribution';
  if ($cat == 'Quelles synergies mettre en place entre MDP, MDPro et MDE pour augmenter le nombre d’affaires?')
    return 'Synergie reseau proximité';
  var_dump($cat);die();
  return 'none';
}

function insert_data() {
  echo date('H:i:s') , " Insert Ideas from Excel file" , EOL;
  global $obj;
  foreach ($obj as $user => $content) {
    $userobj = (new User)->create(
      [
        'email' => $user,
        'password' => "xxx",
        'roles' => 'a:0:{}',
        'first_name' => ucwords_specific(mb_strtolower($content['firstname'], 'UTF-8'), "-'"),
        'last_name' => ucwords_specific(mb_strtolower($content['lastname'], 'UTF-8'), "-'"),
        'login_name' => '',
        'jobposition_id' => $content['jobposition_id'],
        'cgu' => 0
      ]
    );
    foreach ($content['idea'] as $idea) {
      $fstatus = 'draft';
      if ($idea['status'] == 'Non retenu')
        $fstatus = 'refused';
      if ($idea['status'] == 'Déjà existant')
        $fstatus = 'duplicate';
      if ($idea['status'] == 'Mis en oeuvre')
        $fstatus = 'confirmed';
      if ($idea['status'] == 'En cours d&apos;estimation')
        $fstatus = 'estimate';
      if ($idea['status'] == 'En cours d\'estimation')
        $fstatus = 'estimate';
      if ($idea['status'] == 'A l&apos;étude')
        $fstatus = 'waiting';
      if ($idea['status'] == 'A l\'étude')
        $fstatus = 'waiting';
      $newidea = (new Idea)->create(
        [
          'created_at' => $idea['date'] . ' 05:00:00',
          'name' => str_replace("&apos;","'",$idea['title']),
          'description' => str_replace("&apos;","'",$idea['content']),
          'user_id' => $userobj['user_id'],
          'status' => $fstatus,
          'idea_category_id' => get_category(map_category($idea['category']))
        ]);
    }
  }
  // Specific data for Anonymous user to be switched to admin account
  (new User)->update(
    [
      'email' => 'ag2r@entropic-group.com',
      'password' => "xxx",
      'description' => "Admin AG2R",
      'name' => "Admin AG2R",
      'roles' => 'a:2:{i:0;s:4:"USER";i:1;s:10:"SUPERADMIN";}',
      'first_name' => 'AG2R',
      'last_name' => 'Admin',
      'login_name' => 'admin',
      'cgu' => 1
    ],
    [
      'user_id' => 1
    ]);
  // Switch two specific users to admin
  $adm = (new User)->get_one('user_id', '`email`="damien.debloteau@ag2rlamondiale.fr"');
  (new User)->update(['roles' => 'a:2:{i:0;s:4:"USER";i:1;s:10:"SUPERADMIN";}'], ['user_id' => $adm['user_id']]);
  // This user does not exists in Dimelo import
  //$adm = (new User)->get_one('user_id', '`email`="remy.longueville@ag2rlamondiale.fr"');
  //(new User)->update(['roles' => 'a:2:{i:0;s:4:"USER";i:1;s:10:"SUPERADMIN";}'], ['user_id' => $adm['user_id']]);
  echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;
}

check_files();
clean_db();
read_users();
insert_data();
