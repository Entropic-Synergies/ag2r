<?php

newrelic_set_appname('ag2r');
newrelic_name_transaction('/ag2r/workers/send_activity_logs_email');

require __DIR__ . '/../vendor/autoload.php';

$routes = require __DIR__ . '/../config/routes.php';

$app = new \Cameleon\App();
$obj = [];
$cats = [];
$statuses = [];

date_default_timezone_set('Europe/Paris');

function test() {
	$monthly_active_users = (new ActivityLog)->get_monthly_active_users();

	$start_day = new DateTime('NOW');
	$start_day->modify('first day of this month 00:00:00');
	$end_day = new DateTime('NOW');
	$end_day->modify('last day of this month 23:59:59');

	$email_content = '<h1>Connections du ' . date("Y-m-d", $start_day->getTimestamp()) . " au " . date("Y-m-d", $end_day->getTimestamp()) . "</h1>\n";

	$email_content .= "<ul>\n";
	foreach ($monthly_active_users as &$login) {
		$user = (new User)->get_current($login['user_id']);
		$tmp = "<li>" . $user['first_name'] . " " . $user["last_name"] . " (" . $login['user_id'] . ") last login at " . date("Y-m-d H:i:s", $login['timestamp']->getTimestamp()) . "</li>\n";
		$email_content .= $tmp;
	}
	$email_content .= "</ul>\n";

	//echo $email_content;

	mail('ademoulins@entropic-synergies.com', $email_content, date(DATE_RFC822));
}

test();
