<?php

require __DIR__ . '/../vendor/autoload.php';

$routes = require __DIR__ . '/../config/routes.php';

$app = new \Cameleon\App();

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

date_default_timezone_set('Europe/Paris');

function insert_data() {
  $users = (new User)->get_array('all');
  foreach ($users as $u) {
    if (!isset($u['name']) or empty($u['name'])) {
      echo 'Adding ' . $u['first_name'] . ' ' . $u['last_name'] . EOL;
      (new User)->update(['name' => $u['first_name'] . ' ' . $u['last_name']], ['user_id' => $u['user_id']]);
    }
  }
}

insert_data();
