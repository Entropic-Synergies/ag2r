<?php

// To use this script, please install PHPExcel at root. PHPExcel can be removed after this script execution
//
// WARNING: ugly code below...

require __DIR__ . '/../vendor/autoload.php';

$routes = require __DIR__ . '/../config/routes.php';

$app = new \Cameleon\App();
$obj = [];
$cats = [];
$directions = [];
$direction_DB = [];
$statuses = [];

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

date_default_timezone_set('Europe/Paris');

require_once dirname(__FILE__) . '/../Classes/PHPExcel/IOFactory.php';

function check_files() {
  if (!file_exists('users.xlsx')) {
    echo 'Missing Excel file users.xlsx' , EOL;
    exit (1);
  }
}

function read_users() {
  global $obj;
  global $cats;
  global $statuses;
  global $directions;
  global $direction_DB;
  $objReader = PHPExcel_IOFactory::createReader('Excel2007');
  $objPHPExcel = $objReader->load("users.xlsx");

  foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
    if ($worksheet->getCodeName() == 'Worksheet') {
      foreach ($worksheet->getRowIterator() as $row) {
        if ($row->getRowIndex() != 1) {
          $cellIterator = $row->getCellIterator();
          $cellIterator->setIterateOnlyExistingCells(false);
          $email = '';
          $direction = '';
          foreach ($cellIterator as $cell) {
            if (!is_null($cell)) {
              if ($cell->getColumn() == 'A')
                $email = $cell->getCalculatedValue();
              if ($cell->getColumn() == 'B')
                $direction = $cell->getCalculatedValue();
            }
          }
          $directions[$email] = $direction;
        }
      }
    }
    if ($worksheet->getCodeName() == 'Worksheet_1') {
      foreach ($worksheet->getRowIterator() as $row) {
        $cellIterator = $row->getCellIterator();
        $cellIterator->setIterateOnlyExistingCells(false);
        $direction = '';
        foreach ($cellIterator as $cell) {
          if (!is_null($cell)) {
            if ($cell->getColumn() == 'A')
              $direction = $cell->getCalculatedValue();
          }
        }
        $jp = (new Jobposition)->get_one('jobposition_id', '`name`="'.mysql_real_escape_string($direction).'"'); //['name' => $direction]);
        $direction_DB[$direction] = $jp['jobposition_id'];
      }
    }
    if ($worksheet->getCodeName() == 'Worksheet_2') {
      foreach ($worksheet->getRowIterator() as $row) {
        if ($row->getRowIndex() != 1) {
          $cellIterator = $row->getCellIterator();
          $cellIterator->setIterateOnlyExistingCells(false);
          $id = 0;
          $title = '';
          $description = '';
          $email = '';
          $firstname = '';
          $lastname = '';
          foreach ($cellIterator as $cell) {
            if (!is_null($cell)) {
              if ($cell->getColumn() == 'A')
                $id = $cell->getCalculatedValue();
              if ($cell->getColumn() == 'Y')
                $email = $cell->getCalculatedValue();
              if ($cell->getColumn() == 'D')
                $title = $cell->getCalculatedValue();
              if ($cell->getColumn() == 'E')
                $description = $cell->getCalculatedValue();
              if ($cell->getColumn() == 'W')
                $firstname = $cell->getCalculatedValue();
              if ($cell->getColumn() == 'X')
                $lastname = $cell->getCalculatedValue();
            }
          }
          if (isset($email) and !empty($email))
            $obj[$id] = [
              'id' => $id,
              'email' => $email,
              'title' => $title,
              'description' => $description,
              'firstname' => $firstname,
              'lastname' => $lastname,
            ];
        }
      }
    }
  }
}

function ucwords_specific ($string, $delimiters = '', $encoding = NULL)
{
  if ($encoding === NULL)
    $encoding = mb_internal_encoding();

  if (is_string($delimiters))
    $delimiters =  str_split( str_replace(' ', '', $delimiters));

  $delimiters_pattern1 = array();
  $delimiters_replace1 = array();
  $delimiters_pattern2 = array();
  $delimiters_replace2 = array();
  foreach ($delimiters as $delimiter)
  {
    $uniqid = uniqid();
    $delimiters_pattern1[]   = '/'. preg_quote($delimiter) .'/';
    $delimiters_replace1[]   = $delimiter.$uniqid.' ';
    $delimiters_pattern2[]   = '/'. preg_quote($delimiter.$uniqid.' ') .'/';
    $delimiters_replace2[]   = $delimiter;
  }

  $return_string = $string;
  $return_string = preg_replace($delimiters_pattern1, $delimiters_replace1, $return_string);

  $words = explode(' ', $return_string);

  foreach ($words as $index => $word)
    $words[$index] = mb_strtoupper(mb_substr($word, 0, 1, $encoding), $encoding).mb_substr($word, 1, mb_strlen($word, $encoding), $encoding);
  $return_string = implode(' ', $words);
  $return_string = preg_replace($delimiters_pattern2, $delimiters_replace2, $return_string);

  return $return_string;
}

function insert_data() {
  global $obj;
  global $directions;
  global $direction_DB;
  $missings = [];
  foreach ($obj as $id => $content) {
    $userobj = (new User)->get_one('user_id', '`email`="'.$content['email'].'"');
    if (isset($userobj) and !empty($userobj)) {
      $idea = (new Idea)->get_one('idea_id', '`name`="'.mysql_real_escape_string($content['title']).'" AND `description`="'.mysql_real_escape_string($content['description']).'"');
      if (isset($idea) and !empty($idea)) {
        $userids = [];
        $current_operators = (new Idea)->get_operators($idea['idea_id']);
        foreach($current_operators as $operator)
          $userids[] = $operator['user_id'];
        if (!in_array($userobj['user_id'], $userids)) {
          echo 'insert into `ideas_operators` (`idea_id`, `user_id`) values ("' . $idea['idea_id'] . '", "' . $userobj['user_id'] . '");' . EOL;
        }
      }
    } else {
      if (!in_array($content['email'], $missings)) {
        $missings[$content['email']] = [];
        $missings[$content['email']]['firstname'] = $content['firstname'];
        $missings[$content['email']]['lastname'] = $content['lastname'];
      }
    }
  }
  foreach ($missings as $e => $v) {
    $job_id = 'NULL';
    if (isset($directions[$e]) and isset($direction_DB[$directions[$e]]))
      $job_id = $direction_DB[$directions[$e]];
    echo '-- Adding missing email ' . $e . EOL;
    echo 'insert into `users` (`created_at`, `updated_at`, `first_name`, `last_name`, `email`, `password`, `roles`, `skills`, `jobposition_id`, `cgu`) values (NOW(), NOW(), "'.$v['firstname'].'", "'.$v['lastname'].'", "'.$e.'", "xxx", "a:0:{}", "a:0:{}", '.$job_id.', 0);' . EOL;
  }
}

check_files();
read_users();
insert_data();
