<?php
require __DIR__ . '/../vendor/autoload.php';

$routes = require __DIR__ . '/../config/routes.php';

$app = new \Cameleon\App();

function run(Cameleon\Model\AModel $model, $fk)
{
	$items = $model->get_array('all');
	foreach ($items as $item) {
		$item = $model->format_picture_old($item);
		if (null !== $item['picture']['url']) {
			echo $item['name'] . "\n";
			(new $model)->update(
				[
					'file_picture_id' => $item['picture']['file_id']
				],
				[
					$fk => $item[$fk]
				]
			);
		} else {
		$model->update(
				[
					'file_picture_id' => null
				],
				[
					$fk => $item[$fk]
				]
			);
		}
	}
}

run(new User, 'user_id');
run(new Experimentation(), 'experimentation_id');
run(new Idea(), 'idea_id');
run(new Innovation(), 'innovation_id');

